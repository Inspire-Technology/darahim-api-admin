<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['users'] = 'webservices/users';
$route['signup'] = 'webservices/users/userSignUp';
$route['createProfile'] = 'webservices/users/createProfile';
$route['verifyCode'] = 'webservices/users/UserVerifyCode';
$route['login'] = 'webservices/users/userLogin';
$route['changePassword'] = 'webservices/users/changePassword';
$route['forgetPasswordByOTP'] = 'webservices/users/forgetPasswordByOTP';


$route['chatPush'] = 'webservices/users/chatPush';
$route['updateLocale'] = 'webservices/users/updateLocale';
$route['Car'] = 'webservices/Car';
$route['addCar'] = 'webservices/Car/addCar';
$route['getCar'] = 'webservices/Car/getCar';
$route['getCars'] = 'webservices/Car/getCars';
$route['getallCars'] = 'webservices/Car/getallCars';
$route['get_Cars'] = 'webservices/Car/get_Cars';
$route['editCar'] = 'webservices/Car/editCar';
$route['editCarnew'] = 'webservices/Car/editCarnew';
$route['CarsBySorting'] = 'webservices/Car/CarsBySorting';
$route['CarsByFilter'] = 'webservices/Car/CarsByFilter';
$route['carLike'] = 'webservices/Car/carLike';
$route['addFav'] = 'webservices/Car/addFav';
$route['blockCar'] = 'webservices/Car/blockCar';
$route['favCars'] = 'webservices/Car/favCars';
$route['carTables'] = 'webservices/Car/carTables';
$route['carTablesKeywords'] = 'webservices/Car/carTablesKeywords';
$route['getAdminSetting'] = 'webservices/Car/getAdminSetting';
$route['getSoldCars'] = 'webservices/Car/getSoldCars';
$route['GetCarHistory'] = 'webservices/Car/GetCarHistory';
$route['GetPlateHistory'] = 'webservices/Car/GetPlateHistory';
$route['userProfile'] = 'webservices/users/userProfile';
$route['editProfile'] = 'webservices/users/editProfile';
$route['addCarnew'] = 'webservices/Car/addCarnew';
$route['addCarneww'] = 'webservices/Car/addCarnewnew';
$route['addCarWithPush'] = 'webservices/Car/addCarWithPush';
$route['reportCar'] = 'webservices/Car/reportCar';
$route['getCarsbyImage'] = 'webservices/Car/getCarsbyImage';
$route['isPushNotification'] = 'webservices/Car/pushNotificationFavModel';
$route['GetPayment'] = 'webservices/car/GetPayment';
$route['addCarWithPushNew'] = 'webservices/Car/addCarWithPushNew';
$route['UploadCarImage'] = 'webservices/Car/UploadCarImage';
$route['editCarWoImage'] = 'webservices/Car/editCarWoImage';
$route['paidStatus'] = 'webservices/Car/paidStatus';


$route['addPlate'] = 'webservices/Plate/addPlateWithPush';
$route['getPlates'] = 'webservices/Plate/getPlates';
$route['PlateLike'] = 'webservices/Plate/PlateLike';
$route['PlateFav'] = 'webservices/Plate/addFav';
$route['favPlates'] = 'webservices/Plate/favPlates';
$route['likePlates'] = 'webservices/Plate/likePlates';
$route['blockPlate'] = 'webservices/Plate/blockPlate';
$route['editPlate'] = 'webservices/Plate/editPlatenew';
$route['reportPlate'] = 'webservices/Plate/reportplate';
$route['GetCity'] = 'webservices/Plate/GetCity';
$route['getPlateAllCategory'] = 'webservices/Plate/getPlateAllCategory';
$route['getPlateCategory'] = 'webservices/Plate/getPlateCategory';
$route['GetHistory'] = 'webservices/Plate/GetHistory';


$route['PlatesBySorting'] = 'webservices/Plate/PlatesBySorting';
$route['PlatesByFilter'] = 'webservices/Plate/PlatesByFilter';
$route['plateTables'] = 'webservices/Plate/plateTables';
$route['plateTablesKeywords'] = 'webservices/Plate/plateTablesKeywords';
$route['addPlatenew'] = 'webservices/Plate/addPlatenew';
$route['addPlateneww'] = 'webservices/Plate/addPlatenewnew';
$route['addPlateWithPush'] = 'webservices/Plate/addPlateWithPush';
$route['getPlatesbyImage'] = 'webservices/Plate/getPlatesbyImage';


/*
 *  Adminpanel Routing Start
 */
$route['admin'] = 'admin_panel/admin';
$route['admin/home'] = 'admin_panel/admin/dashboard';
$route['admin/allUsers'] = 'admin_panel/admin/allUsers';
$route['admin/viewuser'] = 'admin_panel/admin/viewuser';
$route['admin/allCars'] = 'admin_panel/admin/allCars';
$route['admin/inactiveCars'] = 'admin_panel/admin/inactiveCars';
$route['admin/activeCars'] = 'admin_panel/admin/activeCars';
$route['admin/viewcar'] = 'admin_panel/admin/viewcar';
$route['admin/editcar'] = 'admin_panel/admin/editcar';
$route['admin/deleteCar'] = 'admin_panel/admin/deleteCar';
$route['admin/addMake'] = 'admin_panel/admin/addMake';
$route['admin/editMake'] = 'admin_panel/admin/editMake';
$route['admin/deleteMake/(:num)'] = 'admin_panel/admin/deleteMake/$1';
$route['admin/addYear'] = 'admin_panel/admin/addYear';
$route['admin/addModel'] = 'admin_panel/admin/addModel';
$route['admin/allMake'] = 'admin_panel/admin/allMake';
$route['admin/allYear'] = 'admin_panel/admin/allYear';
$route['admin/allModel'] = 'admin_panel/admin/allModel';
$route['admin/getyearbymake'] = 'admin_panel/admin/getyearbymake';
$route['admin/getmodelbyyear'] = 'admin_panel/admin/getmodelbyyear';
$route['admin/get_user'] = 'admin_panel/admin/get_user';
$route['admin/allMYM'] = 'admin_panel/admin/allMakeYearModel';
$route['admin/dashboard'] = 'admin_panel/admin/dashboard';
$route['admin/logout'] = 'admin_panel/admin/logout';
$route['admin/paymentSetting'] = 'admin_panel/admin/paymentSetting';
$route['admin/paymentModeSetting'] = 'admin_panel/admin/paymentModeSetting';
$route['admin/changePassword'] = 'admin_panel/admin/changePassword';
$route['admin/allPlates'] = 'admin_panel/admin/allPlates';
$route['admin/activePlates'] = 'admin_panel/admin/activePlates';
$route['admin/inactivePlates'] = 'admin_panel/admin/inactivePlates';
$route['admin/viewplate'] = 'admin_panel/admin/viewplate';
$route['admin/deleteplate'] = 'admin_panel/admin/deleteplate';
$route['admin/editplate'] = 'admin_panel/admin/editplate';
$route['admin/plateStatus'] = 'admin_panel/admin/plateStatus';
$route['admin/carStatus'] = 'admin_panel/admin/carStatus';
$route['admin/reportCar'] = 'admin_panel/admin/reportCar';
$route['admin/reportPlate'] = 'admin_panel/admin/reportPlate';
$route['admin/gen_notification'] = 'admin_panel/admin/gen_notification';
$route['test/sendPush'] = 'admin_panel/test/sendPush';
/*
 *  Adminpanel Routing End
 */

$route['admin/test'] = 'admin_panel/admin/test';
