<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller 
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model'); 
        $this->load->library('email');
        //$this->load->helper('aes'); 
    }
    /*
     * Modified By      :Amit Kumar
     * Modified Date    :11Jan,16
     * Description      : Function used for user registration 
     */


    public function userSignUp()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['email']              = $json['email'];
            $document['loginType']          = $json['loginType'];
            $loginType                      = $json['loginType'];
            $document['socialId']           = $json['socialId'];
            $isSuccess                      = false;
            $obj                            = new user_model;
            if($loginType=='1' or $loginType=='2' or $loginType=='3')
            {

                $isUserExistS                    = $obj->isUserExistS($document);
                //echo $isUserExistS; die;
                if(!$isUserExistS)
                {
                    $isSuccess      = true;
                    $message        = "User registered successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "User already registered";
                    $data           = array();
                } 
            }
            else
            {
                $isUserExist                    = $obj->isUserExist($document);
                //echo $isUserExist; die;
                if(!$isUserExist)
                {
                    $isSuccess      = true;
                    $message        = "User registered successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "User already registered";
                    $data           = array();
                } 
            }        
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }






    /*
     * Modified By      :Amit Kumar
     * Modified Date    :11Jan,16
     * Description      : Function used to Create Profile
     */

    public function createProfile()
    {            
        $json                               = file_get_contents('php://input');     
$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
$txt = $json;
fwrite($myfile, "\n". $txt);
fclose($myfile);		
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();

            $document['email']              = $json['email'];
            $document['name']               = $json['name'];
            $document['mobile']             = $json['mobile'];
            $document['gender']             = $json['gender'];
            $document['country']            = $json['country'];
            $document['socialId']           = $json['socialId'];
            $document['loginType']          = $json['loginType'];
            $loginType                      = $document['loginType'];
            $document['password']           = md5($json['password']);
            $document['deviceToken']        = $json['deviceToken'];
            $document['deviceType']         = $json['deviceType'];

            $profileImage                   = $json['profileImage'];

            $isSuccess                      = false;
            $obj                            = new user_model;


            if($loginType=='1' or $loginType=='2' or $loginType=='3')
            {

                $user                        = $obj->isUserExistS($document);

                if(!$user)
                {
                 $results                        = $obj->createProfile($document);
                    if($results)
                    { 
                        $user           =   $obj->getProfile($results);

                      

                        $isSuccess      = true;
                        $message        = "Profile created successfully";
                        $data           = $user;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Profile could not create";
                        $data           = array();
                    }
            
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Profile already exist";
                    $data           = array();
                }
            }
            else
            {

                $user                        = $obj->isUserExist($document);
                
                if(!$user)
                {
                   

                    if($profileImage==NULL or $profileImage=="")
                        {  $profileImage = "";  
                        }
                    else{  
                             
                           $document['profileImage']       = $obj->makeBase64Img($profileImage);
                           
                           //$txt     = makeBase64Img($profileImage);
                        }
                           // print_r($document);die();
                    $results                        = $obj->createProfile($document);
                    if($results)
                    { 
                        $user           =   $obj->getProfile($results);
						//print_r($user);die();
                        $user['profileImage'] =  "http://fourthscreenlabs.com/amit/motary/profileImages/".$user['profileImage'];

                        $isSuccess      = true;
                        $message        = "Profile created successfully";
                        $data           = $user;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Profile could not create";
                        $data           = array();
                    }
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Profile already exist";
                    $data           = array();
                }
            }
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }





    public function UserVerifyCode()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();
            $document['email']              = $json['email'];
            $document['verificationCode']   = $json['otp'];
            //$document['deviceToken']        = $json['deviceToken'];
            $isSuccess                      = false;
            $message                        = "";
            $obj                            = new user_model;          
            $results                        = $obj->UserVerifyCode($document);             
            if($results==1)
            {
                $isSuccess      = true;
                $message        = "OTP verified successfully";
                $data           = array();
            }
            elseif($results==2)
            {
                $isSuccess      = false;
                $message        = "OTP mismatched";
                $data           = array();
            }
            elseif($results==3)
            {
                $isSuccess      = false;
                $message        = "user not exist";
                $data           = array();
            }
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"Message"=>$message,"Result"=>$data));
    }
    /*
     * Modified By      :Raman Chauhan
     * Modified Date    :11Jan,16
     * Description      : Function used for Valet login
     */


    public function userLogin()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();
            $document['loginType']          = $json['loginType'];
            $loginType                      = $json['loginType'];
            $document['socialId']           = $json['socialId'];
            $document['email']              = $json['email'];
            $document['password']           = md5($json['password']);//md5($json['password']); 
            $isSuccess                      = false;
            $message                        = "";
            $obj                            = new user_model;   



            if($loginType=='1' or $loginType=='2' or $loginType=='3')
            {
                $isUserExistS                    = $obj->isUserExistS($document);

                if($isUserExistS)
                {
                    $user                    = $obj->userLoginS($document); 
                    //print_r($user);
                     //echo die();
                    if($user)
                    {
                        $isSuccess      = true;
                        $message        = "Login successfully";
                        $messageCode    =  "1";
                        $data           = $user;
                        //print_r($data); die;
                    }
                   /* else
                    {
                        $isSuccess      = false;
                        $message        = "Wrong password!";
                        $messageCode    =  "2";
                        $data           = array();
                    }
                    */
                }
                else
                {
                        $isSuccess      = false;
                        $message        = "User not exist!";
                        $messageCode    =  "3";
                        $data           = array();
                }
            }
            else
            {
                
                $isUserExist                    = $obj->isUserExist($document);

                if($isUserExist)
                {
                    $user                    = $obj->userLogin($document); 
                    //print_r($user);
                    //echo die();
                    if($user)
                    {
                        $isSuccess      = true;
                        $message        = "Login successfully";
                        $messageCode    =  "1";
                        $user['profileImage'] = "http://fourthscreenlabs.com/amit/motary/profileImages/".$user['profileImage'];
                        $data           = $user;
                        //pre($user); die();
                        //print_r($data); die;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Wrong password!";
                        $messageCode    =  "2";
                        $data           = array();
                    }
                }
                else
                {
                        $isSuccess      = false;
                        $message        = "User not exist!";
                        $messageCode    =  "3";
                        $data           = array();
                }
            }
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"Message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }




    /*
     * Modified By      :Amit kUmar
     * Modified Date    :11Jan,16
     * Description      :Function used to Forget Password 
     */
   /*
    public function forgetPassword()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['email']              = $json['email'];

            $isSuccess                      = false;
            $obj                            = new user_model;
            $isUserExist                    = $obj->isUserExist($document);
            if($isUserExist)
            {
               // echo $isUserExist; die;
                $user                       = $obj->getProfile($isUserExist);
                //echo($user['password']);
                //print_r($user); die;
                $password=$document['password'] = $user['password'];
                $email  = $document['email'];
                
                $config  = array();
                $config['protocol'] = 'mail';
                $this->load->library('email');
                $config = array();
                $config['useragent']           = "CodeIgniter";
                $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
                $config['protocol']            = "smtp";
                $config['smtp_host']           = "localhost";
                $config['smtp_port']           = "25";
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['newline']  = "\r\n";
                $config['wordwrap'] = TRUE;
                $this->email->initialize($config);
                $this->email->from($email, 'Motary App');
                $this->email->to('rudra11.raman@gmail.com');
                $msg = "Message from Motary Your password is $password";
                $this->email->subject('Motary App:Your Password');
                $this->email->message($msg);
                if($this->email->send())
                {
                        $to = $email;
                        $msg = "Your password is ".$password;
                        $sub = "Your Password from motary";
                        $headers = 'From: Motary <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

                    mail($to,$sub,$msg,$headers);
                
                    $isSuccess      = true;
                    $message        = "Password sent to mail successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "sorry, password could not sent";
                    $data           = array();
                    //echo $this->email->print_debugger();
                }
                
            }
            else
            {
                $isSuccess      = false;
                $message        = "User not exist";
                $data           = array();
            }         
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

*/

    public function forgetPasswordByOTP()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['email']              = $json['email'];
            $document['verificationCode']   = rand(1000,9999);

            $isSuccess                      = false;
            $obj                            = new user_model;
            $isUserExist                    = $obj->isUserExist($document);
            if($isUserExist)
            {
                $updateProfile                    = $obj->updateProfile($document);

                    $to = $document['email'];
                    $msg = "Your OTP is ".$document['verificationCode'];
                    $sub = "Your OTP from motary";
                    $headers = 'From: Motary <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

                    $sentmail = mail($to,$sub,$msg,$headers);
                if(!empty($sentmail))
                {
                    $isSuccess      = true;
                    $message        = "OTP sent to your mail successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "sorry, OTP could not sent";
                    $data           = array();
                }
                
            }
            else
            {
                $isSuccess      = false;
                $message        = "User not exist";
                $data           = array();
            }         
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

    /*
     * Modified By      :Amit kUmar
     * Modified Date    :11Jan,16
     * Description      :Function used to change Password 
     */
    public function changePassword()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['email']              = $json['email'];
            $document['password']           = md5($json['newPassword']);

            $isSuccess                      = false;
            $obj                            = new user_model;


            $isUserExist                    = $obj->isUserExist($document);

            if($isUserExist)
            {
                $results                    = $obj->changePassword($document);
                if(!empty($results))
                {  
                    $isSuccess      = true;
                    $message        = "Password changed successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "password could not change";
                    $data           = array();
                }
            }
            else
            {
                $isSuccess      = false;
                $message        = "User not exist";
                $data           = array();
            }         
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }



}