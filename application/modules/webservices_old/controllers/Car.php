<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car extends MX_Controller 
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('Car_model'); 
        $this->load->library('email');
        //$this->load->helper('aes'); 
    }
    /*
     * Modified By      :Amit Kumar
     * Modified Date    :14Jan,16
     * Description      : Function used for user registration 
     */

public function carTables()
    {             
        $json                               = file_get_contents('php://input'); 
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();

            $document['table']              = $json['table'];

            if($document['table']=="caryear")
            {
                $document['make_id']       = $json['make_id'];
            }
            if($document['table']=="carmodel")
            {
                $document['makeyear_id']   = $json['makeyear_id'];
            }

            $isSuccess                      = false;
            $obj                            = new Car_model;


            $carTables                      = $obj->carTables($document);
            $isSuccess      = true;
            $message        = "Result return successfully";
            $data                           = $carTables;
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

public function carTablesKeywords()
    {             
        $json                               = file_get_contents('php://input'); 
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();

            $document['table']              = $json['table'];
            $document['keyword']            = $json['keyword'];

            $isSuccess                      = false;
            $obj                            = new Car_model;


            $carTablesKeywords              = $obj->carTablesKeywords($document);
            $isSuccess                      = true;
            $message                        = "Result return successfully";
            $data                           = $carTablesKeywords;
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }


public function addCar()
    {             
        $json                               = file_get_contents('php://input'); 
        if(is_json($json))
        {
            //$json                           = json_decode($json,true);
            $document                       = array();

            $document['userId']             = $_REQUEST['userId']; 
            $document['carCondition']       = $_REQUEST['carCondition'];
            $document['carMake']            = $_REQUEST['carMake'];
            $document['carYear']            = $_REQUEST['carYear'];
            $document['carModel']           = $_REQUEST['carModel'];
            $document['carPrice']           = $_REQUEST['carPrice'];
            $document['carTrans']           = $_REQUEST['carTrans'];
            $document['carFuel']            = $_REQUEST['carFuel'];
            $document['carCity']            = $_REQUEST['carCity'];
            $document['carMileage']         = $_REQUEST['carMileage'];
            $document['carCylinders']       = $_REQUEST['carCylinders'];
            $document['carColor']           = $_REQUEST['carColor'];
            $document['carComments']        = $_REQUEST['carComments'];

            $document['latitude']           = $_REQUEST['latitude'];
            $document['longitude']          = $_REQUEST['longitude'];
            $document['address']            = $_REQUEST['address'];
            $document['name']               = $_REQUEST['name'];
            $document['email']              = $_REQUEST['email'];
            $document['phone']              = $_REQUEST['phone'];
            
            $obj                            = new Car_model;

            if(isset($_FILES['carImage1']['name'])){

                $document['carImage1']   =   $obj->uploadCardImage("carImage1");
            }else{

                $document['carImage1']   =   "";
            }
            if(isset($_FILES['carImage2']['name'])){

                $document['carImage2']   =   $obj->uploadCardImage("carImage2");
            }else{

                $document['carImage2']   =   "";
            }
            if(isset($_FILES['carImage3']['name'])){

                $document['carImage3']   =   $obj->uploadCardImage("carImage3");
            }else{

                $document['carImage3']   =   "";
            }
            if(isset($_FILES['carImage4']['name'])){

                $document['carImage4']   =   $obj->uploadCardImage("carImage4");
            }else{

                $document['carImage4']   =   "";
            }

            $isSuccess                      = false;

            $addCar                    = $obj->addCar($document);
            if($addCar)
            {
            $isSuccess      = true;
            $message        = "Car added successfully";
            $data           = $addCar;
            }
            else
            {
             $isSuccess      = false;
            $message        = "Car could not add";
            $data           = array();   
            }                            
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
public function editCar()
    {             
        $json                               = file_get_contents('php://input'); 
        if(is_json($json))
        {
            //$json                           = json_decode($json,true);
            $document                       = array();

            $document['carId']             = $_REQUEST['carId']; 
            $document['userId']             = $_REQUEST['userId']; 

            $document['carCondition']       = $_REQUEST['carCondition'];
            $document['carMake']            = $_REQUEST['carMake'];
            $document['carYear']            = $_REQUEST['carYear'];
            $document['carModel']           = $_REQUEST['carModel'];
            $document['carPrice']           = $_REQUEST['carPrice'];
            $document['carTrans']           = $_REQUEST['carTrans'];
            $document['carFuel']            = $_REQUEST['carFuel'];
            $document['carCity']            = $_REQUEST['carCity'];
            $document['carMileage']         = $_REQUEST['carMileage'];
            $document['carCylinders']       = $_REQUEST['carCylinders'];
            $document['carColor']           = $_REQUEST['carColor'];
            $document['carComments']        = $_REQUEST['carComments'];
            
            $document['latitude']           = $_REQUEST['latitude'];
            $document['longitude']          = $_REQUEST['longitude'];
            $document['address']            = $_REQUEST['address'];
            $document['name']               = $_REQUEST['name'];
            $document['email']              = $_REQUEST['email'];
            $document['phone']              = $_REQUEST['phone'];
            $isSuccess                      = false;
            $obj                            = new Car_model;

            $query = '';

            if(isset($_FILES['carImage1']['name'])){

                $document['carImage1']   =   $obj->uploadCardImage("carImage1");
                //$query = $query."carImage1='".$document['carImage1']."' AND ";

            }
            if(isset($_FILES['carImage2']['name'])){

                $document['carImage2']   =   $obj->uploadCardImage("carImage2");
                //$query = $query."carImage2='".$document['carImage1']."' AND ";

            }
            if(isset($_FILES['carImage3']['name'])){

                $document['carImage3']   =   $obj->uploadCardImage("carImage3");
                //$query = $query."carImage3='".$document['carImage1']."' AND ";
            }
            if(isset($_FILES['carImage4']['name'])){

                $document['carImage4']   =   $obj->uploadCardImage("carImage4");
                //$query = $query."carImage4='".$document['carImage1']."' AND ";
            }
           // echo $query;die();
   
            $editCar                    = $obj->editCar($document);
            
            if($editCar)
            {
            $isSuccess      = true;
            $message        = "Car edited successfully";
            $data           = $editCar;
            }
            else
            {
             $isSuccess      = false;
            $message        = "Car could not update";
            $data           = array();   
            }
                
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }



 public function carLike()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['userId']             = $json['userId'];
            $document['carId']              = $json['carId'];
            $document['isLike']             = $json['isLike'];
            
            $islike                         = $json['isLike'];

            $isSuccess                      = false;
            $obj                            = new Car_model;

            $searchIsLike                   = $obj->searchIsLike($document); //from Carlike

            //print_r($searchIsLike); die;
            if(!empty($searchIsLike))
            { 
                $searchIsLike                   = $searchIsLike['isLike'];
                $updateIsLike                   = $obj->updateIsLike($document);
                $countTotalLikes                = $obj->countTotalLikes($document);
                $document['totalLikes']         = $countTotalLikes; 
                $countTotalDislikes             = $obj->countTotalDislikes($document);
                $document['totalDislikes']      = $countTotalDislikes;
                //$updateTotalDislikes            = $obj->updateTotalDislikes($document);
 
                if($searchIsLike==0 and $islike==0)
                {
                    $isSuccess                  = false;
                    $message                    = "Already Disiked";
                }
                else if($searchIsLike==1 and $islike==1)
                {
                    $isSuccess                  = false;
                    $message                    = "Already Liked";
                }
                else if($searchIsLike==0 and $islike==1)
                {
                    //$updateTotalDislike                   = $obj->updateTotalDislike($document);
                    //$countTotalLikes                        = $obj->countTotalLikes($document);
                    $document['totalLikes']                 = $countTotalLikes+1;
                    $updateTotalLikes                       = $obj->updateTotalLikes($document);
                    
                    //$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes']              = $countTotalDislikes-1;
                    $updateTotalDislikes                    = $obj->updateTotalDislikes($document);
                    
                    $isSuccess                              = true;
                    $message                                = "Liked successfully";
                }
                else //if($searchIsLike==1 and $islike==0)
                {
                    //$countTotalLikes                        = $obj->countTotalLikes($document);
                    $document['totalLikes']                 = $countTotalLikes-1;
                    $updateTotalLikes                       = $obj->updateTotalLikes($document);
                    
                    //$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes']              = $countTotalDislikes+1;
                    $updateTotalDislikes                    = $obj->updateTotalDislikes($document);
                    
                    $isSuccess                              = true;
                    $message                                = "Dislike successfully";
                }

                //$getCarLikes                                = $obj->getCarLikes($document['carId']); //from Carlike
                //$data                                       = $getCarLikes;
            }
            else
            {
                $insertIsLike                               = $obj->insertIsLike($document);
                //$getCarLikes                              = $obj->getCarLikes($document['carId']); //from Carlike
                $countTotalLikes                            = $obj->countTotalLikes($document);
                $countTotalDislikes                         = $obj->countTotalDislikes($document);
                if($islike==1)
                {
                    $document['totalLikes']                 = $countTotalLikes+1;
                    $updateTotalLikes                       = $obj->updateTotalLikes($document);
                    
                    $isSuccess                              = true;
                    $message                                = "Liked successfully";
                }
                else 
                {
                    //$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes']              = $countTotalDislikes+1;
                    $updateTotalDislikes                    = $obj->updateTotalDislikes($document);
                    
                    $isSuccess                              = true;
                    $message                                = "Disliked successfully";
                }
            }
                $getCarLikes                                = $obj->getCarLikes($document['carId']); //from Carlike
                $data                                       = $getCarLikes;

           // $data = $document['totalLikes'];
        }                       
        else
        {
            $isSuccess                                      = false;
            $message                                        = "Invaid Json Input";
            $data                                           = array();
        }
        
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }


 public function addFav()
    {             
        $json                                   = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                               = json_decode($json,true);
            $document                           = array();
            $document['carId']                  = $json['carId'];
            $document['userId']                 = $json['userId'];
            $document['isFav']                  = $json['isFav'];
          
            $isSuccess                          = false;
            $obj                                = new Car_model;
  
            $isFavExist                         = $obj->isFavExist($document);
            if(!empty($isFavExist))
            {
                $updateFav                      = $obj->updateFav($document);
                if($updateFav)
                {
                    $isSuccess                  = true;
                    $message                    = "Fav Updated successfully";
                    $data                       = array();
                }
                else
                {
                    $isSuccess                  = false;
                    $message                    = "Fav couldn't update";
                    $data                       = array();    
                }
            }
            else
            {
            
                $addFav                    = $obj->addFav($document);
                
                if($addFav)
                {
                $isSuccess                  = true;                   
                $message                    = "Fav added successfully";                    
                $data                       = array();
                }
                else
                {
                $isSuccess                  = false;
                $message                    = "Fav couldn't add";
                $data                       = array();   
                }
            }
        }
        else
        {
            $isSuccess                      = false;
            $message                        = "Invaid Json Input";
            $data                           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }


 public function get_Cars()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {

            $json                           = json_decode($json,true);
            $document                       = array();

            $document['carId']              = $json['carId'];
            $document['userId']             = $json['userId'];

            $document['sortType']           = $json['sortType'];
            $document['sortOrder']          = $json['sortOrder'];

            $document['carCondition']       = $json['carCondition'];
            $document['carMake']            = $json['carMake'];
            $document['carModel']           = $json['carModel'];
            $document['carColor']           = $json['carColor'];
            $document['carYear']            = $json['carYear'];
            $document['carTrans']           = $json['carTrans'];
            $document['carFuel']            = $json['carFuel'];
            $document['carCity']            = $json['carCity'];
            $document['carPrice']           = $json['carPrice'];
            $document['carMileage']         = $json['carMileage'];
            $document['carCylinders']       = $json['carCylinders'];
            $document['count']              = $json['count'];

            $document['keyword']             = $json['keyword'];

            $isSuccess                      = false;
            $obj                            = new Car_model;
                
            $get_Cars                    = $obj->get_Cars($document);
            if($get_Cars)
            {
            $isSuccess      = true;
            $message        = "Car list displayed successfully ";
            $TotalCount     = $get_Cars[0]['TotalCount'];
            $data           = $get_Cars;
            }
            else
            {
             $isSuccess      = false;
            $message        = "No car found";
            $data           = array();
            $TotalCount     = $get_Cars[0]['TotalCount'];
            }               
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
            $TotalCount     = 0;
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"TotalCount"=>$TotalCount,"Result"=>$data));
    }


 public function favCars()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();

            $document['userId']             = $json['userId'];
            $document['count']              = $json['count'];

            $isSuccess                      = false;
            $obj                            = new Car_model;
                
            $favCars                    = $obj->favCars($document);
            
            if($favCars)
            {
            $isSuccess      = true;
            $message        = "Car list displayed successfully ";
            $data           = $favCars;
            $TotalCount     = $favCars[0]['TotalCount'];
            }
            else
            {
             $isSuccess     = false;
            $message        = "no any car found";
            $TotalCount     = 0;
            $data           = array();   
            }                          
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"TotalCount"=>$TotalCount,"Result"=>$data));
    }


 public function getAdminSetting()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();

            $isSuccess                      = false;
            $obj                            = new Car_model;
                
            $getAdminSetting                = $obj->getAdminSetting($document);
            
            if($getAdminSetting)
            {
            $isSuccess      = true;
            $message        = "Admin payment option";
            $data           = $getAdminSetting;
            }
            else
            {
            $isSuccess     = false;
            $message        = "no payment option found";
            $data           = array();   
            }                              
        }                       
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

}

