<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
    }
    function getUsers()
    {
        //connect to mongodb collection (i.e., table) named as ‘tbl_users’
        $collection = $this->mongo_db->db->selectCollection('tbl_users');
        //selecting records from the collection - tbl_users
        $result     = $collection->find();
        $results    = array();
        foreach($result as $data) 
        {  
          //display the records  
          $results[]= $data;
          
        } 
        return $results;
    }


    function userSignUp($document)
    {
        //print_r($document); die;
        $query  = $this->db->insert('users', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }


    function createProfile($document)
    {
        //print_r($document); die;
        $this->db->insert('users', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

    function getProfile($id)
    { 
        //echo $id; die;
        $this->db->where('userId', $id); 
        $details=$this->db->get('users')->row_array();

        //$details=$this->db->result();
        //print_r($details); die;
        //print_r($details); die;
        return $details;
    }


    function updateProfile($document)
    { 
        $this->db->where('email', $document['email']); 
        $query  = $this->db->update('users', $document);
        if (!empty($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    function isUserExist($document)
    {
        
        $this->db->where('email', $document['email']); 
        $result = $this->db->get('users')->row_array();       
        if (!empty($result))
        {
            return $result['userId'];
        }
        else
        {
            return false;
        }
    }

    function isUserExistS($document)
    {
        
        $this->db->where('socialId', $document['socialId']); 
       // $this->db->where('loginType', $document['loginType']); 
        $result = $this->db->get('users')->row_array();       
        if (!empty($result))
        {
            return $result['userId'];
        }
        else
        {
            return false;
        }
    }

    function makeBase64Img($image){

         $data = str_replace(" ","+",$image);
         $data = base64_decode($data);
         $im = imagecreatefromstring($data);
         $fileName = rand(5, 115).time().".png";
         $imageName = "/home/fourtfsa/public_html/amit/motary/profileImages/".$fileName; 
               

         if ($im !== false) {

          imagepng($im,$imageName);

          imagedestroy($im);

         }else {
          echo 'An error occurred.';
         } 
         
         return $fileName;
        }

    function UserVerifyCode($document)
    {
        
        $this->db->where('email', $document['email']); 
        $result = $this->db->get('users')->row_array();

        if(!empty($result))
        {
            if($result['verificationCode']==$document['verificationCode'])
            {                    
                return 1;
            }
            else
            {
                return 2;
            }
        }
        else
        {
            return 3;
        }
    }


    function userLogin($document)
    {
        $this->db->where('email', $document['email']); 
        $this->db->where('password', $document['password']); 
        $result = $this->db->get('users')->row_array(); 
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    function userLoginS($document)
    {
        //$this->db->where('loginType', $document['loginType']); 
        $this->db->where('socialId', $document['socialId']); 
        //$this->db->where('password', $document['password']); 
        $result = $this->db->get('users')->row_array(); 
        //echo $result['userId']; die;
        //print_r($result);
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

/*
    function forgetPassword($document)
    {
    echo $document['password']; die;

        $this->mail()
        if($this->db->affected_rows())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

*/
    function changePassword($document)
    {
        $this->db->where('email', $document['email']); 
        $query = $this->db->update('users', $document);
        if(!empty($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}