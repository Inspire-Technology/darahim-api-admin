<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Car_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
    }


    function carTables($document) 
    { 
        $table = $document['table'];
        if($table=="caryear"){ 
                                $this->db->where('make_id', $document['make_id']);             
                                $this->db->order_by('year', 'asc');            
                             }
        if($table=="carmodel"){ 
                                $this->db->where('makeyear_id', $document['makeyear_id']);    
                              }

        $result = $this->db->get($table)->result_array();       
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    function carTablesKeywords($document) 
    {
        $table = $document['table'];
        $keyword = $document['keyword'];

        if($table=="caryear") { 
            
            $this->db->select("year");
            $this->db->distinct();
            $this->db->where("(`year` LIKE '$keyword%')"); }
        else { 
            $this->db->select("name");
            $this->db->distinct();
            $this->db->where("(`name` LIKE '$keyword%')");  }

        $result = $this->db->get($table)->result_array();       
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return array();
        }
    }



    function addCar($document) 
    {
        $query  = $this->db->insert('car', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

    function uploadFile($tmpName,$fileName) 
    {
        $fileName = $fileName.time();
        $target = "motary/carImages/".$fileName;

        $res = move_uploaded_file($tmpName, $target);
        if($res)
        {
            return $target;
        }
        else
        {
            return false;
        }
    }

    function uploadCardImage($key)
    {
        $files = $_FILES[$key];
        $cnt = count($files['name']);
                                              
        if ($files['error'] == 0) 
        {
            $name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time().basename($_FILES[$key]["name"]);
            $target_file = $name.$file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file) )
            {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;

    }

     function getCar($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('car')->row_array();       
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    function getCarLikes($carId)
    {
        
        $this->db->where('carId', $carId); 
        $result = $this->db->get('car')->row_array();       
        if (!empty($result))
        {
            return $result['totalLikes'];
        }
        else
        {
            return false;
        }
    }

    

    function editCar($document) 
    {
        $this->db->where('carId', $document['carId']); 
        //$this->db->where('userId', $document['userId']);
        $query  = $this->db->update('car', $document);

        if($query=='true')
        {
            return $document['carId'];
        }
        else
        {
            return false;
        }
    }


    function makeBase64Img($image){

         $data = str_replace(" ","+",$image);
         $data = base64_decode($data);
         $im = imagecreatefromstring($data);
         $fileName = rand(5, 115).time().".png";
         $imageName = "/opt/lampp/htdocs/motary/carImages/".$fileName; 
         if ($im !== false) {
          imagepng($im,$imageName);

          imagedestroy($im);

         }else {
          echo 'An error occurred.';
         } 
         
         return $fileName;
        }


     function searchIsLike($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_carlikes')->row_array();       
        if (!empty($result))
        {
            //return $result['isLike'];
            return $result;
        }
        else
        {
            return false;
        }
    }

    function insertIsLike($document) 
    {
        $query  = $this->db->insert('mt_carlikes', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }
    
    function updateIsLike($document) 
    {
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']);
        $query  = $this->db->update('mt_carlikes', $document);
        //$id = $this->db->insert_id();
        if($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    function updateTotalLikes($document) 
    {
       // unset($document['isLike']);
       // unset($document['userId']);
        //print_r($document); die();
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('carId', $document['carId']); 
        $query  = $this->db->update('car',array('totalLikes'=>$document['totalLikes']));

        if($query=='true')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function updateTotalDislikes($document) 
    {
        //unset($document['isLike']);
        //unset($document['userId']);
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('carId', $document['carId']); 
        $query  = $this->db->update('car', array( 'totalDislikes' => $document['totalDislikes']) );
        //$query  = $this->db-> last_query();
        if($query=='true')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function countTotalLikes($document)
    {
        $this->db->where('carId', $document['carId']); 
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_car')->row_array();       
        if (!empty($result))
        {
            return $result['totalLikes'];
        }
        else
        {
            return false;
        }
    }
    
    function countTotalDislikes($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_car')->row_array();       
        if (!empty($result))
        {
            return $result['totalDislikes'];
        }
        else
        {
            return false;
        }
    }

 function addFav($document)
    {
        //print_r($document); die;
        $query  = $this->db->insert('carfavourite', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

 function updateFav($document)
    {
        $this->db->where('carId', $document['carId']); 
        $query  = $this->db->update('carfavourite', $document);
        //$id = $this->db->insert_id();
        if($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

 function isFavExist($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']); 
        $result = $this->db->get('carfavourite')->row_array();       
        if (!empty($result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    

    function CarsBySorting($document)
    {
        if($document['type']!="")
            { 
                $this->db->order_by($document['type'], $document['order']); 
            }  
        

        $result = $this->db->get('car')->result_array();
        $count = count($result);
        for($i=0;$i<$count;$i++){
          $result[$i]['carImage']="localhost/motary/carImages/".$result[$i]['carImage'];

        }
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    function CarsByFilter($document)
    {

        if($document['sortType']!="")           {   $this->db->order_by($document['sortType'], $document['sortOrder']);     } 

        if($document['carCondition']!="")   {    $this->db->where('carCondition', $document['carCondition']);   }  
        if($document['carMake']!="")        {    $this->db->where('carMake',    $document['carMake']);          }  
        if($document['carModel']!="")       {    $this->db->where('carModel',   $document['carModel']);         }  
        if($document['carColor']!="")       {    $this->db->where('carColor',   $document['carColor']);         }  
        if($document['carYear']!="")        {    $this->db->where('carYear',    $document['carYear']);          }  
        if($document['carTrans']!="")       {    $this->db->where('carTrans',   $document['carTrans']);         }  
        if($document['carFuel']!="")        {    $this->db->where('carFuel',    $document['carFuel']);          }  
        if($document['carCity']!="")        {    $this->db->where('carCity',    $document['carCity']);          }  
        if($document['carPrice']!="")       {    $this->db->where('carPrice',   $document['carPrice']);         }  
        if($document['carMileage']!="")     {    $this->db->where('carMileage', $document['carMileage']);       }  
        if($document['carCylinders']!="")   {    $this->db->where('carCylinders',$document['carCylinders']);    }  

        $result = $this->db->get('car')->result_array();
        $count = count($result);
        for($i=0;$i<$count;$i++){
          $result[$i]['carImage']="localhost/motary/carImages/".$result[$i]['carImage'];

        }
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    function commonFuction($document)
    {
                

         if($document['carId']!="")          { $this->db->where('carId', $document['carId']);    }
        // if($document['userId']!="")       { $this->db->where('userId', $document['userId']);  }
        if($document['sortType']=="")        { $this->db->order_by('createDate','desc');         }

        if($document['sortType']!="")       {   $this->db->order_by($document['sortType'], $document['sortOrder']);     } 

        if($document['carCondition']!="")   {    $this->db->where('carCondition', $document['carCondition']);   }  
        if($document['carMake']!="")        {    $this->db->where('carMake',    $document['carMake']);          }  
        if($document['carModel']!="")       {    $this->db->where('carModel',   $document['carModel']);         }  
        if($document['carColor']!="")       {    $this->db->where('carColor',   $document['carColor']);         }  
        if($document['carYear']!="")        {    $this->db->where('carYear',    $document['carYear']);          }  
        if($document['carTrans']!="")       {    $this->db->where('carTrans',   $document['carTrans']);         }  
        if($document['carFuel']!="")        {    $this->db->where('carFuel',    $document['carFuel']);          }  
        if($document['carCity']!="")        {    $this->db->where('carCity',    $document['carCity']);          }  
        if($document['carPrice']!="")       {    $this->db->where('carPrice',   $document['carPrice']);         }  
        if($document['carMileage']!="")     {    $this->db->where('carMileage', $document['carMileage']);       }  
        if($document['carCylinders']!="")   {    $this->db->where('carCylinders',$document['carCylinders']);    }  

        if($document['keyword']!="")           
            { 
                $keyword=$document['keyword'];
                $this->db->where("(`carCondition` LIKE '%$keyword%' OR `carMake` LIKE '%$keyword%' OR `carModel` LIKE '%$keyword%' OR  `carColor` LIKE '%$keyword%' OR `carYear` LIKE '%$keyword%' OR `carTrans` LIKE '%$keyword%' OR `carFuel` LIKE '%$keyword%' OR `carCity` LIKE '%$keyword%' OR `carPrice` LIKE '%$keyword%' OR `carMileage` LIKE '%$keyword%' OR `carCylinders` LIKE '%$keyword%' )");
            }

    }

    function get_Cars($document)
    {  
        $this->commonFuction($document);
        //$last_query = $this->db->last_query();     echo $last_query;  die();
        $res = $this->db->get('car')->result_array();
        $TotalCount = count($res);

        $this->db->limit(10, $document['count']);
        $this->commonFuction($document);
                //$last_query = $this->db->last_query();     echo $last_query;  die();

        $result = $this->db->get('car')->result_array();
        $coun = count($result);
        for($i=0;$i<$coun;$i++)
        {
          if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage1']; }
          if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage2']; }
          if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage3']; }
          if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage4']; }
          $result[$i]['TotalCount'] = $TotalCount;

            $result[$i]['userLike']=0;
            $result[$i]['userFav']=0;
            if($document['userId']!="")       
            {   
                $query = $this->db->query("Select carId from mt_carlikes where isLike!=0 and userId='".$document['userId']."'");
                $carids= $query->result_array();

                $query1 = $this->db->query("Select carId from mt_carfavourite where isFav='1' and userId='".$document['userId']."'");
                $carfavs= $query1->result_array();
//pre($carids);


                $Likecarids = array();
                foreach($carids as $entry)
                {
                    $Likecarids[] = $entry['carId'];
                }
                $Favcarids = array();
                foreach($carfavs as $entry1)
                {
                    $Favcarids[] = $entry1['carId'];
                }

                if (in_array($result[$i]['carId'], $Likecarids))  { $result[$i]['userLike'] =1 ; } 
                if (in_array($result[$i]['carId'], $Favcarids)) { $result[$i]['userFav']  =1 ; } 
            }
        }
        if (!empty($result))
        { 
            return $result;
        }
        else
        {
            return false;
        }
    }


    function favCars($document)
    {
        $this->db->where('carfavourite.userId',$document['userId']);  
        $this->db->where('carfavourite.isFav','1'); 
        $TotalCount = $this->db->count_all_results('carfavourite');

        $this->db->join('carfavourite','car.carId = carfavourite.carId'); 

        if($document['count']!="") 
            {    
                $limitStart = $document['count'];
                $limitEnd   = $limitStart+10;

                $this->db->limit($limitEnd, $limitStart); 
            }

        $result = $this->db->get('car')->result_array();
        $count = count($result);
        for($i=0;$i<$count;$i++){
          $result[$i]['carImage1']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage1'];
          $result[$i]['carImage2']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage2'];
          $result[$i]['carImage3']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage3'];
          $result[$i]['carImage4']="http://fourthscreenlabs.com/amit/motary/carImages/".$result[$i]['carImage4'];
          $result[$i]['TotalCount'] = $TotalCount;
        }


        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

 function getAdminSetting()
    {
        
        $this->db->select('paymentOption'); 
        $result = $this->db->get('adminSettings')->row_array();       
        if (!empty($result))
        {
            return $result['paymentOption'];
        }
        else
        {
            return false;
        }
    }



}
