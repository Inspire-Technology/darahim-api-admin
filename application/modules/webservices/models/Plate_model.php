<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Plate_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function findName2Id($table,$name)
    {
        $where = "name='".$name."' OR arabic_name='".$name."'";
        $this->db->select('id');
        $this->db->where($where);  
        $id = $this->db->get($table)->row_array();
        return $id;
    }
    
   
    
    function findId2Name($table,$id,$locale)
    {
        $this->db->select('name,arabic_name');
        $this->db->where('id',$id);  
        $name = $this->db->get($table)->row_array();
        if($locale == "ar") { $name['name'] = $name['arabic_name']; }
        return $name;
    }

    function addPlate($document) {

        if (isset($document['plateId']) and $document['plateId'] != "") {
            $oldPlateId = $document['plateId'];  //old plateId

            unset($document['plateId']);
            //insert new Edited PLate//
            $this->db->insert('plate', $document);
            //update status//
            $data = array('plateStatus' => 2);
            $this->db->where('plateId', $oldPlateId);
            $this->db->update('plate', $data);
            //get new edited Plate Id //
            $this->db->order_by('createDate', 'desc');
            $this->db->limit(1);
            $result = $this->db->get('plate')->row_array();
            $newPlateId = $result['plateId']; //new CarId;
            //get edited table already inserted records or not //
            $this->db->where('newPlateId', $oldPlateId);
            $rowexist = $this->db->get('platedit')->row_array();
            $data = array('oldPlateId' => $oldPlateId, 'newPlateId' => $newPlateId);

            if (count($rowexist) > 0) {
                $this->db->select('oldPlateId');
                $this->db->where('newPlateId', $oldPlateId);
                $id = $this->db->get('platedit')->row_array();
                //delte  first old record 
                $this->db->where('plateId', $id['oldPlateId']);
                $this->db->delete('plate');
                //update platedit table with new plateId
                $this->db->where('newPlateId', $oldPlateId);
                $id = $this->db->update('platedit', $data);
            } else {
                $id = $this->db->insert('platedit', $data);
            }
//            $this->db->where('plateId', $document['plateId']);
//            $id = $this->db->update('plate', $document);
        } else {
            //unset($document['modifyDate']);

            $this->db->where('plateCategory', $document['plateCategory']);
            $this->db->where('plateNumber', $document['plateNumber']);
            $this->db->where('plateCity', $document['plateCity']);
            $array = $this->db->get('plate')->result_array();
            if (count($array) >= 1) {
                return false;
            }
            $query = $this->db->insert('plate', $document);
            $id = $this->db->insert_id();
        }

        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    function uploadPlateImage($key) {
        $files = $_FILES[$key];
        $cnt = count($files['name']);
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {
            $name = "plateImages/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    function get_Plates($document) {
        //pre($document); die();
        //if ($document['createDate'] != "") { $this->db->having('createDate <', $document['createDate']); }
        if ($document['platePrice'] != "") {
            $this->db->where('platePrice =', $document['platePrice']);
        }

        if ($document['plateCity'] != "") {
            $this->db->where('plateCity = ', $document['plateCity']);
        }

        if ($document['plateCategory'] != "") {

            $this->db->where('plateCategory = ', $document['plateCategory']);
        }

        if ($document['plateNumber'] != "") {

            $this->db->where('plateNumber = ', $document['plateNumber']);
        }

        if ($document['plateId'] != "") {
            $this->db->where('plateId', $document['plateId']);
        }

        //if ($document['plateCondition'] != "") {  $this->db->where('plateCondition', $document['plateCondition']);  }

        if ($document['name'] != "") {
            $this->db->where('name', $document['name']);
        }

        if ($document['priceFrom'] != "") {
            if ($document['priceTo'] != "") {
                $this->db->where('platePrice >=', $document['priceFrom']);
                $this->db->where('platePrice <=', $document['priceTo']);
            } else {
                $this->db->where('platePrice >=', $document['priceFrom']);
            }
        } elseif ($document['priceTo'] != "") {
            $this->db->where('platePrice <=', $document['priceTo']);
        }

        if ($document['sortType'] != "") {
            if ($document['sortOrder'] != "") {
                $this->db->order_by($document['sortType'], $document['sortOrder']);
            } else {
                $this->db->order_by($document['sortType'], 'desc');
            }
        }

        // if ($document['userId'] != "") {  $this->db->where('userId', $document['userId']);}

        if ($document['keyword'] != "") {
           
            $sql = $this->db->query('select id from mt_plateCity where name = "'.$document["keyword"].'" or arabic_name="'.$document["keyword"].'"');
            $city = $sql->row_array();
            
            if($city['id'] !=0){$keyword1 = $city['id'];} else {$keyword1 = "0";}
          
            $where = 'plateCity="'.$keyword1.'" OR plateNumber="'.$document['keyword'].'"';
            $this->db->where($where);
            
        }

        $total = $this->db->get('plate')->result_array();
        $totalCount = count($total);


        //if ($document['createDate'] != "") { $this->db->having('createDate <', $document['createDate']); }
        if ($document['platePrice'] != "") {
            $this->db->where('platePrice =', $document['platePrice']);
        }

        if ($document['plateCity'] != "") {
            $this->db->where('plateCity = ', $document['plateCity']);
        }

        if ($document['plateCategory'] != "") {

            $this->db->where('plateCategory = ', $document['plateCategory']);
        }

        if ($document['plateNumber'] != "") {

            $this->db->where('plateNumber = ', $document['plateNumber']);
        }

        if ($document['plateId'] != "") {
            $this->db->where('plateId', $document['plateId']);
        }

        //if ($document['plateCondition'] != "") {  $this->db->where('plateCondition', $document['plateCondition']);  }

        if ($document['name'] != "") {
            $this->db->where('name', $document['name']);
        }

        if ($document['priceFrom'] != "") {
            if ($document['priceTo'] != "") {
                $this->db->where('platePrice >=', $document['priceFrom']);
                $this->db->where('platePrice <=', $document['priceTo']);
            } else {
                $this->db->where('platePrice >=', $document['priceFrom']);
            }
        } elseif ($document['priceTo'] != "") {
            $this->db->where('platePrice <=', $document['priceTo']);
        }

        if ($document['sortType'] != "") {
            if ($document['sortOrder'] != "") {
                $this->db->order_by($document['sortType'], $document['sortOrder']);
            } else {
                $this->db->order_by($document['sortType'], 'desc');
            }
        }

        //if ($document['userId'] != "") {  $this->db->where('userId', $document['userId']);}

        if ($document['keyword'] != "") {
            $sql = $this->db->query('select id from mt_plateCity where name = "'.$document['keyword'].'" or arabic_name="'.$document['keyword'].'"');
            $city = $sql->row_array();
            if($city['id'] !=0){$keyword1 = $city['id'];} else {$keyword1 = "0";}
          
            $where = 'plateCity="'.$keyword1.'" OR plateNumber="'.$document['keyword'].'"';
            $this->db->where($where);
        }
        $this->db->where('plateStatus', 1);
        $this->db->order_by('createDate', 'desc');
        $this->db->limit(10, $document['count']);
        $result = $this->db->get('plate')->result_array();
        for ($i = 0; $i < sizeof($result); $i++) {
            $result[$i]['totalCount'] = $totalCount;
            $plateId = $result[$i]['plateId'];
            $userId = $document['userId'];


            $this->db->where('plateId', $plateId);
            $this->db->where('userId', $userId);
            $this->db->where('isLike', 1);
            $row = $this->db->get('platelikes')->num_rows();

            if ($row > 0) {
                $result[$i]['userlike'] = 1;
            } else {

                $result[$i]['userlike'] = 0;
            }
        }

        for ($i = 0; $i < sizeof($result); $i++) {
            $userId = $result[$i]['userId'];
            $this->db->select('name,mobile,profileImage');
            $this->db->where('userId', $userId);
            $row = $this->db->get('users')->row_array();
            if (strpos($row['profileImage'], "http") or $row['profileImage'] != "") {
                
            } else {
                $row['profileImage'] = base_url("profileImages/" . $row['profileImage']);
            }


            $result[$i]['userDetail'] = $row;
        }




//        if ($document['plateId'] != "") {
//            $this->db->where('plateId', $document['plateId']);
//        }
//
//        if ($document['plateCity'] != "") {
//            $this->db->where('plateCity', $document['plateCity']);
//        }
//
//        $res = $this->db->get('plate')->result_array();
//        $TotalCount = count($res);
//
//        if ($document['plateId'] != "") {
//            $this->db->where('plateId', $document['plateId']);
//        }
//        if ($document['plateCity'] != "") {
//            $this->db->where('plateCity', $document['plateCity']);
//        }
//        $this->db->limit(10, $document['count']);
//        $result = $this->db->get('plate')->result_array();
//
//
//        $coun = count($result);
//        if ($coun > 0) {
//            $result[0]['totalCount'] = $TotalCount;
//        }
//
//        for ($i = 0; $i < $coun; $i++) {
//            echo $result[$i]['plateId'];
        /*
          $this->db->where('userId', $document['userId']);
          $this->db->where('plateId', $result[$i]['plateId']);
          $like = $this->db->get('platelikes')->result_array();

          if ($like) {
          $result[$i]['userlike'] = 1;
          } else {
          $result[$i]['userlike'] = 0;
          }

          $this->db->where('userId', $document['userId']);
          $this->db->where('plateId', $result[$i]['plateId']);
          $fav = $this->db->get('platefavourite')->result_array();

          if ($fav) {
          $result[$i]['userfav'] = 1;
          } else {
          $result[$i]['userfav'] = 0;
          } */
//        }

        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function getPlateCategory($document) {

        if ($document['city'] == 'Abu Dhabi' || $document['city'] == "أبوظبي") {
            $data = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            return $data;
        } else if ($document['city'] == 'Ajman' || $document['city'] == "عجمان") {
            $data = array('A', 'B', 'C', 'D', 'E');
            return $data;
        } else if ($document['city'] == 'Dubai' || $document['city'] == "دبي") {
            $data = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
            return $data;
        } else if ($document['city'] == 'Fujairah' || $document['city'] == "الفجيرة") {
            $data = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'K', 'M', 'P', 'R', 'S', 'T');
            return $data;
        } else if ($document['city'] == 'Ras Al Khaimah' || $document['city'] == "رأس الخيمة") {
            $data = array('A', 'B', 'C', 'D', 'I', 'K', 'M', 'N', 'S', 'V', 'Y');
            return $data;
        } else if ($document['city'] == 'Sharjah' || $document['city'] == "الشارقة") {
            $data = array('1', '2', '3');
            return $data;
        } else if ($document['city'] == 'Umm Al Quwain' || $document['city'] == "أم القيوين") {
            $data = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'X');
            return $data;
        } else {
            return false;
        }
    }

    function getPlateAllCategory($document) {
        $data = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        return $data;
    }

    function GetCityName($document) {
        if ($document['locale'] == "ar") {
            $data = array(
                'أبوظبي',
                'دبي',
                'الشارقة',
                'عجمان',
                'أم القيوين',
                'رأس الخيمة',
                'الفجيرة'
            );
        } else {
            $data = array(
                 'Abu Dhabi',
                'Dubai',
                'Sharjah',
                'Ajman',
                'Umm Al Quwain',
                'Ras Al Khaimah',
                'Fujairah'
            );
        }
        return $data;
    }

    function searchIsLike($document) {

        $this->db->where('plateId', $document['plateId']);
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_platelikes')->row_array();
        if (!empty($result)) {
            //return $result['isLike'];
            return $result;
        } else {
            return false;
        }
    }

    function updateIsLike($document) {
        $this->db->where('plateId', $document['plateId']);
        $this->db->where('userId', $document['userId']);
        $query = $this->db->update('mt_platelikes', $document);
        //$id = $this->db->insert_id();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function countTotalLikes($document) {
        $this->db->where('plateId', $document['plateId']);
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_plate')->row_array();
        if (!empty($result)) {
            return $result['totalLikes'];
        } else {
            return false;
        }
    }

    function countTotalDislikes($document) {

        $this->db->where('plateId', $document['plateId']);
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_plate')->row_array();
        if (!empty($result)) {
            return $result['totalDislikes'];
        } else {
            return false;
        }
    }

    function updateTotalLikes($document) {
        // unset($document['isLike']);
        // unset($document['userId']);
        //print_r($document); die();
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('plateId', $document['plateId']);
        $query = $this->db->update('plate', array('totalLikes' => $document['totalLikes']));

        if ($query == 'true') {
            return true;
        } else {
            return false;
        }
    }

    function updateTotalDislikes($document) {
        //unset($document['isLike']);
        //unset($document['userId']);
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('plateId', $document['plateId']);
        $query = $this->db->update('plate', array('totalDislikes' => $document['totalDislikes']));
        //$query  = $this->db-> last_query();
        if ($query == 'true') {
            return true;
        } else {
            return false;
        }
    }

    function insertIsLike($document) {
        $query = $this->db->insert('mt_platelikes', $document);
        $id = $this->db->insert_id();
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    function isFavExist($document) {
        $this->db->where('plateId', $document['plateId']);
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('platefavourite')->row_array();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function updateFav($document) {
        $this->db->where('plateId', $document['plateId']);
        $this->db->where('userId', $document['userId']);
        $query = $this->db->update('platefavourite', $document);
        //$id = $this->db->insert_id();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function addFav($document) {
        //print_r($document); die;
        $query = $this->db->insert('platefavourite', $document);
        $id = $this->db->insert_id();
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    function favPlates($document) {

        $this->db->where('userId', $document['userId']);
        $this->db->where('isFav', '1');
        $result = $this->db->get('platefavourite')->result_array();
//        $this->db->join('platelogo', 'plate.plateMake = platelogo.plateMake');
//
//        $this->db->join('platefavourite', 'plate.plateId = platefavourite.plateId');
//        $this->db->where('platefavourite.userId', $document['userId']);
//        $this->db->where('platefavourite.isFav', '1');
//
//        $res = $this->db->get('plate')->result_array();
//        $TotalCount = count($res);
//        //echo $this->db->last_query();
//
//        $this->db->select('platelogo.plateLogo,plate.plateId,plate.plateImage1,plate.plateImage2,plate.plateImage3,plate.plateImage4,plate.plateMake,plate.plateModel,plate.plateYear');
//        $this->db->where('platefavourite.userId', $document['userId']);
//        $this->db->where('platefavourite.isFav', '1');
//        $this->db->join('platefavourite', 'plate.plateId = platefavourite.plateId');
//        $this->db->join('platelogo', 'plate.plateMake = platelogo.plateMake');
//
//        $this->db->limit(10, $document['count']);
//
//        $result = $this->db->get('Plate')->result_array();
//        $count = count($result);
//        for ($i = 0; $i < $count; $i++) {
//
//            $result[$i]['plateLogo'] = strtolower($result[$i]['plateLogo']);
//            //if($result[$i]['logo']!=""){ $result[$i]['logo']="http://fourthscreenlabs.com/amit/motary/plateLogo/".$result[$i]['plateLogo']; }
//
//            if ($result[$i]['plateImage1'] != "") {
//                $result[$i]['plateImage1'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/plateLogo/" . $result[$i]['plateLogo'];
//            }
//            if ($result[$i]['plateImage2'] != "") {
//                $result[$i]['plateImage2'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/plateImages/" . $result[$i]['plateImage2'];
//            }
//            if ($result[$i]['plateImage3'] != "") {
//                $result[$i]['plateImage3'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/plateImages/" . $result[$i]['plateImage3'];
//            }
//            if ($result[$i]['plateImage4'] != "") {
//                $result[$i]['plateImage4'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/plateImages/" . $result[$i]['plateImage4'];
//            }
//            $result[$i]['TotalCount'] = $TotalCount;
//            unset($result[$i]['plateLogo']);
//        }
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function likePlates($document) {

        $this->db->where('userId', $document['userId']);
        $this->db->where('isLike', '1');
        $result = $this->db->get('platelikes')->result_array();

        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

//    function isFavModels($document) {
//        $this->db->where('plate.plateId', $document['plateId']);
//        $this->db->where('platefavourite.userId', $document['userId']);
//        $this->db->where('platefavourite.isFav', 1);
//        $this->db->join('platefavourite', 'plate.plateId = platefavourite.plateId');
//        $result = $this->db->get('plate')->result_array();
//        if (!empty($result)) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//    function getPlate($document) {
//        $this->db->select('plateId');
//        $this->db->where('plateId', $document['plateId']);
//        //$this->db->where('userId', $document['userId']);
//        $result = $this->db->get('plate')->row_array();
//        if (!empty($result)) {
//            return $result;
//        } else {
//            return false;
//        }
//    }

    function plateTables($document) {
        $table = $document['table'];
        if ($table == "plateyear") {
            $this->db->where('make_id', $document['make_id']);
            $this->db->order_by('year', 'desc');
        }
        if ($table == "platemodel") {
            $this->db->where('makeyear_id', $document['makeyear_id']);
        }

        $result = $this->db->get($table)->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function plateTablesKeywords($document) {
        $table = $document['table'];
        $keyword = $document['keyword'];

        if ($table == "plateyear") {

            $this->db->select("year");
            $this->db->distinct();
            $this->db->where("(`year` LIKE '$keyword%')");
            $this->db->order_by('year', 'desc');
        } else {
            $this->db->select("name");
            $this->db->distinct();
            $this->db->where("(`name` LIKE '$keyword%')");
        }

        $result = $this->db->get($table)->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    function uploadFile($tmpName, $fileName) {
        $fileName = $fileName . time();
        $target = "motary/plateImages/" . $fileName;

        $res = move_uploaded_file($tmpName, $target);
        if ($res) {
            return $target;
        } else {
            return false;
        }
    }

    function uploadPlatedImage($key) {
        $files = $_FILES[$key];
        $cnt = count($files['name']);
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {
            $name = "plateImages/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/plateImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    function uploadSearchImage($key) {  //echo $key; die();
        $files = $_FILES[$key];
        $cnt = count($files['name']);
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {

            $name = "/var/www/html/motaryAdmin/imageSearch/uploads/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/plateImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    function getPlateLikes($plateId) {

        $this->db->where('plateId', $plateId);
        $result = $this->db->get('plate')->row_array();
        if (!empty($result)) {
            return $result['totalLikes'];
        } else {
            return false;
        }
    }

    function blockPlate($document) {
        $this->db->where('plateId', $document['plateId']);
        $this->db->where('userId', $document['userId']);
        $query = $this->db->update('mt_plate', $document);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function editPlate($document) {
        $this->db->where('plateId', $document['plateId']);
        //$this->db->where('userId', $document['userId']);
        $query = $this->db->update('plate', $document);

        if ($query == 'true') {
            return $document['plateId'];
        } else {
            return false;
        }
    }

    function isReported($document) {
        $this->db->where('plateId', $document['plateId']);
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('reportedPlate')->row_array();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function reportPlate($document) {
        $query = $this->db->insert('reportedPlate', $document);
        $id = $this->db->insert_id();
        //$q = $this->db->last_query();
        //echo $q; die;
        if ($id) {
            return true;
        } else {
            return false;
        }
    }

    function getSoldPlates($document) {
        //$this->db->select('plateId,platePrice');
        $this->db->where('isSold', 1);
        $this->db->order_by('createDate', 'desc');
        if ($document['createDate'] != "") {
            $this->db->having('createDate <', $document['createDate']);
        }
        $result = $this->db->get('plate')->result_array();

        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function GetHistory($document) {
        //$this->db->select('plateId,platePrice');
        $this->db->where('userId', $document['userId']);
        $this->db->order_by('createDate', 'desc');
        $this->db->limit(10, $document['count']);
        $result = $this->db->get('plate')->result_array();

        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function makeBase64Img($image) {

        $data = str_replace(" ", "+", $image);
        $data = base64_decode($data);
        $im = imagecreatefromstring($data);
        $fileName = rand(5, 115) . time() . ".png";
        $imageName = "plateImages/" . $fileName;

        if ($im !== false) {
            imagepng($im, $imageName);

            imagedestroy($im);
        } else {
            echo 'An error occurred.';
        }

        return $fileName;
    }

    function makeBase64ImgSearch($image) {
        //echo $image; die();
        $data = str_replace(" ", "+", $image);
        $data = base64_decode($data);
        $im = imagecreatefromstring($data);
        $fileName = rand(5, 115) . time() . ".png";
        $imageName = "imageSearched/" . $fileName;

        if ($im !== false) {
            imagepng($im, $imageName);

            imagedestroy($im);
        } else {
            echo 'An error occurred.';
        }

        return $fileName;
    }

    function pushNotificationAddPlate($document) {

        $this->db->select('users.deviceType,users.deviceToken');
        $this->db->where('plate.plateModel', $document);
        $this->db->where('plate.isSold', 1);

        $this->db->join('platefavourite', 'plate.plateId = platefavourite.plateId');
        $this->db->join('users', 'users.userId = platefavourite.userId');
        $result = $this->db->get('plate')->result_array();
        //$q = $this->db->last_query();
        //echo $q; die();

        $message = "Push Message";
       
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $value) {
                $deviceType = $result[$i]['deviceType'];
                $deviceToken = $result[$i]['deviceToken'];
                $generatePush = generatePush($deviceType, $deviceToken, $message);
                return true;
                $i++;
            }
        } else {
            return false;
        }
    }

    function PlatesBySorting($document) {
        if ($document['type'] != "") {
            $this->db->order_by($document['type'], $document['order']);
        }


        $result = $this->db->get('plate')->result_array();
        $count = count($result);
        for ($i = 0; $i < $count; $i++) {
            $result[$i]['plateImage'] = "localhost/motary/plateImages/" . $result[$i]['plateImage'];
        }
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function PlatesByFilter($document) {

        if ($document['sortType'] != "") {
            $this->db->order_by($document['sortType'], $document['sortOrder']);
        }

        if ($document['plateCondition'] != "") {
            $this->db->where('plateCondition', $document['plateCondition']);
        }
        if ($document['plateMake'] != "") {
            $this->db->where('plateMake', $document['plateMake']);
        }
        if ($document['plateModel'] != "") {
            $this->db->where('plateModel', $document['plateModel']);
        }
        if ($document['plateColor'] != "") {
            $this->db->where('plateColor', $document['plateColor']);
        }
        if ($document['plateYear'] != "") {
            $this->db->where('plateYear', $document['plateYear']);
        }
        if ($document['plateTrans'] != "") {
            $this->db->where('plateTrans', $document['plateTrans']);
        }
        if ($document['plateFuel'] != "") {
            $this->db->where('plateFuel', $document['plateFuel']);
        }
        if ($document['plateCity'] != "") {
            $this->db->where('plateCity', $document['plateCity']);
        }
        if ($document['platePrice'] != "") {
            $this->db->where('platePrice', $document['platePrice']);
        }
        if ($document['plateMileage'] != "") {
            $this->db->where('plateMileage', $document['plateMileage']);
        }
        if ($document['plateCylinders'] != "") {
            $this->db->where('plateCylinders', $document['plateCylinders']);
        }

        $result = $this->db->get('plate')->result_array();
        $count = count($result);
        for ($i = 0; $i < $count; $i++) {
            $result[$i]['plateImage'] = "localhost/motary/plateImages/" . $result[$i]['plateImage'];
        }
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function getPlatesbyImageTags($result) {
        $myarray = array();
        //$result=$keywords;
        $tags = explode(' ', $result['name']);
        $tagCount = count($tags);
        $keyword = $tags[0];
        $this->db->limit(10, $result['count']);
        $this->db->where("(`plateComments` LIKE '%$keyword%' OR plateCondition` LIKE '%$keyword%' OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' OR `plateTrans` LIKE '%$keyword%' OR `plateFuel` LIKE '%$keyword%' OR `plateCity` LIKE '%$keyword%' OR `platePrice` LIKE '%$keyword%' OR `plateMileage` LIKE '%$keyword%' OR `plateCylinders` LIKE '%$keyword%' )");
        for ($i = 1; $i < $tagCount; $i++) {
            $keyword = $tags[$i];
            $this->db->or_where("(`plateComments` LIKE '%$keyword%' OR plateCondition` LIKE '%$keyword%' OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' OR `plateTrans` LIKE '%$keyword%' OR `plateFuel` LIKE '%$keyword%' OR `plateCity` LIKE '%$keyword%' OR `platePrice` LIKE '%$keyword%' OR `plateMileage` LIKE '%$keyword%' OR `plateCylinders` LIKE '%$keyword%' )");
        }
        $plate = $this->db->get('plate')->result_array();

        $totalCount = count($plate);
        $plate['totalCount'] = $totalCount;
        $plate['tags'] = $result['name'];

        return $plate;
    }

    function fact($n, $token) {
        $response1['status'] = $n;
        if ($response1['status'] != "completed") {
            time_sleep_until(microtime(true) + 4);
            //echo "Amfdit";
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.cloudsightapi.com/image_responses/" . $token,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: CloudSight CwqFqm2Ai7eweMSs-yB0Jg",
                    "cache-control: no-cache",
                    "postman-token: 83626698-8975-2a7f-73fe-5a0ca7757710"
                ),
            ));

            $response1 = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;
            }
            $response1 = json_decode($response1, true);
            // pre($response1);
        }
        return $response1;
    }

    public function getPlatesbyImage() {
        $key = "plateImage";
        $isSuccess = false;
        $obj = new plate_model;
        $dummyRes = array();
        $dummyRes['count'] = $_REQUEST['count'];

        if (isset($_REQUEST['tags']) and $_REQUEST['tags'] != "") {
            $dummyRes['name'] = $_REQUEST['tags'];
        } else {

            $image_url = $this->uploadSearchImage($key);

            $image_url = base_url() . "imageSearch/uploads/" . $image_url;

            //echo $image_url; //die();

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://cloudsightapi.com/image_requests",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[locale]\"\r\n\r\nen-US\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[remote_image_url]\"\r\n\r\nhttp://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/imageSearch/uploads/1459257944plate.jpeg\r\n-----011000010111000001101001--",
                CURLOPT_HTTPHEADER => array(
                    "authorization: OAuth oauth_consumer_key='CwqFqm2Ai7eweMSs-yB0Jg',oauth_token='370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',oauth_signature_method='HMAC-SHA1',oauth_timestamp='1459258038',oauth_nonce='akodW7',oauth_version='1.0',oauth_signature='VhjdbmPWz3aiNeNpRJf%2FO9wHQMI%3D'",
                    "cache-control: no-cache",
                    "content-type: multipart/form-data; boundary=---011000010111000001101001",
                    "postman-token: 97345529-8e69-0759-ae39-e95693ab8410"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;
            }

            $response = json_decode($response, true);
            // $response1['status'] = "Amit";

            $newtimestmp = time() + 12;
            $dummyRes = "amit";
            while (($dummyRes = $this->fact($dummyRes, $response['token'])) != "completed") {
                //pre($dummyRes);
                if ($dummyRes['status'] != "completed") {

                    $timestmp = time();
                    if ($timestmp > $newtimestmp) {
                        break;
                    }
                    continue;
                } else {
                    break;
                }
            }
            //pre($dummyRes); 
        }




        $getPlatesbyImage = $obj->getPlatesbyImageTags($dummyRes);

        $isSuccess = true;
        $message = "Result return successfully";
        $data = $getPlatesbyImage;
        //pre($data); die();
        //echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"totalCount"=>$data['totalCount'],"tags"=>$data['tags'],"Result"=>$data));
    }

    function commonFuction($document) {   //pre($document);
        // die();
        $result = array();
        if (isset($document['plateImage']) and $document['plateImage'] != "") {
            // $this->db->where('plateId', $document['plateId']); 
            $image_url = $this->makeBase64ImgSearch($document['plateImage']);
            $image_url = base_url() . "imageSearched/" . $image_url;
            //$image_url = "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/imageSearched/371459346426.png";
            //echo $image_url; //die();

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://cloudsightapi.com/image_requests",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[locale]\"\r\n\r\nen-US\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[remote_image_url]\"\r\n\r\n" . $image_url . "\r\n-----011000010111000001101001--",
                CURLOPT_HTTPHEADER => array(
                    "authorization: OAuth oauth_consumer_key='CwqFqm2Ai7eweMSs-yB0Jg',oauth_token='370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',oauth_signature_method='HMAC-SHA1',oauth_timestamp='1459258038',oauth_nonce='akodW7',oauth_version='1.0',oauth_signature='VhjdbmPWz3aiNeNpRJf%2FO9wHQMI%3D'",
                    "cache-control: no-cache",
                    "content-type: multipart/form-data; boundary=---011000010111000001101001",
                    "postman-token: 97345529-8e69-0759-ae39-e95693ab8410"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response; die();
                $response = json_decode($response, true);
                // $response1['status'] = "Amit";
                //pre($response); die();   
                $newtimestmp = time() + 20;
                $dummyRes = "amit";
                while (($dummyRes = $this->fact($dummyRes, $response['token'])) != "completed") {
                    //pre($dummyRes);
                    if ($dummyRes['status'] != "completed") {

                        $timestmp = time();
                        if ($timestmp > $newtimestmp) {
                            break;
                        }
                        continue;
                    } else {
                        break;
                    }
                }
            }

            //pre($dummyRes); 
            //echo $dummyRes['name']; die();
            //$myarray = array();
            //$result=$keywords;
            //pre($plateImageTags); die();
            //$result[0]['plateImageTags']   = $dummyRes['name'];
            //$result[0]['plateImageTags']."<br>"; //die();

            $tags = explode(' ', $dummyRes['name']);
            $tagCount = count($tags);
            $querylike = "";
            for ($i = 1; $i < $tagCount; $i++) {
                $keyword = $tags[$i];
                //echo "keyword".$keyword."<br>";
                $keyword = explode("'", $keyword);
                $keyword = $keyword[0];

                $querylikenew = "OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%'";
                $querylike = $querylike . $querylikenew;
                //$this->db->or_where("(`plateComments` LIKE '%$keyword%' OR plateCondition` LIKE '%$keyword%' OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' OR `plateTrans` LIKE '%$keyword%' OR `plateFuel` LIKE '%$keyword%' OR `plateCity` LIKE '%$keyword%' OR `platePrice` LIKE '%$keyword%' OR `plateMileage` LIKE '%$keyword%' OR `plateCylinders` LIKE '%$keyword%' )");
            }
            //echo "querylike".$querylike."<br>";
            $keyword = $tags[0];
            $keyword = explode("'", $keyword);
            $keyword = $keyword[0];

            $this->db->where("(`plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' $querylike )");
            return($dummyRes['name']);
        } else if (isset($document['plateImageTags']) and $document['plateImageTags'] != "") {
            $tags = explode(' ', $document['plateImageTags']);
            $tagCount = count($tags);
            //$keyword = $tags[0];
            //echo "keyword".$keyword."<br>tagCount".$tagCount."<br>";
            //$this->db->limit(10, $result['count']); 
            $querylike = "";
            for ($i = 1; $i < $tagCount; $i++) {
                $keyword = $tags[$i];

                $keyword = explode("'", $keyword);
                $keyword = $keyword[0];
                //echo "keyword".$keyword."<br>";

                $querylikenew = "OR `plateComments` LIKE '%$keyword%' OR plateCondition` LIKE '%$keyword%' OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' OR `plateTrans` LIKE '%$keyword%' OR `plateFuel` LIKE '%$keyword%' OR `plateCity` LIKE '%$keyword%' OR `platePrice` LIKE '%$keyword%' OR `plateMileage` LIKE '%$keyword%' OR `plateCylinders` LIKE '%$keyword%'";
                $querylike = $querylike . $querylikenew;
            }
            //echo "querylike".$querylike."<br>";
            $keyword = $tags[0];
            $keyword = explode("'", $keyword);
            $keyword = $keyword[0];
            $this->db->where("(`plateComments` LIKE '%$keyword%' OR plateCondition` LIKE '%$keyword%' OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' OR `plateTrans` LIKE '%$keyword%' OR `plateFuel` LIKE '%$keyword%' OR `plateCity` LIKE '%$keyword%' OR `platePrice` LIKE '%$keyword%' OR `plateMileage` LIKE '%$keyword%' OR `plateCylinders` LIKE '%$keyword%' $querylike )");
        }

        if ($document['plateId'] != "") {
            $this->db->where('plateId', $document['plateId']);
        }
        // if($document['userId']!="")      { $this->db->where('userId', $document['userId']);  }
//        if ($document['sortType'] == "") {
//            $this->db->order_by('createDate', 'desc');
//        }
//        if ($document['sortType'] != "") {
//            $this->db->order_by($document['sortType'], $document['sortOrder']);
//        }
//
//        if ($document['plateCondition'] != "") {
//            $this->db->where('plateCondition', $document['plateCondition']);
//        }
//        if ($document['plateMake'] != "") {
//            $this->db->where('plateMake', $document['plateMake']);
//        }
//        if ($document['plateModel'] != "") {
//            $this->db->where('plateModel', $document['plateModel']);
//        }
//        if ($document['plateColor'] != "") {
//            $this->db->where('plateColor', $document['plateColor']);
//        }
//        if ($document['plateYear'] != "") {
//            $this->db->where('plateYear', $document['plateYear']);
//        }
//        if ($document['plateTrans'] != "") {
//            $this->db->where('plateTrans', $document['plateTrans']);
//        }
//        if ($document['plateFuel'] != "") {
//            $this->db->where('plateFuel', $document['plateFuel']);
//        }
//        if ($document['plateCity'] != "") {
//            $this->db->where('plateCity', $document['plateCity']);
//        }
//        if ($document['platePrice'] != "") {
//            $this->db->where('platePrice', $document['platePrice']);
//        }
//        if ($document['plateMileage'] != "") {
//            $this->db->where('plateMileage', $document['plateMileage']);
//        }
//        if ($document['plateCylinders'] != "") {
//            $this->db->where('plateCylinders', $document['plateCylinders']);
//        }
//
//        if ($document['priceFrom'] != "" and $document['priceTo'] != "") {
//            $to = $document['priceTo'];
//            $from = $document['priceFrom'];
//            $this->db->where("platePrice BETWEEN '$from' and '$to'");
//        }
//        if ($document['priceFrom'] != "") {
//            $this->db->where('platePrice >=', $document['priceFrom']);
//        }
//        if ($document['priceTo'] != "") {
//            $this->db->where('platePrice <=', $document['priceTo']);
//        }
//
//        if ($document['keyword'] != "") {
//            $keyword = $document['keyword'];
//            $this->db->where("(`plateComments` LIKE '%$keyword%' OR plateCondition` LIKE '%$keyword%' OR `plateMake` LIKE '%$keyword%' OR `plateModel` LIKE '%$keyword%' OR  `plateColor` LIKE '%$keyword%' OR `plateYear` LIKE '%$keyword%' OR `plateTrans` LIKE '%$keyword%' OR `plateFuel` LIKE '%$keyword%' OR `plateCity` LIKE '%$keyword%' OR `platePrice` LIKE '%$keyword%' OR `plateMileage` LIKE '%$keyword%' OR `plateCylinders` LIKE '%$keyword%' )");
//        }
    }

    function getAdminSetting() {
        $this->db->select('paymentOption');
        $result = $this->db->get('adminSettings')->row_array();
        if (!empty($result)) {
            return $result['paymentOption'];
        } else {
            return false;
        }
    }

//////////////////-------------Start--------Motary M4--------Start---------------///////////////////////
//////////////////--------------End--------Motary M4--------End---------------///////////////////////
}
