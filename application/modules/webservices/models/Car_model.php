<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Car_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
       // $this->load->model('push_helper'); 
    
       $this->color = $this->db->get('carcolour')->result_array(); 
      
    }

    function carTables($document) 
    { 
        $table = $document['table'];
        if($table=="caryear"){ 
                                $this->db->where('make_id', $document['make_id']);             
                                $this->db->order_by('year', 'desc');            
                             }
        if($table=="carmodel"){ 
                                $this->db->where('makeyear_id', $document['makeyear_id']);    
                              }

        $result = $this->db->get($table)->result_array();       
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return array();
        }
    }

    function carTablesKeywords($document) 
    {
        $table = $document['table'];
        $keyword = $document['keyword'];

        if($table=="caryear") { 
            
            $this->db->select("year");
            $this->db->distinct();
            $this->db->where("(`year` LIKE '$keyword%')"); 
            $this->db->order_by('year', 'desc');            
        }
        else { 
            //$this->db->select("name");
            $this->db->distinct();
            $this->db->where("(`name` LIKE '$keyword%')");  
            
            
        }

        $result = $this->db->get($table)->result_array(); 
        
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return array();
        }
    }



    function addCar($document) 
    {
        //pre($document);die();
        if(isset($document['carCondition']) && $document['carCondition'] != "")
        {
            $carcondition = $document['carCondition'];
            $where = "name='".$carcondition."'  OR arabic_name='".$carcondition."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carcondition')->row_array();
           $document['carCondition'] = $id['id'];
        }
         if(isset($document['carTrans']) && $document['carTrans'] != "")
        {
            $carTrans = $document['carTrans'];
            $where = "name='".$carTrans."'  OR arabic_name='".$carTrans."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('cartransmission')->row_array();
            $document['carTrans'] = $id['id'];
        }
         if(isset($document['carFuel']) && $document['carFuel'] != "")
        {
            $carFuel = $document['carFuel'];
            $where = "name='".$carFuel."'  OR arabic_name='".$carFuel."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carfuel')->row_array();
            $document['carFuel'] = $id['id'];
        }
        if(isset($document['carCity']) && $document['carCity'] != "")
        {
            $carCity = $document['carCity'];
            $where = "name='".$carCity."'  OR arabic_name='".$carCity."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carCity')->row_array();
            if(!empty($id))
            {
            $document['carCity'] = $id['id'];
            } 
            else 
            {
                
                $document['carCityName'] = $document['carCity'];
                $document['carCity'] = '0';
            }
        }
       if(isset($document['carColor']) && $document['carColor'] != "")
        {
        
            $carColor = $document['carColor'];
            $where = "name='".$carColor."'  OR arabic_name='".$carColor."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carcolour')->row_array();
            if(!empty($id)) {
            $document['carColor'] = $id['id'];
            $document['carColorName'] = "";
            } else {
                
                $document['carColorName'] = $carColor;
                $document['carColor'] = '0';
            }
        }
       
        $query  = $this->db->insert('car', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }



function editProfile($document) {
	    $this->db->where('carId', $document['carId']);
	    $query  = $this->db->update('car', $document);
        
        if($query)
        {
            return $document['carId'];
        }
        else
        {
            return false;
        }
	}

    function uploadFile($tmpName,$fileName) 
    {
        $fileName = $fileName.time();
        $target = "motary/carImages/".$fileName;

        $res = move_uploaded_file($tmpName, $target);
        if($res)
        {
            return $target;
        }
        else
        {
            return false;
        }
    }

    function uploadCardImage($key)
    {
        $files = $_FILES[$key];
        $cnt = count($files['name']);
       //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) 
        {
            $name = "carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time().basename($_FILES[$key]["name"]);
            $target_file = $name.$file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file) )
            {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;

    }

    function uploadSearchImage($key)
    {  //echo $key; die();
        $files = $_FILES[$key];
        $cnt = count($files['name']);
       //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) 
        {

            $name = "/var/www/html/motaryAdmin/imageSearch/uploads/";//.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time().basename($_FILES[$key]["name"]);
            $target_file = $name.$file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file) )
            {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;

    }

     function getCar($document)
    {
        
        $this->db->select('carModel'); 
        $this->db->where('carId', $document['carId']); 
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('car')->row_array();       
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    function getCarLikes($carId)
    {
        
        $this->db->where('carId', $carId); 
        $result = $this->db->get('car')->row_array();       
        if (!empty($result))
        {
            return $result['totalLikes'];
        }
        else
        {
            return false;
        }
    }


    function editCar($document) 
    {
            //pre($document);exit();
            $oldCarId  = $document['carId'];  //old carId
            unset($document['carId']);
            //insert new Edited car//
             if(isset($document['carCondition']) && $document['carCondition'] != "")
        {
            $carcondition = $document['carCondition'];
            $where = "name='".$carcondition."'  OR arabic_name='".$carcondition."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carcondition')->row_array();
           $document['carCondition'] = $id['id'];
        }
         if(isset($document['carTrans']) && $document['carTrans'] != "")
        {
             
            $carTrans = $document['carTrans'];
            $where = "name='".$carTrans."'  OR arabic_name='".$carTrans."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('cartransmission')->row_array();
            $document['carTrans'] = $id['id'];
            
        }
         if(isset($document['carFuel']) && $document['carFuel'] != "")
        {
            $carFuel = $document['carFuel'];
            $where = "name='".$carFuel."'  OR arabic_name='".$carFuel."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carfuel')->row_array();
            $document['carFuel'] = $id['id'];
        }
        if(isset($document['carCity']) && $document['carCity'] != "")
        {
            $carCity = $document['carCity'];
            $where = "name='".$carCity."'  OR arabic_name='".$carCity."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carCity')->row_array();
            
            if(!empty($id)) {
            $document['carCity'] = $id['id'];
            $document['carCityName'] = "";
            } else {
                
                $document['carCityName'] = $document['carCity'];
                $document['carCity'] = '0';
            }
        }
       if(isset($document['carColor']) && $document['carColor'] != "")
        {
            $carColor = $document['carColor'];
            $where = "name='".$carColor."'  OR arabic_name='".$carColor."'";
            $this->db->select('id');
            $this->db->where($where);  
            $id = $this->db->get('carcolour')->row_array();
            if(!empty($id)) {
            $document['carColor'] = $id['id'];
            $document['carColorName'] = "";
            } else {
                
                $document['carColorName'] = $document['carColor'];
                $document['carColor'] = '0';
            }
        }
	    $this->db->insert('car' , $document);
            //update status//
            $data = array('carStatus'=>2);
            $this->db->where('carId',$oldCarId);
            $this->db->update('car',$data);
            //get new edited car Id //
            $this->db->order_by('createDate', 'desc');
            $this->db->limit(1);
            $result = $this->db->get('car')->row_array();
            $newCarId = $result['carId']; //new CarId;
            
            //get edited table already inserted records or not //
            $this->db->where('newCarId',$oldCarId);
            $rowexist = $this->db->get('caredit')->row_array();
            $data = array('oldCarId'=>$oldCarId,'newCarId'=>$newCarId);
            if(count($rowexist) > 0) {
                $this->db->select('oldCarId');
                $this->db->where('newCarId',$oldCarId);
                $id = $this->db->get('caredit')->row_array();
                //delte  first old record 
                 $this->db->where('carId', $id['oldCarId']);
                 $this->db->delete('car'); 
                //update caredit table with new carId
                $this->db->where('newCarId',$oldCarId);
                $this->db->update('caredit',$data);
            } else {
              $id =  $this->db->insert('caredit',$data);
            }
            
            //update reported car id ///
            $this->db->where('CarId',$oldCarId);
            $this->db->where('UserId',$document['userId']);
            $result = $this->db->get('reportedCars')->row_array();
            //echo $result['CarId'];
            if(count($result['CarId']) > 0) 
            {
                $data = array('CarId'=>$newCarId);
                $this->db->where('UserId',$document['userId']);
                $this->db->where('CarId',$oldCarId);
                $this->db->update('reportedCars',$data);
            }
            
//	   $this->db->where('carId', $document['carId']); 
//        //$this->db->where('userId', $document['userId']);
//        $query  = $this->db->update('car', $document);

       
            return $newCarId;
       
    }


    function makeBase64Img($image)
    {
         $data = str_replace(" ","+",$image);
         $data = base64_decode($data);
         $im = imagecreatefromstring($data);
         $fileName = rand(5, 115).time().".png";
         $imageName = "carImages/".$fileName; 

         if ($im !== false) {
          imagepng($im,$imageName);

          imagedestroy($im);

         }else {
          echo 'An error occurred.';
         } 
         
         return $fileName;
    }


    function makeBase64ImgSearch($image)
    {
        //echo $image; die();
         $data = str_replace(" ","+",$image);
         $data = base64_decode($data);
         $im = imagecreatefromstring($data);
         $fileName = rand(5, 115).time().".png";
         $imageName = "imageSearched/".$fileName; 

         if ($im !== false) {
          imagepng($im,$imageName);

          imagedestroy($im);

         }else {
          echo 'An error occurred.';
         } 
         
         return $fileName;
        }



     function searchIsLike($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_carlikes')->row_array();       
        if (!empty($result))
        {
            //return $result['isLike'];
            return $result;
        }
        else
        {
            return false;
        }
    }

    function insertIsLike($document) 
    {
        $query  = $this->db->insert('mt_carlikes', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }
    
    function updateIsLike($document) 
    {
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']);
        $query  = $this->db->update('mt_carlikes', $document);
        //$id = $this->db->insert_id();
        if($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function blockCar($document) 
    {
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']);
        $query  = $this->db->update('mt_car', $document);
        if($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function pushNotificationAddCar($document) 
    {
         
        $this->db->select('users.deviceType,users.deviceToken,users.isPushNotification,users.locale');
        $this->db->where('car.carModel', $document); 
        //$this->db->where('car.isSold', 1); 
	$this->db->where('users.isPushNotification', 1); 
        $this->db->join('carfavourite','car.carId = carfavourite.carId'); 
        $this->db->join('users','users.userId = carfavourite.userId'); 
        $result = $this->db->get('car')->result_array();
        
        
        if (!empty($result))
        {
            $i = 0 ;
            foreach ($result as $key => $value) 
            {
                $message = array();
                $message['message']="Someone has just added your favourite car model  please check it out..";
                if($result[$i]['locale'] == "ar") {$message['message']="قم بالاطلاع على إعلان السيارة المفضلة عبر تطبيق دراهم "; }
                $message['pushType'] = 2;
                $message = json_encode($message);
                $deviceType=$result[$i]['deviceType'];
                $deviceToken=$result[$i]['deviceToken'];
                $generatePush       = generatePush($deviceType,$deviceToken,$message);
                $i++;
            }
        }
        else
        {
            return false;
        }
        
    }    

    function updateTotalLikes($document) 
    {
       // unset($document['isLike']);
       // unset($document['userId']);
        //print_r($document); die();
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('carId', $document['carId']); 
        $query  = $this->db->update('car',array('totalLikes'=>$document['totalLikes']));

        if($query=='true')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function updateTotalDislikes($document) 
    {
        //unset($document['isLike']);
        //unset($document['userId']);
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('carId', $document['carId']); 
        $query  = $this->db->update('car', array( 'totalDislikes' => $document['totalDislikes']) );
        //$query  = $this->db-> last_query();
        if($query=='true')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function countTotalLikes($document)
    {
        $this->db->where('carId', $document['carId']); 
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_car')->row_array();       
        if (!empty($result))
        {
            return $result['totalLikes'];
        }
        else
        {
            return false;
        }
    }
    
    function countTotalDislikes($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        //$this->db->where('userId', $document['userId']);
        $result = $this->db->get('mt_car')->row_array();       
        if (!empty($result))
        {
            return $result['totalDislikes'];
        }
        else
        {
            return false;
        }
    }

 function addFav($document)
    {
        //print_r($document); die;
        $query  = $this->db->insert('carfavourite', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

 function updateFav($document)
    {
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']); 
        $query  = $this->db->update('carfavourite', $document);
        if($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

 function isFavExist($document)
    {
        
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']); 
        $result = $this->db->get('carfavourite')->row_array();       
        if (!empty($result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


 function isFavModels($document)
    {
        $this->db->where('car.carModel', $document['carModel']); 
        $this->db->where('carfavourite.userId', $document['userId']); 
        $this->db->where('carfavourite.isFav', 1); 
        $this->db->join('carfavourite','car.carId = carfavourite.carId'); 
        $result = $this->db->get('car')->result_array();
       // echo $this->db->last_query(); die();
        if (!empty($result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function CarsBySorting($document)
    {
        if($document['type']!="")
            { 
                $this->db->order_by($document['type'], $document['order']); 
            }  
        

        $result = $this->db->get('car')->result_array();
        $count = count($result);
        for($i=0;$i<$count;$i++){
          $result[$i]['carImage']="localhost/motary/carImages/".$result[$i]['carImage'];

        }
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    function CarsByFilter($document)
    {

        if($document['sortType']!="")           {   $this->db->order_by($document['sortType'], $document['sortOrder']);     } 

        if($document['carCondition']!="")   {    $this->db->where('carCondition', $document['carCondition']);   }  
        if($document['carMake']!="")        {    $this->db->where('carMake',    $document['carMake']);          }  
        if($document['carModel']!="")       {    $this->db->where('carModel',   $document['carModel']);         }  
        if($document['carColor']!="")       {    $this->db->where('carColor',   $document['carColor']);         }  
        if($document['carYear']!="")        {    $this->db->where('carYear',    $document['carYear']);          }  
        if($document['carTrans']!="")       {    $this->db->where('carTrans',   $document['carTrans']);         }  
        if($document['carFuel']!="")        {    $this->db->where('carFuel',    $document['carFuel']);          }  
        if($document['carCity']!="")        {    $this->db->where('carCity',    $document['carCity']);          }  
        if($document['carPrice']!="")       {    $this->db->where('carPrice',   $document['carPrice']);         }  
        if($document['carMileage']!="")     {    $this->db->where('carMileage', $document['carMileage']);       }  
        if($document['carCylinders']!="")   {    $this->db->where('carCylinders',$document['carCylinders']);    }  

        $result = $this->db->get('car')->result_array();
        $count = count($result);
        for($i=0;$i<$count;$i++){
          $result[$i]['carImage']="localhost/motary/carImages/".$result[$i]['carImage'];

        }
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    
    function getCarsbyImageTags($result)    
    {  
        $myarray = array();
        //$result=$keywords;
        $tags = explode(' ', $result['name']);
        $tagCount = count($tags);
        $keyword = $tags[0];
        $this->db->limit(10, $result['count']); 
        $this->db->where("(`carComments` LIKE '%$keyword%' OR carCondition` LIKE '%$keyword%' OR `carMake` LIKE '%$keyword%' OR `carModel` LIKE '%$keyword%' OR  `carColor` LIKE '%$keyword%' OR `carYear` LIKE '%$keyword%' OR `carTrans` LIKE '%$keyword%' OR `carFuel` LIKE '%$keyword%' OR `carCity` LIKE '%$keyword%' OR `carPrice` LIKE '%$keyword%' OR `carMileage` LIKE '%$keyword%' OR `carCylinders` LIKE '%$keyword%' )");
        for($i=1; $i<$tagCount; $i++)
        {
            $keyword = $tags[$i];
            $this->db->or_where("(`carComments` LIKE '%$keyword%' OR carCondition` LIKE '%$keyword%' OR `carMake` LIKE '%$keyword%' OR `carModel` LIKE '%$keyword%' OR  `carColor` LIKE '%$keyword%' OR `carYear` LIKE '%$keyword%' OR `carTrans` LIKE '%$keyword%' OR `carFuel` LIKE '%$keyword%' OR `carCity` LIKE '%$keyword%' OR `carPrice` LIKE '%$keyword%' OR `carMileage` LIKE '%$keyword%' OR `carCylinders` LIKE '%$keyword%' )");
        }
        $car = $this->db->get('car')->result_array();  

        $totalCount = count($car);
        $car['totalCount']  = $totalCount;
        $car['tags']  = $result['name'];

        return $car;
    }

    function fact($n,$token) 
    {
        $response1['status'] = $n;
        if($response1['status'] != "completed" )
        {
            time_sleep_until(microtime(true)+4);
            //echo "Amfdit";
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.cloudsightapi.com/image_responses/".$token,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "authorization: CloudSight CwqFqm2Ai7eweMSs-yB0Jg",
                "cache-control: no-cache",
                "postman-token: 83626698-8975-2a7f-73fe-5a0ca7757710"
              ),
            ));

            $response1 = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) 
            {
              echo "cURL Error #:" . $err;
            } 
            else 
            {
              //echo $response;
            }
            $response1 = json_decode($response1,true);
            // pre($response1);
        }
                    return $response1;
    }


    public function getCarsbyImage()
    {             
            $key                            = "carImage";
            $isSuccess                      = false;
            $obj                            = new Car_model;
            $dummyRes                       = array();
            $dummyRes['count']              = $_REQUEST['count'];

            if(isset($_REQUEST['tags']) and $_REQUEST['tags']!="" )
            {
                $dummyRes['name']              = $_REQUEST['tags'];            
            }
            else
            {

                $image_url                      =   $this->uploadSearchImage($key);

                $image_url                      =   base_url()."imageSearch/uploads/".$image_url;

    //echo $image_url; //die();

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "http://cloudsightapi.com/image_requests",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[locale]\"\r\n\r\nen-US\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[remote_image_url]\"\r\n\r\nhttp://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/imageSearch/uploads/1459257944car.jpeg\r\n-----011000010111000001101001--",
                      CURLOPT_HTTPHEADER => array(
                        "authorization: OAuth oauth_consumer_key='CwqFqm2Ai7eweMSs-yB0Jg',oauth_token='370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',oauth_signature_method='HMAC-SHA1',oauth_timestamp='1459258038',oauth_nonce='akodW7',oauth_version='1.0',oauth_signature='VhjdbmPWz3aiNeNpRJf%2FO9wHQMI%3D'",
                        "cache-control: no-cache",
                        "content-type: multipart/form-data; boundary=---011000010111000001101001",
                        "postman-token: 97345529-8e69-0759-ae39-e95693ab8410"
                      ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) 
                    {
                      echo "cURL Error #:" . $err;
                    } 
                    else 
                    {
                      //echo $response;
                    }

                    $response = json_decode($response,true);
                   // $response1['status'] = "Amit";
                        
                        $newtimestmp = time()+12;
                        $dummyRes="amit";
                        while(($dummyRes=$this->fact($dummyRes,$response['token']))!="completed")
                        {
                            //pre($dummyRes);
                            if($dummyRes['status']!="completed")
                            {
                                
                                $timestmp = time();
                                if($timestmp > $newtimestmp)
                                {
                                   break; 
                                }
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
            //pre($dummyRes); 
            }




            $getCarsbyImage                      = $obj->getCarsbyImageTags($dummyRes);

            $isSuccess                      = true;
            $message                        = "Result return successfully";
            $data                           = $getCarsbyImage;
            //pre($data); die();

        //echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"totalCount"=>$data['totalCount'],"tags"=>$data['tags'],"Result"=>$data));

    }

   

    function commonFuction($document)
    {    //pre($document);
         //die();

        try {
        $result = array();
        if(isset($document['carImage']) and $document['carImage']!="")
        { 
           
            
              $dummyRes['name']="dshfhui";
            
           // $this->db->where('carId', $document['carId']); 
            $image_url = $this->makeBase64ImgSearch($document['carImage']);
            $image_url                      =   base_url()."imageSearched/".$image_url;
            
            //$image_url = "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/imageSearched/371459346426.png";
            //echo $image_url;echo "<br>"; //die();
               
                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => "http://cloudsightapi.com/image_requests",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 50,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[locale]\"\r\n\r\nen-US\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"image_request[remote_image_url]\"\r\n\r\n".$image_url."\r\n-----011000010111000001101001--",
                CURLOPT_HTTPHEADER => array(
                "authorization: OAuth oauth_consumer_key='CwqFqm2Ai7eweMSs-yB0Jg',oauth_token='370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',oauth_signature_method='HMAC-SHA1',oauth_timestamp='1459258038',oauth_nonce='akodW7',oauth_version='1.0',oauth_signature='VhjdbmPWz3aiNeNpRJf%2FO9wHQMI%3D'",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=---011000010111000001101001",
                "postman-token: 97345529-8e69-0759-ae39-e95693ab8410"
                ),
                ));
            ///pre($curl); die(); 
                $response = curl_exec($curl);
                $response = json_decode($response,true);
                 //pre($response);  
                 
                $err = curl_error($curl);

                curl_close($curl);
                //pre($err); die();
                //echo $response;
                //$response = json_decode($response,true);
            //echo "response ->";    pre($response); echo "<br>";
                
              // if(isset($response['error'])) { echo "Error ->". $response['error']; echo "<br>";}
               // if(isset($response['token'])) { echo "Token ->". $response['token']; }
                //die();
                
                if ($err) 
                {
                    //echo "cURL Error #:" . $err;
                    $dummyRes['name']="dshfhui";
                } 
                else if (!empty($response['error']))
                {
                    $dummyRes['name']="dshfhui";
                    //echo $response['error'];
                }
                else 
                {
                    $dummyRes['name']="dshfhui";
                  //echo $response; die();
                        //$response = json_decode($response,true);
                       // $response1['status'] = "Amit";
                          //pre($response); die();   
                        $newtimestmp = time()+20;
                        //$dummyRes="amit";
                        while(($dummyRes=$this->fact($dummyRes,$response['token']))!="completed")
                        {
                            if($dummyRes['status']!="completed")
                            {
                                $timestmp = time();
                                if($timestmp > $newtimestmp)
                                {
                                   break; 
                                }
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
                }
           // Request rate 5.867 req/min exceeded 5
            if(isset($dummyRes['name']) and $dummyRes['name']!="" and $dummyRes['name']!="dshfhui")
            {
                $tags = explode(' ', $dummyRes['name']);
                $tagCount = count($tags);
                
            }
            else
            {
                
                $tags = "dshfhui";
                $tagCount = 1;
            }
                $querylike ="";
               
                for($i=1; $i<$tagCount; $i++)
                {
                   
                    $keyword = $tags[$i];
                    //echo "...".$keyword."...";
                    $keyword = explode("'",$keyword);
                    
                    $keyword = $keyword[0];

                    $querylikenew = "OR `carMake`= '$keyword' OR `carModel`='$keyword' OR  `carColor`='$keyword' OR `carYear`='$keyword'";
                    $querylike = $querylike.$querylikenew;
                }
                    $keyword = $tags[0];
                    $keyword = explode("'",$keyword);
                    $keyword = $keyword[0];

            $this->db->where("(`carMake`= '$keyword' OR `carModel`='$keyword' OR  `carColor`='$keyword' OR `carYear`='$keyword' $querylike )");
             //echo "----".$dummyRes['name']."-----";
             return($dummyRes['name']);
        
        }
        else if(isset($document['carImageTags']) and $document['carImageTags']!="")
        {
            $tags = explode(' ', $document['carImageTags']);
            $tagCount = count($tags);

            $querylike ="";
            for($i=1; $i<$tagCount; $i++)
            {
                $keyword = $tags[$i];

                $keyword = explode("'",$keyword);
                $keyword = $keyword[0];
                $querylikenew = "OR `carMake` = '$keyword' OR `carModel`='$keyword' OR  `carColor`='$keyword' OR `carYear`='$keyword'";
                $querylike = $querylike.$querylikenew;
                //$this->db->or_where("(`carComments` LIKE '%$keyword%' OR carCondition` LIKE '%$keyword%' OR `carMake` LIKE '%$keyword%' OR `carModel` LIKE '%$keyword%' OR  `carColor` LIKE '%$keyword%' OR `carYear` LIKE '%$keyword%' OR `carTrans` LIKE '%$keyword%' OR `carFuel` LIKE '%$keyword%' OR `carCity` LIKE '%$keyword%' OR `carPrice` LIKE '%$keyword%' OR `carMileage` LIKE '%$keyword%' OR `carCylinders` LIKE '%$keyword%' )");
            }
            $keyword = $tags[0];
            $keyword = explode("'",$keyword);
            $keyword = $keyword[0];
            $this->db->where("(`carMake`= '$keyword' OR `carModel`='$keyword' OR  `carColor`='$keyword' OR `carYear`='$keyword' $querylike )");
           
        }
      

        if($document['carId']!="")          {  $this->db->where('carId', $document['carId']);    }
        // if($document['userId']!="")      { $this->db->where('userId', $document['userId']);  }
        if($document['sortType']=="")       { $this->db->order_by('createDate','desc');         }

        if($document['sortType']!="")       {   $this->db->order_by($document['sortType'], $document['sortOrder']);     } 

        if($document['carCondition']!="")
        { 
              $sql = $this->db->query('select id from mt_carcondition where name = "'.$document['carCondition'].'" or arabic_name="'.$document['carCondition'].'"');
              $cond = $sql->row_array();
              $condition = $cond['id'];
              $this->db->where('carCondition', $condition);
        }  
        if($document['carMake']!="")        {    $this->db->where('carMake',    $document['carMake']);          }  
        if($document['carModel']!="")       {    $this->db->where('carModel',   $document['carModel']);         }  
        if($document['carColor']!="")
        {
        $sql = $this->db->query('select id from mt_carcolour where name = "'.$document['carColor'].'" or arabic_name="'.$document['carColor'].'"');
        $col = $sql->row_array();
        if(count($col) > 0) {
            $color  =  $col['id'];
            $this->db->where('carColor',   $color);
        } else {
            $color  =  0;
            $where = 'carColor="'.$color.'" and carColorName="'.$document['carColor'].'" ';
            $this->db->where($where);
        }
        }  
        if($document['carYear']!="")        {    $this->db->where('carYear',    $document['carYear']);          }  
        if($document['carTrans']!="")
        {
           $sql = $this->db->query('select id from mt_cartransmission where name = "'.$document['carTrans'].'" or arabic_name="'.$document['carTrans'].'"');
           $Trans = $sql->row_array();
           $transmission = $Trans['id'];
           $this->db->where('carTrans',  $transmission);
        }  
        if($document['carFuel']!="")
        {
            $sql = $this->db->query('select id from mt_carfuel where name = "'.$document['carFuel'].'" or arabic_name="'.$document['carFuel'].'"');
            $fuel = $sql->row_array();
            $carfuel = $fuel['id'];   
            $this->db->where('carFuel',$carfuel);
        }  
        if($document['carCity']!="")
        { 
            $sql = $this->db->query('select id from mt_carCity where name = "'.$document['carCity'].'" or arabic_name="'.$document['carCity'].'"');
            $city = $sql->row_array();
            if(count($city) > 0) {
            $carcity =  $city['id'];
            $this->db->where('carCity',   $carcity);
        } else {
            $carcity  =  '0';
            
            $where = 'carCity="'.$carcity.'" and carCityName="'.$document['carCity'].'"';
            $this->db->where($where);
        }
        }
        if($document['carPrice']!="")       {    $this->db->where('carPrice',   $document['carPrice']);         }  
        if($document['carMileage']!="")     {    $this->db->where('carMileage', $document['carMileage']);       }  
        if($document['carCylinders']!="")   {    $this->db->where('carCylinders',$document['carCylinders']);    }  
        
        if($document['priceFrom']!="" and $document['priceTo']!="")   { $to=$document['priceTo']; $from=$document['priceFrom']; $this->db->where("carPrice BETWEEN '$from' and '$to'");    }  
        if($document['priceFrom']!="")   { $this->db->where('carPrice >=',$document['priceFrom']);   }  
        if($document['priceTo']!="")     { $this->db->where('carPrice <=',$document['priceTo']);   }  
// KEYWORD SEARCHING START HERE///////
        if($document['keyword']!="")           
        { 
            $keywords = $document['keyword'];
            $keywords = explode(" ",$keywords);
            $tKeys = count($keywords);
            $where ="";
            for($i=0;$i<$tKeys;$i++)
            {
                $keyword = $keywords[$i];
                $sql = $this->db->query('select id from mt_carcondition where name = "'.$keyword.'" or arabic_name="'.$keyword.'"');
                $cond = $sql->row_array();
                if(count($cond) > 0 )
                {
                    if($where==""){ $where = "carCondition =".$cond['id'];}
                    else { $where = $where." AND ( carCondition =".$cond['id'] . ")"; } 
                } 
                else 
                {
                    $sql = $this->db->query('select id from mt_carcolour where name = "'.$keyword.'" or arabic_name="'.$keyword.'"');
                    $col = $sql->row_array();
                    if(count($col) > 0) 
                    { 
                        if($where==""){ $where = "carColor =".$col['id'];}
                        else { $where = $where." AND ( carColor =".$col['id'] . ")"; } 
                    } 
                    else
                    {
                        $sql = $this->db->query('select id from mt_carCity where name = "'.$keyword.'" or arabic_name="'.$keyword.'"');
                        $city = $sql->row_array();
                        if(count($city) > 0) 
                        { 
                            if($where==""){ $where = "carCity =".$city['id'];}
                            else { $where = $where." AND ( carCity =".$city['id'] . ")"; } 
                        } 
                        else 
                        {
                            $sql = $this->db->query('select id from mt_cartransmission where name = "'.$keyword.'" or arabic_name="'.$keyword.'"');
                            $Trans = $sql->row_array();
                            if(count($Trans) > 0 )
                            {
                                if($where==""){ $where = "carTrans =".$Trans['id'];}
                                else { $where = $where." AND ( carTrans =".$Trans['id'] . ")"; }
                            } 
                            else 
                            { 

                                $sql = $this->db->query('select id from mt_carfuel where name = "'.$keyword.'" or arabic_name="'.$keyword.'"');
                                $fuel = $sql->row_array();
                                if(count($fuel) > 0 )
                                {
                                    if($where==""){ $where = "carFuel =".$fuel['id'];}
                                    else { $where = $where." AND ( carFuel =".$fuel['id'] . ")"; }
                                }
                                else 
                                {
                                    if($where==""){ $where = " ( `carMake` like '".$keyword."' OR `carModel` like '".$keyword."' OR `carYear` = '".$keyword."' OR `carComments` like '%$keyword%' )";}
                                    else {  $where = $where." AND ( `carMake` Like '".$keyword."' OR `carModel` Like '".$keyword."' OR `carYear` = '".$keyword."' OR `carComments` like '%$keyword%' )" ; }
                                }
                            }
                        }
                    }
                }
                
                
            }
 $this->db->where($where);
}
                $this->db->where('carStatus', 1); 
        
               //$this->db->where('isActive', '1'); 
                
    }
    catch (Exception $e) 
    {
        return  $e->getMessage();
    }
    
    }


    function get_Cars($document)
    {    //pre($document); die;
        //$carImageTags = "";

        $carImageTags = $this->commonFuction($document);

        if(isset($document['carImageTags']) and $document['carImageTags']!="")
        {
            $carImageTags = $document['carImageTags'];
        }
        else if($carImageTags!="")
        {            
        }
        else
        {
            $carImageTags = "";
        }
//echo $carImageTags; die();

        $res = $this->db->get('car')->result_array();
        //$last_query = $this->db->last_query();     echo $last_query;  die();
        $TotalCount = count($res);

        $this->db->limit(10, $document['count']);
        $this->commonFuction($document);

        $result = $this->db->get('car')->result_array();
         //pre($result);die();
        //getting value by ids of car details.........//
        foreach($result as $key=>$r)
        {
            $carCondition = $result[$key]['carCondition'];
            if($carCondition != "") {
            $this->db->where('id',$carCondition);
            $cond = $this->db->get('carcondition')->row_array();
            $result[$key]['carCondition'] = $cond['name'];
            if($document['locale'] == 'ar')
            {
            $result[$key]['carCondition'] = $cond['arabic_name'];
            }
            }
            
            $carColor = $result[$key]['carColor'];
            if($carColor != "") {
                $pos=0;
				$count = 0;
                for($i=0;$i<count($this->color);$i++)
                {
                    if($carColor == $this->color[$i]['id'])
                    {
                       $count  = 1;
                       $pos = $this->color[$i]['id'];
                      
                    }
                }
                 if($count == 1)
                    {
                     
                     $result[$key]['carColor'] = $this->color[$pos-1]['name'];
                     if($document['locale'] == 'ar') {  $result[$key]['carColor'] = $this->color[$pos-1]['arabic_name']; }
                    }
                    else
                    {
                        $result[$key]['carColor'] = $result[$key]['carColorName'];
                    }
            }
               
           $carTrans = $result[$key]['carTrans'];
            if($carTrans != "") {
            $this->db->where('id',$carTrans);
            $Trans = $this->db->get('cartransmission')->row_array();
            $result[$key]['carTrans'] = $Trans['name'];
            if($document['locale'] == 'ar') { $result[$key]['carTrans'] = $Trans['arabic_name']; }
            }
            
            $carFuel = $result[$key]['carFuel'];
            if($carTrans != "") {
            $this->db->where('id',$carFuel);
            $Fuel = $this->db->get('carfuel')->row_array();
            $result[$key]['carFuel'] = $Fuel['name'];
            if($document['locale'] == 'ar') { $result[$key]['carFuel'] = $Fuel['arabic_name']; }
            }
            
            $carCity = $result[$key]['carCity'];
            if($carCity != "") {
            $this->db->where('id',$carCity);
            $city = $this->db->get('carCity')->row_array();
            if(count($city) > 0 )
            {
            $result[$key]['carCity'] = $city['name'];
            if($document['locale'] == 'ar') {  $result[$key]['carCity'] = $city['arabic_name']; }
            }
            else {
            $result[$key]['carCity'] = $result[$key]['carCityName'];   
            }
           }
        }
         //getting value by ids of car details ends here...//
         
        //pre($result);die();
        $coun = count($result);
        for($i=0;$i<$coun;$i++)
        {
            $makeName     = $result[$i]['carMake'];

            $query = $this->db->query("Select id from mt_carmake where name='$makeName'");
            $makes = $query->row_array();
            $result[$i]['makeId']= $makes['id'];


            $YearYear     = $result[$i]['carYear'];
            $selyear      = $this->db->query("Select id from mt_caryear where year='$YearYear' ");
            $resyear      = $selyear->row_array();
            $yearId        = $resyear['id'];
            $result[$i]['yearId']=$yearId; 

            //$makeYear     = $result[$i]['carYear'];

           // $result[$i]['makeId'] = $TotalCount;
            //$result[$i]['yearId'] = $TotalCount;

            $userID     = $result[$i]['userId'];
            $seluser    = $this->db->query("Select profileImage,loginType from mt_users where userId='$userID' ");
            $resuser    = $seluser->row_array();
            $userimage  = $resuser['profileImage'];
            $result[$i]['profileImage']=$userimage; 
            //$loginType  = $resuser['loginType'];
          //if($userimage!="" )
            //{
               // if($loginType==0){ $result[$i]['profileImage']="http://motary.ae/motaryApp/profileImages/".$userimage;  }
               // else             { $result[$i]['profileImage']= $userimage;  }
            //}  
           // else 
           // { $result[$i]['profileImage']=""; 
           // }

          if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']= base_url()."carImages/".$result[$i]['carImage1']; }
          if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']=base_url()."carImages/".$result[$i]['carImage2']; }
          if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']=base_url()."carImages/".$result[$i]['carImage3']; }
          if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']=base_url()."carImages/".$result[$i]['carImage4']; }
          if($result[$i]['carImage5']!=""){ $result[$i]['carImage5']=base_url()."carImages/".$result[$i]['carImage5']; }
          if($result[$i]['carImage6']!=""){ $result[$i]['carImage6']=base_url()."carImages/".$result[$i]['carImage6']; }
          if($result[$i]['carImage7']!=""){ $result[$i]['carImage7']=base_url()."carImages/".$result[$i]['carImage7']; }
          if($result[$i]['carImage8']!=""){ $result[$i]['carImage8']=base_url()."carImages/".$result[$i]['carImage8']; }
          if($result[$i]['carImage9']!=""){ $result[$i]['carImage9']=base_url()."carImages/".$result[$i]['carImage9']; }
          if($result[$i]['carImage10']!=""){ $result[$i]['carImage10']=base_url()."carImages/".$result[$i]['carImage10']; }
          $result[$i]['TotalCount']     = $TotalCount;
          $result[$i]['carImageTags']     = $carImageTags;

            $result[$i]['userLike']=0;
            $result[$i]['userFav']=0;
            if($document['userId']!="")       
            {   
                $query = $this->db->query("Select carId from mt_carlikes where isLike!=0 and userId='".$document['userId']."'");
                $carids= $query->result_array();

                $query1 = $this->db->query("Select carId from mt_carfavourite where isFav='1' and userId='".$document['userId']."'");
                $carfavs= $query1->result_array();
//pre($carids);


                $Likecarids = array();
                foreach($carids as $entry)
                {
                    $Likecarids[] = $entry['carId'];
                }
                $Favcarids = array();
                foreach($carfavs as $entry1)
                {
                    $Favcarids[] = $entry1['carId'];
                }

                if (in_array($result[$i]['carId'], $Likecarids))  { $result[$i]['userLike'] =1 ; } 
                if (in_array($result[$i]['carId'], $Favcarids)) { $result[$i]['userFav']  =1 ; } 
            }
        }
        if (!empty($result))
        { 
            return $result;
        }
        else
        {
            return false;
        }
    }



 function getAdminSetting($document)
    {
       //$this->db->select('paymentOption'); 
        
        
        $this->db->where('deviceToken',$document['deviceToken']);
        $result1 = $this->db->get('allUser')->row_array();
        if(count($result) != 0) {
        $this->db->where('deviceToken',$document['deviceToken']);
        $result1 = $this->db->update('allUser',$document);
        } else {
        $this->db->insert('allUser',$document); }
        
$result = $this->db->get('adminSettings')->row_array();unset($result['id']);
//if($result['paymentOption']==0){ $result['paymentOption']="No"; } else { $result['paymentOption']="Yes"; }      
       // if($result['platePaymentOption']==0){ $result['platePaymentOption']="No"; } else { $result['platePaymentOption']="Yes"; }      
       // if($result['imageSearchOption']==0){ $result['imageSearchOption']="No"; } else { $result['imageSearchOption']="Yes"; } 
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

//////////////////-------------Start--------Motary M4--------Start---------------///////////////////////


    function favCars($document)
    {
        $this->db->join('carlogo','car.carMake = carlogo.carMake');

        $this->db->join('carfavourite','car.carId = carfavourite.carId');
        $this->db->where('carfavourite.userId',$document['userId']);  
        $this->db->where('carfavourite.isFav','1'); 

        $res = $this->db->get('car')->result_array();
        $TotalCount = count($res);
        //echo $this->db->last_query();

        $this->db->select('carlogo.carLogo,car.carId,car.carImage1,car.carImage2,car.carImage3,car.carImage4,car.carMake,car.carModel,car.carYear');
        $this->db->where('carfavourite.userId',$document['userId']);  
        $this->db->where('carfavourite.isFav','1'); 
        $this->db->join('carfavourite','car.carId = carfavourite.carId');
        $this->db->join('carlogo','car.carMake = carlogo.carMake');

        $this->db->limit(10, $document['count']); 

        $result = $this->db->get('car')->result_array();
        $count = count($result);
        for($i=0;$i<$count;$i++)
        {

            $result[$i]['carLogo'] = strtolower ($result[$i]['carLogo'] );
              //if($result[$i]['logo']!=""){ $result[$i]['logo']="http://fourthscreenlabs.com/amit/motary/carLogo/".$result[$i]['carLogo']; }

            if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carLogo/".$result[$i]['carLogo']; }
            if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage2']; }
            if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage3']; }
            if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage4']; }
            $result[$i]['TotalCount'] = $TotalCount;
            unset($result[$i]['carLogo']);
            
            
        }
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }


    function getSoldCars($document)
    {
        $this->db->where('isSold', 1);  
        $res = $this->db->get('car')->result_array(); 
        $TotalCount = count($res);


        $this->db->select('carId,carImage1,carImage2,carImage3,carImage4,carMake,carModel,carYear,carPrice');
        $this->db->limit(10, $document['count']);
        $this->db->where('isSold', 1); 
        $this->db->order_by('carId', 'desc'); 
        $result = $this->db->get('car')->result_array(); 

        $count = count($result);
        for($i=0;$i<$count;$i++){
          if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage1']; }
          if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage2']; }
          if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage3']; }
          if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']="http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/carImages/".$result[$i]['carImage4']; }
          $result[$i]['TotalCount'] = $TotalCount;
        }
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }  

    function reportCar($document) 
    {
        $query  = $this->db->insert('reportedCars', $document);
        $id = $this->db->insert_id();
        //$q = $this->db->last_query();
        //echo $q; die;
        if($id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function isReported($document)
    {
        $this->db->where('carId', $document['carId']); 
        $this->db->where('userId', $document['userId']); 
        $result = $this->db->get('reportedCars')->row_array();
        if (!empty($result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


//////////////////--------------End--------Motary M4--------End---------------///////////////////////

    function GetCarHistory($document)
    {
	    if($document['userId'] != "") {
	    //$this->db->select('userId,carId,carImage1,carImage2,carImage3,carImage4,carMake,carModel,carYear,carPrice,isSold,isPaid');
        $this->db->where('userId', $document['userId']); 
		$this->db->where('carStatus', '1'); 
		$this->db->limit(10, $document['count']);
        $this->db->order_by('carId', 'desc'); 
        $result = $this->db->get('car')->result_array(); 
		$TotalCount = count($result);
		$count = count($result);
        for($i=0;$i<$count;$i++)
        {
            if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']= base_url("carImages/".$result[$i]['carImage1']); }
            if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']= base_url("carImages/".$result[$i]['carImage2']); }
            if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']= base_url("carImages/".$result[$i]['carImage3']); }
            if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']= base_url("carImages/".$result[$i]['carImage4']); }
               $result[$i]['TotalCount'] = $TotalCount;
               
            $carCondition = $result[$i]['carCondition'];
            if($carCondition != "") {
            $this->db->where('id',$carCondition);
            $cond = $this->db->get('carcondition')->row_array();
            $result[$i]['carCondition'] = $cond['name'];
            if($document['locale'] == 'ar')
            {
            $result[$i]['carCondition'] = $cond['arabic_name'];
            }
            }
            
            $carColor = $result[$i]['carColor'];
            if($carColor != "") {
            $this->db->where('id',$carColor);
            $color = $this->db->get('carcolour')->row_array();
            if(count($color) > 0 )
            {
            $result[$i]['carColor'] = $color['name'];
            if($document['locale'] == 'ar') {  $result[$i]['carColor'] = $color['arabic_name']; }
            }
            else {
            $result[$i]['carColor'] = $result[$i]['carColorName'];   
            }
           }
           
           $carTrans = $result[$i]['carTrans'];
            if($carTrans != "") {
            $this->db->where('id',$carTrans);
            $Trans = $this->db->get('cartransmission')->row_array();
            $result[$i]['carTrans'] = $Trans['name'];
            if($document['locale'] == 'ar') { $result[$i]['carTrans'] = $Trans['arabic_name']; }
            }
            
            $carFuel = $result[$i]['carFuel'];
            if($carTrans != "") {
            $this->db->where('id',$carFuel);
            $Fuel = $this->db->get('carfuel')->row_array();
            $result[$i]['carFuel'] = $Fuel['name'];
            if($document['locale'] == 'ar') { $result[$i]['carFuel'] = $Fuel['arabic_name']; }
            }
            
            $carCity = $result[$i]['carCity'];
            if($carCity != "") {
            $this->db->where('id',$carCity);
            $city = $this->db->get('carCity')->row_array();
            if(count($city) > 0 )
            {
            $result[$i]['carCity'] = $city['name'];
            if($document['locale'] == 'ar') {  $result[$i]['carCity'] = $city['arabic_name']; }
            }
            else {
            $result[$i]['carCity'] = $result[$i]['carCityName'];   
            }
           }
        }
		for($i=0;$i<$count;$i++) {
		
		 $makeName     = $result[$i]['carMake'];

            $query = $this->db->query("Select id from mt_carmake where name='$makeName'");
            $makes = $query->row_array();
            $result[$i]['makeId']= $makes['id'];


            $YearYear     = $result[$i]['carYear'];
            $selyear      = $this->db->query("Select id from mt_caryear where year='$YearYear' ");
            $resyear      = $selyear->row_array();
            $yearId        = $resyear['id'];
            $result[$i]['yearId']=$yearId; 

            //$makeYear     = $result[$i]['carYear'];

           // $result[$i]['makeId'] = $TotalCount;
            //$result[$i]['yearId'] = $TotalCount;

            $userID     = $result[$i]['userId'];
            $seluser    = $this->db->query("Select profileImage,loginType from mt_users where userId='$userID' ");
            $resuser    = $seluser->row_array();
            $userimage  = $resuser['profileImage'];
            $result[$i]['profileImage']=$userimage; 
		
		}
        } else {
				//$this->db->select('carId,carImage1,carImage2,carImage3,carImage4,carMake,carModel,carYear,carPrice,isSold,isPaid');
				$this->db->where('userId', $document['userId']); 
				$this->db->limit(10, $document['count']);
				$this->db->order_by('carId', 'desc'); 
				$result = $this->db->get('car')->result_array(); 
				$this->db->select();
		        $result1 = $this->db->get('car')->result_array(); 
		        $TotalCount = count($result1);
                $count = count($result);
                for($i=0;$i<$count;$i++)
                {
            if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']= base_url("carImages/".$result[$i]['carImage1']); }
            if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']= base_url("carImages/".$result[$i]['carImage2']); }
            if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']= base_url("carImages/".$result[$i]['carImage3']); }
            if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']= base_url("carImages/".$result[$i]['carImage4']); }
               $result[$i]['TotalCount'] = $TotalCount;
        }
		for($i=0;$i<$count;$i++) {
		
		 $makeName     = $result[$i]['carMake'];

            $query = $this->db->query("Select id from mt_carmake where name='$makeName'");
            $makes = $query->row_array();
            $result[$i]['makeId']= $makes['id'];


            $YearYear     = $result[$i]['carYear'];
            $selyear      = $this->db->query("Select id from mt_caryear where year='$YearYear' ");
            $resyear      = $selyear->row_array();
            $yearId        = $resyear['id'];
            $result[$i]['yearId']=$yearId; 

            //$makeYear     = $result[$i]['carYear'];

           // $result[$i]['makeId'] = $TotalCount;
            //$result[$i]['yearId'] = $TotalCount;

            $userID     = $result[$i]['userId'];
            $seluser    = $this->db->query("Select profileImage,loginType from mt_users where userId='$userID' ");
            $resuser    = $seluser->row_array();
            $userimage  = $resuser['profileImage'];
            $result[$i]['profileImage']=$userimage; 
		
		}
    } 
		
		
		
        /* $car = $this->db->get('car')->result_array(); 
        $TotalCount = count($car);

        $this->db->select('carId,carImage1,carImage2,carImage3,carImage4,carMake,carModel,carYear,carPrice,isSold,isPaid');
        $this->db->where('userId', $document['userId']);
		$this->db->limit(10, $document['count']);
        $this->db->order_by('carId', 'desc'); 
        $result = $this->db->get('car')->result_array(); 
        $count = count($result);
        for($i=0;$i<$count;$i++)
        {
            if($result[$i]['carImage1']!=""){ $result[$i]['carImage1']= base_url("carImages/".$result[$i]['carImage1']); }
            if($result[$i]['carImage2']!=""){ $result[$i]['carImage2']= base_url("carImages/".$result[$i]['carImage2']); }
            if($result[$i]['carImage3']!=""){ $result[$i]['carImage3']= base_url("carImages/".$result[$i]['carImage3']); }
            if($result[$i]['carImage4']!=""){ $result[$i]['carImage4']= base_url("carImages/".$result[$i]['carImage4']); }
               $result[$i]['TotalCount'] = $TotalCount;
        } */

        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }  


    function GetPlateHistory($document)
    {
        if($document['userId'] != "") {
		//$this->db->select('userId,plateId,plateImage,platePrice,plateCategory,plateNumber,isSold,isPaid,plateCity');
		$this->db->where('plateStatus', '1'); 
		$this->db->where('userId', $document['userId']);
		$this->db->limit(10, $document['count']);
        $this->db->order_by('plateId', 'desc'); 
        $result = $this->db->get('plate')->result_array(); 
		$TotalCount = count($result);
		$count = count($result);
        for($i=0;$i<$count;$i++)
        {
            if($result[$i]['plateImage']!=""){ $result[$i]['plateImage']= base_url("plateImages/".$result['plateImage']); }
               $result[$i]['TotalCount'] = $TotalCount;
               
            $plateCity = $result[$i]['plateCity'];
            if($plateCity != "") {
            $this->db->where('id',$plateCity);
            $city = $this->db->get('plateCity')->row_array();
            if(count($city) > 0 )
            {
            $result[$i]['plateCity'] = $city['name'];
            if($document['locale'] == 'ar') {  $result[$i]['plateCity'] = $city['arabic_name']; }
            }
            else {
            $result[$i]['plateCity'] = $result[$i]['plateCity'];   
            }
           }
               
        }
        } else {
        //$this->db->select('userId,plateId,plateImage,platePrice,plateCategory,plateNumber,isSold,isPaid,plateCity');
        $this->db->limit(10, $document['count']);
        $this->db->order_by('plateId', 'desc'); 
        $result = $this->db->get('plate')->result_array(); 
		$this->db->select();
		$result1 = $this->db->get('plate')->result_array(); 
		$TotalCount = count($result1);
        $count = count($result);
        for($i=0;$i<$count;$i++)
        {
            if($result[$i]['plateImage']!=""){ $result[$i]['plateImage']= base_url("plateImages/".$result['plateImage']); }
               $result[$i]['TotalCount'] = $TotalCount;
        }
    }  
	 if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
}

    function updatePushNotification($document)
    {
        $this->db->where('userId', $document['userId']); 
	$query  = $this->db->update('users', $document);
		
	if ($query)
        {
            $this->db->where('userId', $document['userId']); 
            $query1  = $this->db->get('users')->result_array();
            return $query1;
        }
        else
        {
            return false;
        }
    }

    function GetPayment($document)
    {
        $query = $this->db->get($document['table'])->result_array();
        
        
	if ($query)
        {
          if($document['type'] == 'car') {
             $data = array('id'=>$query[0]['id'] , 'CarPaymentPay'=>$query[0]['CarPaymentPay']); 
          } else if($document['type'] == 'plate') {
              $data = array('id'=>$query[0]['id'] , 'PlatePaymentPay'=>$query[0]['PlatePaymentPay']); 
          }
          
          $transType = ($query[0]['dev_isActive'] == 1) ? 'dev_' : 'pro_';
          $data['telrValue'] = $query[0]['dev_isActive'];
          $data['key'] = $query[0][$transType.'key'];
          $data['storeId'] = $query[0][$transType.'storeId'];
          $data['appId'] = $query[0][$transType.'appId'];
          $data['appName'] = $query[0][$transType.'appName'];
          $data['appVersion'] = $query[0][$transType.'appVersion'];
          $data['transType'] = $query[0][$transType.'transType'];
          $data['transClass'] = $query[0][$transType.'transClass'];
          $data['transDesc'] = $query[0][$transType.'transDesc'];
          $data['transCurrency'] = $query[0][$transType.'transCurrency'];

          return $data;
        }
        else
        {
            return false;
        }
    }
    
    function checkPaidStatus($document){
        
        $this->db->where('userId', $document['userId']);
        $this->db->where($document['fieldName'], $document[$document['fieldName']]);
        $query = $this->db->get($document['type']);
        
        return $query->result_array();
    }
    
    function paidStatusUpdate($document){
        
        $this->db->where('userId', $document['userId']);
        $this->db->where($document['fieldName'], $document[$document['fieldName']]);
        $this->db->update($document['type'], array('isPaid' => $document['paidStatus'], 'paymentStatus' => $document['paymentStatus']));
        
        
        return $this->db->affected_rows();
    }
}
