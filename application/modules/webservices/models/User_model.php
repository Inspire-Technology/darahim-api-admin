<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getUsers() {
        //connect to mongodb collection (i.e., table) named as ‘tbl_users’
        $collection = $this->mongo_db->db->selectCollection('tbl_users');
        //selecting records from the collection - tbl_users
        $result = $collection->find();
        $results = array();
        foreach ($result as $data) {
            //display the records  
            $results[] = $data;
        }
        return $results;
    }

    function userSignUp($document) {
        //print_r($document); die;
        $query = $this->db->insert('users', $document);
        $id = $this->db->insert_id();
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    function createProfile($document) {
        //print_r($document); die;
        $this->db->insert('users', $document);
        $id = $this->db->insert_id();
        if ($id) {
            ####Below Code to Generate and store EndpointARN .. it needs token Only = $token
            $token = $document['deviceToken'];

            $url_n = 'http://www.motary.ae/sns/classSNS.php';

            $fields_n = array(
                'token' => $token,
                'method' => 'addEndPoint'
            );


            $postvars_n = http_build_query($fields_n);
            $ch_n = curl_init();
            curl_setopt($ch_n, CURLOPT_URL, $url_n);
            curl_setopt($ch_n, CURLOPT_POST, count($fields_n));
            curl_setopt($ch_n, CURLOPT_POSTFIELDS, $postvars_n);

            $result_n = curl_exec($ch_n);
            curl_close($ch_n);

            return $id;
        } else {
            return false;
        }
    }

    function getProfile($id) {
        //echo $id; die;
        $this->db->where('userId', $id);
        $details = $this->db->get('users')->row_array();

        //$details=$this->db->result();
        //print_r($details); die;
        //print_r($details); die;
        return $details;
    }

    function updateProfile($document) {
        $this->db->where('email', $document['email']);
        $query = $this->db->update('users', $document);
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    function isUserExist($document) {

        $this->db->where('email', $document['email']);
        $result = $this->db->get('users')->row_array();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function isUserIdExist($document) {
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('users')->row_array();
        if (!empty($result)) {
            return $result['userId'];
        } else {
            return false;
        }
    }

    function UpdateLocale($document) {
        $this->db->where('userId', $document['userId']);
        $query = $this->db->update('users', $document);
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    function isUserExistS($document) {

        $this->db->where('socialId', $document['socialId']);
        $this->db->where('loginType', $document['loginType']);
        $result = $this->db->get('users')->row_array();
        if (!empty($result)) {
            return $result['userId'];
        } else {
            return false;
        }
    }

    function makeBase64Img($image) {

        $data = str_replace(" ", "+", $image);
        $data = base64_decode($data);
        $im = imagecreatefromstring($data);
        $fileName = rand(5, 115) . time() . ".png";
        $imageName = "profileImages/" . $fileName;


        if ($im !== false) {

            imagepng($im, $imageName);

            imagedestroy($im);
        } else {
            echo 'An error occurred.';
        }

        return $fileName;
    }

    function UserVerifyCode($document) {

        $this->db->where('email', $document['email']);
        $result = $this->db->get('users')->row_array();

        if (!empty($result)) {
            if ($result['verificationCode'] == $document['verificationCode']) {
                return 1;
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    function userLogin($document) {
        $this->db->where('email', $document['email']);
        $this->db->where('password', $document['password']);
        $result = $this->db->get('users')->row_array();
        //echo $result['userId']; die;
        //print_r($result);
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function userLoginS($document) {
        $this->db->where('loginType', $document['loginType']);
        $this->db->where('socialId', $document['socialId']);
        //$this->db->where('password', $document['password']); 
        $result = $this->db->get('users')->row_array();
        //echo $result['userId']; die;
        //print_r($result);
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    /*
      function forgetPassword($document)
      {
      echo $document['password']; die;

      $this->mail()
      if($this->db->affected_rows())
      {
      return true;
      }
      else
      {
      return false;
      }
      }

     */

    function changePassword($document) {
        $this->db->where('email', $document['email']);
        $query = $this->db->update('users', $document);
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

//////////////////---------------Start--------Motary M4--------Start-----------------///////////////////////


    function userProfile($document) {
        $this->db->where('userId', $document['userId']);
        $result = $this->db->get('users')->row_array();
        //$result[0]['profileImage']="http://fourthscreenlabs.com/amit/motarym4/profileImages/".$result[0]['profileImage'];
        //$result[0]['profileImage']=$result[0]['profileImage'];
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function editProfile($document) {
        // $this->db->where('email', $document['email']);
        $this->db->where('userId', $document['userId']);
        $query = $this->db->update('users', $document);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

//////////////////--------------------End--------Motary M4--------End-------------------///////////////////////


    function chatPush($document) {

        $this->db->select('users.deviceType,users.deviceToken,users.isPushNotification');
        $this->db->where('car.carModel', $document);
        $this->db->where('car.isSold', 1);
        $this->db->where('users.isPushNotification', 1);
        $this->db->join('carfavourite', 'car.carId = carfavourite.carId');
        $this->db->join('users', 'users.userId = carfavourite.userId');
        $result = $this->db->get('car')->result_array();
        //$q = $this->db->last_query();
        //echo $q; die();


        $message = "Push Message";

        if (!empty($result)) {
            $i = 0;
            foreach ($result as $value) {
                $deviceType = $result[$i]['deviceType'];
                $deviceToken = $result[$i]['deviceToken'];
                $generatePush = $this->generatePush($deviceType, $deviceToken, $message);
                return true;
                $i++;
            }
        } else {
            return false;
        }
    }

}
