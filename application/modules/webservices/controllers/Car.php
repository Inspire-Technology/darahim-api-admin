<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Car extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Car_model');
        $this->load->library('email');
        //$this->load->helper('aes'); 
        error_reporting('0');
    }

    /*
     * Modified By      :Amit Kumar
     * Modified Date    :14Jan,16
     * Description      : Function used for user registration 
     */

    function uploadSearchImage($key) {  //echo $key; die();
        $files = $_FILES[$key];
        $cnt = count($files['name']);
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {

            $name = "/var/www/html/motaryAdmin/imageSearch/uploads/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    function replace($string) {
        $replace = str_replace('+', ' ', $string);
        $replace = str_replace('-', ' ', $replace);
        return $replace;
    }

//============= https://github.com/tazotodua/useful-php-scripts/ ===========


    public function carTables() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            //$document['locale']             = $json['locale'];
            $document['table'] = $json['table'];

            if ($document['table'] == "caryear") {
                $document['make_id'] = $json['make_id'];
            }
            if ($document['table'] == "carmodel") {
                $document['makeyear_id'] = $json['makeyear_id'];
            }

            $isSuccess = false;
            $obj = new Car_model;
            $carTables = $obj->carTables($document);

            $isSuccess = true;
            $message = "Result return successfully";
            $data = $carTables;

            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "نتائج البحث";
                if (!empty($carTables) and ( $json['table'] == "carcolour" or $json['table'] == "carCity" or $json['table'] == "carcondition" or $json['table'] == "carfuel" or $json['table'] == "cartransmission")) {

                    unset($data);
                    foreach ($carTables as $key => $value) {
                        if ($document['table'] == "carcolour" or $json['table'] == "carCity" or $json['table'] == "carcondition" or $json['table'] == "carfuel" or $json['table'] == "cartransmission") {
                            $value['name'] = $value['arabic_name'];
                            unset($value['arabic_name']);
                            $data[] = $value;
                        }
                    }
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function carTablesKeywords() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $document['table'] = $json['table'];
            $document['keyword'] = $json['keyword'];
            $document['locale'] = $json['locale'];

            $isSuccess = false;
            $obj = new Car_model;


            $carTablesKeywords = $obj->carTablesKeywords($document);

            $isSuccess = true;
            $message = "Result return successfully";
            $data = $carTablesKeywords;
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "نتائج البحث";
                if (!empty($carTablesKeywords) and ( $json['table'] == "carcolour" or $json['table'] == "carCity" or $json['table'] == "carcondition" or $json['table'] == "carfuel" or $json['table'] == "cartransmission")) {
                    unset($data);
                    foreach ($carTablesKeywords as $key => $value) {
                        if ($document['table'] == "carcolour" or $json['table'] == "carCity" or $json['table'] == "carcondition" or $json['table'] == "carfuel" or $json['table'] == "cartransmission") {
                            $value['name'] = $value['arabic_name'];
                            unset($value['arabic_name']);
                            $data[] = $value;
                        }
                    }
                }
            }
            //$data                           = $carTablesKeywords;
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    /*
      public function addCar()
      {
      $json                               = file_get_contents('php://input');
      if(is_json($json))
      {
      //$json                           = json_decode($json,true);
      $document                       = array();
      $document['userId']             = $_REQUEST['userId'];
      $document['carCondition']       = $_REQUEST['carCondition'];
      $document['carMake']            = $_REQUEST['carMake'];
      $document['carYear']            = $_REQUEST['carYear'];
      $document['carModel']           = $_REQUEST['carModel'];
      $document['carPrice']           = $_REQUEST['carPrice'];
      $document['carTrans']           = $_REQUEST['carTrans'];
      $document['carFuel']            = $_REQUEST['carFuel'];
      $document['carCity']            = $_REQUEST['carCity'];
      $document['carMileage']         = $_REQUEST['carMileage'];
      $document['carCylinders']       = $_REQUEST['carCylinders'];
      $document['carColor']           = $_REQUEST['carColor'];
      $document['carComments']        = $_REQUEST['carComments'];
      $document['latitude']           = $_REQUEST['latitude'];
      $document['longitude']          = $_REQUEST['longitude'];
      $document['address']            = $_REQUEST['address'];
      $document['name']               = $_REQUEST['name'];
      $document['email']              = $_REQUEST['email'];
      $document['phone']              = $_REQUEST['phone'];

      $obj                            = new Car_model;

      if(isset($_FILES['carImage1']['name'])){

      $document['carImage1']   =   $obj->uploadCardImage("carImage1");
      }else{

      $document['carImage1']   =   "";
      }
      if(isset($_FILES['carImage2']['name'])){

      $document['carImage2']   =   $obj->uploadCardImage("carImage2");
      }else{

      $document['carImage2']   =   "";
      }
      if(isset($_FILES['carImage3']['name'])){

      $document['carImage3']   =   $obj->uploadCardImage("carImage3");
      }else{

      $document['carImage3']   =   "";
      }
      if(isset($_FILES['carImage4']['name'])){

      $document['carImage4']   =   $obj->uploadCardImage("carImage4");
      }else{

      $document['carImage4']   =   "";
      }

      $isSuccess                      = false;

      $addCar                    = $obj->addCar($document);

      if($addCar)
      {
      $isSuccess      = true;
      $message        = "Car added successfully";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "تم إضافة الإعلان بنجاح"; }
      $data           = $addCar;
      }
      else
      {
      $isSuccess      = false;
      $message        = "Car could not add";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "لم نتمكن من إضافة الإعلان حاول مرة أخرى"; }
      $data           = array();
      }
      }
      else
      {
      $isSuccess      = false;
      $message        = "Invaid Json Input";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "خطأ في النظام"; }
      $data           = array();
      }
      echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
      }



      public function addCarnew()
      {
      $json                               = file_get_contents('php://input');
      if(is_json($json))
      {
      //$json                           = json_decode($json,true);
      $document                       = array();

      $document['userId']             = $_REQUEST['userId'];
      $document['carCondition']       = $_REQUEST['carCondition'];
      $document['carMake']            = $_REQUEST['carMake'];
      $document['carYear']            = $_REQUEST['carYear'];
      $document['carModel']           = $_REQUEST['carModel'];
      $document['carPrice']           = $_REQUEST['carPrice'];
      $document['carTrans']           = $_REQUEST['carTrans'];
      $document['carFuel']            = $_REQUEST['carFuel'];
      $document['carCity']            = $_REQUEST['carCity'];
      $document['carMileage']         = $_REQUEST['carMileage'];
      $document['carCylinders']       = $_REQUEST['carCylinders'];
      $document['carColor']           = $_REQUEST['carColor'];
      $document['carComments']        = $_REQUEST['carComments'];

      $document['latitude']           = $_REQUEST['latitude'];
      $document['longitude']          = $_REQUEST['longitude'];
      $document['address']            = $_REQUEST['address'];
      $document['name']               = $_REQUEST['name'];
      $document['email']              = $_REQUEST['email'];
      $document['phone']              = $_REQUEST['phone'];

      $count              = $_REQUEST['count']+1;

      $obj                            = new Car_model;


      for($i=1;$i<$count;$i++)
      {
      $document["carImage$i"]   =   $obj->uploadCardImage("carImage$i");
      }


      $isSuccess                      = false;

      $addCar                    = $obj->addCar($document);
      if($addCar)
      {
      $isSuccess      = true;
      $message        = "Car added successfully";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "تم إضافة الإعلان بنجاح"; }
      $data           = $addCar;
      }
      else
      {
      $isSuccess      = false;
      $message        = "Car could not add";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "لم نتمكن من إضافة الإعلان حاول مرة أخرى"; }
      $data           = array();
      }
      }
      else
      {
      $isSuccess      = false;
      $message        = "Invaid Json Input";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "خطأ في النظام"; }
      $data           = array();
      }
      echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
      }


      public function addCarnewnew()
      {
      $json                               = file_get_contents('php://input');
      if(is_json($json))
      {

      $json                           = json_decode($json,true);
      $document                       = array();

      $document['userId']             = $json['userId'];
      $document['carCondition']       = $json['carCondition'];
      $document['carMake']            = $json['carMake'];
      $document['carYear']            = $json['carYear'];
      $document['carModel']           = $json['carModel'];
      $document['carPrice']           = $json['carPrice'];
      $document['carTrans']           = $json['carTrans'];
      $document['carFuel']            = $json['carFuel'];
      $document['carCity']            = $json['carCity'];
      $document['carMileage']         = $json['carMileage'];
      $document['carCylinders']       = $json['carCylinders'];
      $document['carColor']           = $json['carColor'];
      $document['carComments']        = $json['carComments'];

      $document['latitude']           = $json['latitude'];
      $document['longitude']          = $json['longitude'];
      $document['address']            = $json['address'];
      $document['name']               = $json['name'];
      $document['email']              = $json['email'];
      $document['phone']              = $json['phone'];

      //$count                          = $json['count']+1;

      $obj                            = new Car_model;


      $isSuccess                      = false;

      //for($i=1;$i<$count;$i++)
      for($i=1;$i<11;$i++)
      {
      if(isset($json["carImage$i"]))
      {
      if($json["carImage$i"]=="")
      {
      $document["carImage$i"]="";
      }
      else
      {
      $document["carImage$i"]   =   $json["carImage$i"];
      $document["carImage$i"]   =   $obj->makeBase64Img($document["carImage$i"]);
      }
      }
      else
      {
      $document["carImage$i"]="";
      }
      }


      $addCar                    = $obj->addCar($document);

      if($addCar)
      {
      $isSuccess      = true;
      $message        = "Car added successfully";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "تم إضافة الإعلان بنجاح"; }
      $data           = $addCar;
      }
      else
      {
      $isSuccess      = false;
      $message        = "Car could not add";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "لم نتمكن من إضافة الإعلان حاول مرة أخرى"; }
      $data           = array();
      }
      }
      else
      {
      $isSuccess      = false;
      $message        = "Invaid Json Input";
      if(isset($json['locale']) and $json['locale']=="ar"){ $message        = "خطأ في النظام"; }
      $data           = array();
      }


      // if($isSuccess=="true")
      // {
      ///    $pushNotification                    = $obj->pushNotification($document);
      // }
      echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
      }
     */

    public function addCarWithPush() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {

            $json = json_decode($json, true);
            $document = array();

            $document['userId'] = $json['userId'];
            $document['carCondition'] = $json['carCondition'];
            $document['carMake'] = $json['carMake'];
            $document['carYear'] = $json['carYear'];
            $document['carModel'] = $json['carModel'];
            $document['carPrice'] = $json['carPrice'];
            $document['carTrans'] = $json['carTrans'];
            $document['carFuel'] = $json['carFuel'];
            $document['carCity'] = $json['carCity'];
            $document['carMileage'] = $json['carMileage'];
            $document['carCylinders'] = $json['carCylinders'];
            $document['carColor'] = $json['carColor'];
            $document['carComments'] = $json['carComments'];
            $document['isPaid'] = $json['isPaid'];
            //$document['carCityName']        = $json['carCityName'];
            //$document['carColorName']        = $json['carColorName'];
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['address'] = $json['address'];
            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['phone'] = $json['phone'];

            //$count                          = $json['count']+1;

            $obj = new Car_model;


            $isSuccess = false;

            //for($i=1;$i<$count;$i++)
            for ($i = 1; $i < 11; $i++) {
                if (isset($json["carImage$i"])) {
                    if ($json["carImage$i"] == "") {
                        $document["carImage$i"] = "";
                    } else {
                        $document["carImage$i"] = $json["carImage$i"];
                        $document["carImage$i"] = $obj->makeBase64Img($document["carImage$i"]);
                    }
                } else {
                    $document["carImage$i"] = "";
                }
            }

            //$addCar = 12 ;
            $addCar = $obj->addCar($document);
            if ($addCar) {

                $isSuccess = true;
                $message = "Car added successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم إضافة الإعلان بنجاح";
                }
                $data = $addCar;
            } else {
                $isSuccess = false;
                $message = "Car could not add";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم نتمكن من إضافة الإعلان حاول مرة أخرى";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function addCarWithPushNew() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {

            $json = json_decode($json, true);
            $document = array();

            $document['userId'] = $json['userId'];
            $document['carCondition'] = $json['carCondition'];
            $document['carMake'] = $json['carMake'];
            $document['carYear'] = $json['carYear'];
            $document['carModel'] = $json['carModel'];
            $document['carPrice'] = $json['carPrice'];
            $document['carTrans'] = $json['carTrans'];
            $document['carFuel'] = $json['carFuel'];
            $document['carCity'] = $json['carCity'];
            $document['carMileage'] = $json['carMileage'];
            $document['carCylinders'] = $json['carCylinders'];
            $document['carColor'] = $json['carColor'];
            $document['carComments'] = $json['carComments'];
            $document['isPaid'] = $json['isPaid'];
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['address'] = $json['address'];
            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['phone'] = $json['phone'];


            $obj = new Car_model;


            $isSuccess = false;

            $addCar = $obj->addCar($document);
            if ($addCar) {


                $isSuccess = true;
                $message = "Car added successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم إضافة الإعلان بنجاح";
                }
                $data = $addCar;
            } else {
                $isSuccess = false;
                $message = "Car could not add";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم نتمكن من إضافة الإعلان حاول مرة أخرى";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    //upload Car images as form data..
    public function UploadCarImage() {
        //echo "<pre>";print_r($_FILES);print_r($_REQUEST);die;
        if ($_FILES || $_REQUEST) {
            $user_data['carId'] = $_REQUEST['carId'];
            for ($i = 1; $i < 11; $i++) {

                if ($_FILES && array_key_exists("carImage$i", $_FILES) && $_FILES["carImage$i"]['name'] != "") {
                    $user_data["carImage$i"] = $this->uploadfile("carImage$i", 'carImages');
                } else if ($_REQUEST && array_key_exists("carImage$i", $_REQUEST) && $_REQUEST["carImage$i"] != "") {
                    $str = $_REQUEST["carImage$i"];
                    $pos = strpos($str, "carImages");
                    if ($pos > 0) {
                        $path_parts = pathinfo($_REQUEST["carImage$i"]);
                        $image = $path_parts['basename'];
                        $user_data["carImage$i"] = $image;
                    }
                }
            }

            $EditcarId = $this->Car_model->editProfile($user_data);

            $isSuccess = true;
            $message = "Data uploaded successfully.";
            $data = $EditcarId;
        } else {
            $isSuccess = false;
            $message = "Invalid Input data.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function editCarWoImage() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $document['carId'] = $json['carId'];
            $document['userId'] = $json['userId'];

            $document['carCondition'] = $json['carCondition'];
            $document['carMake'] = $json['carMake'];
            $document['carYear'] = $json['carYear'];
            $document['carModel'] = $json['carModel'];
            $document['carPrice'] = $json['carPrice'];
            $document['carTrans'] = $json['carTrans'];
            $document['carFuel'] = $json['carFuel'];
            $document['carCity'] = $json['carCity'];
            $document['carMileage'] = $json['carMileage'];
            $document['carCylinders'] = $json['carCylinders'];
            $document['carColor'] = $json['carColor'];
            $document['carComments'] = $json['carComments'];
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['address'] = $json['address'];
            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['phone'] = $json['phone'];

            $isSuccess = false;
            $obj = new Car_model;

            $editCar = $obj->editCar($document);

            if ($editCar) {
                $isSuccess = true;
                $message = "Car edited successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم تعديل الإعلان بنجاح";
                }
                $data = $editCar;
            } else {
                $isSuccess = false;
                $message = "Car could not update";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم نتمكن من تعديل الإعلان";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function uploadfile($key, $folder_on_root) {
        $folder_on_root = $folder_on_root . "/";
        $files = $_FILES[$key];
        if ($files['error'] == 0) {
            $file_name = rand(1, 10) . time() . basename($_FILES[$key]["name"]);
            $target_file = $folder_on_root . $file_name;
            move_uploaded_file($_FILES[$key]['tmp_name'], $target_file);
            return $file_name;
        }
    }

    public function editCarnew() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $document['carId'] = $json['carId'];
            $document['userId'] = $json['userId'];

            $document['carCondition'] = $json['carCondition'];
            $document['carMake'] = $json['carMake'];
            $document['carYear'] = $json['carYear'];
            $document['carModel'] = $json['carModel'];
            $document['carPrice'] = $json['carPrice'];
            $document['carTrans'] = $json['carTrans'];
            $document['carFuel'] = $json['carFuel'];
            $document['carCity'] = $json['carCity'];
            //$document['carCityName']        = $json['carCityName'];
            $document['carMileage'] = $json['carMileage'];
            $document['carCylinders'] = $json['carCylinders'];
            $document['carColor'] = $json['carColor'];
            //$document['carColorName']           = $json['carColorName'];
            $document['carComments'] = $json['carComments'];
            $document['carComments'] = $json['carComments'];

            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['address'] = $json['address'];
            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['phone'] = $json['phone'];

            $isSuccess = false;
            $obj = new Car_model;

            $query = '';

            for ($i = 1; $i < 11; $i++) {
                if (isset($json["carImage$i"]) and $json["carImage$i"] != "") {
                    $str = $json["carImage$i"];
                    $pos = strpos($str, "carImages");
                    if ($pos > 0) {
                        $path_parts = pathinfo($json["carImage$i"]);
                        $image = $path_parts['filename'] . '.' . $path_parts['extension'];

                        $document["carImage$i"] = $image;
                    } else {
                        $img = $json["carImage$i"];
                        $document["carImage$i"] = $obj->makeBase64Img($img);
                    }
                } else if (isset($json["carImage$i"]) and $json["carImage$i"] == "") {
                    $document["carImage$i"] = "";
                }
            }

            // echo $query;die();

            $editCar = $obj->editCar($document);

            if ($editCar) {
                $isSuccess = true;
                $message = "Car edited successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم تعديل الإعلان بنجاح";
                }
                $data = $editCar;
            } else {
                $isSuccess = false;
                $message = "Car could not update";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم نتمكن من تعديل الإعلان";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function editCar() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            //$json                           = json_decode($json,true);
            $document = array();

            $document['carId'] = $_REQUEST['carId'];
            $document['userId'] = $_REQUEST['userId'];

            $document['carCondition'] = $_REQUEST['carCondition'];
            $document['carMake'] = $_REQUEST['carMake'];
            $document['carYear'] = $_REQUEST['carYear'];
            $document['carModel'] = $_REQUEST['carModel'];
            $document['carPrice'] = $_REQUEST['carPrice'];
            $document['carTrans'] = $_REQUEST['carTrans'];
            $document['carFuel'] = $_REQUEST['carFuel'];
            $document['carCity'] = $_REQUEST['carCity'];
            $document['carMileage'] = $_REQUEST['carMileage'];
            $document['carCylinders'] = $_REQUEST['carCylinders'];
            $document['carColor'] = $_REQUEST['carColor'];
            $document['carComments'] = $_REQUEST['carComments'];

            $document['latitude'] = $_REQUEST['latitude'];
            $document['longitude'] = $_REQUEST['longitude'];
            $document['address'] = $_REQUEST['address'];
            $document['name'] = $_REQUEST['name'];
            $document['email'] = $_REQUEST['email'];
            $document['phone'] = $_REQUEST['phone'];
            $isSuccess = false;
            $obj = new Car_model;

            $query = '';

            for ($i = 1; $i < 11; $i++) {
                if (isset($_FILES["carImage$i"]['name'])) {
                    $document["carImage$i"] = $obj->uploadCardImage("carImage$i");
                }
            }

            // echo $query;die();

            $editCar = $obj->editCar($document);
            if ($editCar) {
                $isSuccess = true;
                $message = "Car edited successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم تعديل الإعلان بنجاح";
                }
                $data = $editCar;
            } else {
                $isSuccess = false;
                $message = "Car could not update";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم نتمكن من تعديل الإعلان";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function carLike() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['carId'] = $json['carId'];
            $document['isLike'] = $json['isLike'];

            $islike = $json['isLike'];

            $isSuccess = false;
            $obj = new Car_model;

            $searchIsLike = $obj->searchIsLike($document); //from Carlike
            //print_r($searchIsLike); die;

            if (!empty($searchIsLike)) {
                $searchIsLike = $searchIsLike['isLike'];
                $updateIsLike = $obj->updateIsLike($document);
                $countTotalLikes = $obj->countTotalLikes($document);
                $document['totalLikes'] = $countTotalLikes;
                $countTotalDislikes = $obj->countTotalDislikes($document);
                $document['totalDislikes'] = $countTotalDislikes;
                //$updateTotalDislikes            = $obj->updateTotalDislikes($document);

                if ($searchIsLike == 0 and $islike == 0) {
                    $isSuccess = false;
                    $message = "Already Disliked";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الغاء الإعجاب";
                    }
                } else if ($searchIsLike == 1 and $islike == 1) {
                    $isSuccess = false;
                    $message = "Already Liked";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الاعجاب";
                    }
                } else if ($searchIsLike == 0 and $islike == 1) {
                    //$updateTotalDislike                   = $obj->updateTotalDislike($document);
                    //$countTotalLikes                        = $obj->countTotalLikes($document);
                    $document['totalLikes'] = $countTotalLikes + 1;
                    $updateTotalLikes = $obj->updateTotalLikes($document);

                    //$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes'] = $countTotalDislikes - 1;
                    $updateTotalDislikes = $obj->updateTotalDislikes($document);

                    $isSuccess = true;
                    $message = "Liked successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الاعجاب";
                    }
                } else { //if($searchIsLike==1 and $islike==0)
                    //$countTotalLikes                        = $obj->countTotalLikes($document);
                    $document['totalLikes'] = $countTotalLikes - 1;
                    $updateTotalLikes = $obj->updateTotalLikes($document);

                    //$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes'] = $countTotalDislikes + 1;
                    $updateTotalDislikes = $obj->updateTotalDislikes($document);

                    $isSuccess = true;
                    $message = "Dislike successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الغاء الاعجاب";
                    }
                }

                //$getCarLikes                                = $obj->getCarLikes($document['carId']); //from Carlike
                //$data                                       = $getCarLikes;
            } else {
                $insertIsLike = $obj->insertIsLike($document);
                //$getCarLikes                              = $obj->getCarLikes($document['carId']); //from Carlike
                $countTotalLikes = $obj->countTotalLikes($document);
                $countTotalDislikes = $obj->countTotalDislikes($document);
                if ($islike == 1) {
                    $document['totalLikes'] = $countTotalLikes + 1;
                    $updateTotalLikes = $obj->updateTotalLikes($document);

                    $isSuccess = true;
                    $message = "Liked successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الاعجاب";
                    }
                } else {
                    //$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes'] = $countTotalDislikes + 1;
                    $updateTotalDislikes = $obj->updateTotalDislikes($document);

                    $isSuccess = true;
                    $message = "Disliked successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الغاء الاعجاب";
                    }
                }
            }
            $getCarLikes = $obj->getCarLikes($document['carId']); //from Carlike
            $data = $getCarLikes;

            // $data = $document['totalLikes'];
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }

        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function addFav() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['carId'] = $json['carId'];
            $document['userId'] = $json['userId'];
            $document['isFav'] = $json['isFav'];

            $isSuccess = false;
            $obj = new Car_model;

            $isFavExist = $obj->isFavExist($document);
            if (!empty($isFavExist)) {
                $getCar = $obj->getCar($document);
                if (!empty($getCar)) {
                    $carModel = $getCar['carModel'];
                    $document['carModel'] = $getCar['carModel'];
                    $isFavModels = $obj->isFavModels($document);
                    if ($isFavModels and $document['isFav'] == 1) {
                        $isSuccess = false;
                        $message = "Same model could not fav again";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم تفضيل هذا الموديل مسبقاً";
                        }
                        $data = array();
                    } else {
                        unset($document['carModel']);
                        $updateFav = $obj->updateFav($document);
                        if ($updateFav) {
                            $isSuccess = true;
                            if ($document['isFav'] == 0) {
                                $message = "Fav updated successfully";
                                if (isset($json['locale']) and $json['locale'] == "ar") {
                                    $message = "تم تحديث المفضلة";
                                }
                            } else {
                                $message = "Fav added successfully";
                                if (isset($json['locale']) and $json['locale'] == "ar") {
                                    $message = "تم الإضافة إلى المفضلة";
                                }
                            }
                            $data = array();
                        } else {
                            $isSuccess = false;
                            $message = "Fav couldn't update";
                            if (isset($json['locale']) and $json['locale'] == "ar") {
                                $message = "لم يتم تحديث المفضلة";
                            }
                            $data = array();
                        }
                    }
                }
            } else {
                $getCar = $obj->getCar($document);
                if (!empty($getCar)) {
                    $carModel = $getCar['carModel'];
                    $document['carModel'] = $getCar['carModel'];
                    $isFavModels = $obj->isFavModels($document);
                    if ($isFavModels) {
                        $isSuccess = false;
                        $message = "Same model could not fav again";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم تفضيل هذا الموديل مسبقاً";
                        }
                        $data = array();
                    } else {
                        unset($document['carModel']);
                        $addFav = $obj->addFav($document);

                        if ($addFav) {
                            $isSuccess = true;
                            $message = "Fav added successfully";
                            if (isset($json['locale']) and $json['locale'] == "ar") {
                                $message = "تم الإضافة إلى المفضلة";
                            }
                            $data = array();
                        } else {
                            $isSuccess = false;
                            $message = "Fav couldn't add";
                            if (isset($json['locale']) and $json['locale'] == "ar") {
                                $message = "لم يتم تحديث المفضلة";
                            }
                            $data = array();
                        }
                    }
                } else {
                    $isSuccess = false;
                    $message = "car not exist";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يوجد نتائج";
                    }
                    $data = array();
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function blockCar() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['carId'] = $json['carId'];
            $document['userId'] = $json['userId'];
            $document['isSold'] = 1;

            $isSuccess = false;
            $obj = new Car_model;

            $blockCar = $obj->blockCar($document);
            if (!empty($blockCar)) {
                $isSuccess = true;
                $message = "Car Blocked successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم ايقاف طلبات الشراء بنجاح";
                }
                $data = $document['carId'];
            } else {
                $isSuccess = false;
                $message = "wrong details";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تفاصيل غير صحيحة";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function get_Cars() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $document['carId'] = $json['carId'];
            $document['userId'] = $json['userId'];

            $document['sortType'] = $json['sortType'];
            $document['sortOrder'] = $json['sortOrder'];

            $document['carCondition'] = $json['carCondition'];
            $document['carMake'] = $json['carMake'];
            $document['carModel'] = $json['carModel'];
            $document['carColor'] = $json['carColor'];
            //$document['carColorName']       = $json['carColorName'];
            $document['carYear'] = $json['carYear'];
            $document['carTrans'] = $json['carTrans'];
            $document['carFuel'] = $json['carFuel'];
            $document['carCity'] = $json['carCity'];
            //$document['carCityName']        = $json['carCityName'];
            $document['carPrice'] = $json['carPrice'];
            $document['carMileage'] = $json['carMileage'];
            $document['carCylinders'] = $json['carCylinders'];
            $document['count'] = $json['count'];

            $document['keyword'] = $json['keyword'];

            $document['priceFrom'] = $json['priceFrom'];
            $document['priceTo'] = $json['priceTo'];
            $document['locale'] = $json['locale'];

            if (isset($json['carImage']) and $json['carImage'] != "") {
                $document['carImage'] = $json['carImage'];
            }

            if (isset($json['carImageTags']) and $json['carImageTags'] != "") {
                $document['carImageTags'] = $json['carImageTags'];
            }


            $isSuccess = false;
            $obj = new Car_model;

            $get_Cars = $obj->get_Cars($document);
            //pre($get_Cars);die();	
            if ($get_Cars) {
                $isSuccess = true;
                $message = "Car list displayed successfully ";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم عرض قائمة السيارات";
                }
                $TotalCount = $get_Cars[0]['TotalCount'];
                //$carImageTags   = $get_Cars[0]['carImageTags'];
                $carImageTags = $get_Cars[0]['carImageTags'];
                $data = $get_Cars;
            } else {
                $isSuccess = false;
                $message = "No car found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد نتائج";
                }
                $data = array();
                $TotalCount = 0;
                $carImageTags = "";
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
            $carImageTags = "";
        }
        $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
        fwrite($myfile, json_encode($data));
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "carImageTags" => $carImageTags, "Result" => $data));
    }

    public function getAdminSetting() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['deviceToken'] = $json['deviceToken'];
            $document['deviceType'] = $json['deviceType'];


            $isSuccess = false;
            $obj = new Car_model;

            $getAdminSetting = $obj->getAdminSetting($document);

            if ($getAdminSetting) {
                $isSuccess = true;
                $message = "Admin payment option";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "خيار الدفع";
                }
                $data = $getAdminSetting;
            } else {
                $isSuccess = false;
                $message = "no payment option found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم يتم العثور على خيار للدفع";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

////////////////////---------------Start--------Motary M4--------Start---------------///////////////////////


    public function favCars() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {

            $json = json_decode($json, true);
            $document = array();

            $document['userId'] = $json['userId'];
            $document['count'] = $json['count'];

            $isSuccess = false;
            $obj = new Car_model;

            $favCars = $obj->favCars($document);

            if ($favCars) {
                $isSuccess = true;
                $message = "Car list displayed successfully ";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم عرض قائمة السيارات";
                }
                $data = $favCars;
                $TotalCount = $favCars[0]['TotalCount'];
            } else {
                $isSuccess = false;
                $message = "no  car found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد نتائج";
                }
                $TotalCount = 0;
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function getSoldCars() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            // $document['isSold']             = $json['isSold']; 
            // $document['userId']             = $json['userId'];
            $document['count'] = $json['count'];
            $isSuccess = false;
            $obj = new Car_model;

            $getSoldCars = $obj->getSoldCars($document);

            if (!empty($getSoldCars)) {
                $isSuccess = true;
                $message = "Result display";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "إظهار النتائج";
                }
                $data = $getSoldCars;
                $TotalCount = $getSoldCars[0]['TotalCount'];
            } else {
                $isSuccess = true;
                $message = "No car found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد نتائج";
                }
                $data = array();
                $TotalCount = 0;
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    //////////////////--------End--------Motary M4--------End--------///////////////////////



    public function reportCar() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['carId'] = $json['carId'];
            $document['userId'] = $json['userId'];

            $isSuccess = false;
            $obj = new Car_model;

            $isReported = $obj->isReported($document);

            if (empty($isReported)) {
                $reportCar = $obj->reportCar($document);
                if (!empty($reportCar)) {
                    $isSuccess = true;
                    $message = "Car reported successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم التبليغ عن الإعلان بنجاح";
                    }
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Car could not report";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يمكن التبيلغ عن هذا الإعلان";
                    }
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "Already reported";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم التبليغ مسبقا";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function GetCarHistory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['count'] = $json['count'];
            $document['locale'] = $json['locale'];
            $isSuccess = false;
            $obj = new Car_model;
            $GetCarHistory = $obj->GetCarHistory($document);

            if (!empty($GetCarHistory)) {
                $isSuccess = true;
                $message = "Result display";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "إظهار النتائج";
                }
                $data = $GetCarHistory;
                $TotalCount = $GetCarHistory[0]['TotalCount'];
            } else {
                $isSuccess = true;
                $message = "No car found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد نتائج";
                }
                $data = array();
                $TotalCount = 0;
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function GetPlateHistory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['count'] = $json['count'];
            $document['locale'] = $json['locale'];
            $isSuccess = false;
            $obj = new Car_model;

            $GetPlateHistory = $obj->GetPlateHistory($document);


            if (!empty($GetPlateHistory)) {
                $isSuccess = true;
                $message = "Result display";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "إظهار النتائج";
                }
                $data = $GetPlateHistory;
                $TotalCount = $GetPlateHistory[0]['TotalCount'];
            } else {
                $isSuccess = true;
                $message = "No Plate found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد لوحات";
                }
                $data = array();
                $TotalCount = 0;
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function pushNotificationFavModel() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['isPushNotification'] = $json['isPushNotification'];
            $isSuccess = false;
            $obj = new Car_model;

            $updatePushNotification = $obj->updatePushNotification($document);
            //pre($updatePushNotification);die();
            if (!empty($updatePushNotification)) {
                $isSuccess = true;
                $message = "Record updated";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "سجل تحديث";
                }
                $status = $updatePushNotification[0]['isPushNotification'];
                $data = array();
            } else {
                $isSuccess = true;
                $message = "Record could not update";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا يمكن إجراء التعديل";
                }
                $status = "";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $status = "";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Status" => $status));
    }

    public function GetPayment() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['table'] = $json['table'];
            $document['type'] = $json['type'];

            $isSuccess = false;
            $obj = new Car_model;

            $GetPaymentSuccess = $obj->GetPayment($document);

            if (!empty($GetPaymentSuccess)) {
                $isSuccess = true;
                $message = "Record Display";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "إظهار النتائج";
                }
                $data = $GetPaymentSuccess;
            } else {
                $isSuccess = true;
                $message = "Record could not update";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا يمكن إجراء التعديل";
                }

                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $status = "";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "data" => $data));
    }

    public function paidStatus() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $document = json_decode($json, true);

            $isSuccess = false;
            $obj = new Car_model;

            $document['paidStatus'] = ($document['paymentStatus'] == 'success' || $document['paymentStatus'] == 'Success') ? 1 : 0;
            $document['fieldName'] = ($document['type'] == 'car') ? 'carId' : 'plateId';
            $GetPaidStatus = $obj->checkPaidStatus($document);

            if (!empty($GetPaidStatus)) {
                
                        
                $statusUpdate = $obj->paidStatusUpdate($document);
                if ($statusUpdate == 1) {
                    $isSuccess = true;
                    $message = "Successfully updated";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "إظهار النتائج";
                    }
                } else {
                    $isSuccess = true;
                    $message = "Record could not be updated";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يمكن إجراء التعديل";
                    }
                }
            } else {
                $isSuccess = true;
                $message = "Record not found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا يمكن إجراء التعديل";
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $status = "";
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message));
    }

}
