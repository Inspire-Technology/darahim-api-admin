<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plate extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Plate_model');
        $this->load->library('email');
//$this->load->helper('aes'); 
    }

    /*
     * Modified By      :Amit Kumar
     * Modified Date    :14Jan,16
     * Description      : Function used for user registration 
     */

    
    
    
    
    public function addPlateWithPush() 
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
        {
            $json = json_decode($json, true);
            //$date = date('Y-m-d H:i:s');
            $document = array();
            $obj = new Plate_model;
            
            $document['userId'] = $json['userId'];
            if (isset($json['plateId']) and $json['plateId'] != "") {
                $document['plateId'] = $json['plateId'];
            }
            //$document['plateCategory'] = $json['plateCategory'];
            $document['plateCategory'] = $json['plateCategory'];
            $document['plateNumber'] = $json['plateNumber'];
            $document['name'] = $json['name'];
            $document['platePrice'] = $json['platePrice'];
            $document['plateCity'] = $json['plateCity'];
            //$document['modifyDate'] = $date;
            
            if(isset($document['plateCity']) && $document['plateCity'] != "")
            { 
                $table                      = "plateCity";
                $name                       = $document['plateCity'];
                $id                         = $obj->findName2Id($table,$name);
                if(!empty($id))
                {
                    $document['plateCity']      = $id['id'];
                }
                else
                {
                    $document['plateCity']      = '0';
                    $document['plateCityOther'] = $json['plateCity']; 
                }
            }
            
            if (isset($_FILES['plateImage']['name'])) 
            {
                $document['plateImage'] = $obj->uploadPlateImage("plateImage");
            } 
            else 
            {
                $document['plateImage'] = "";
            }

            $isSuccess = false;

            $addPlate = $obj->addPlate($document);
            if ($addPlate) {
                $isSuccess = true;
                $message = "Plate added successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تمت إضافة اللوحة بنجاح";
                }
                $data = $addPlate;
            } else {
                $isSuccess = false;
                $message = "This Plate Already Exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "هذه اللوحة موجودة مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }


//        if ($isSuccess == "true") {
//            $pushNotificationAddPlate = $obj->pushNotificationAddPlate($document['plateModel']);
//            if ($pushNotificationAddPlate) {
////echo ""; die('yes');
//            }
//        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

 

    public function getPlates() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $isSuccess = false;
            $message = "got";
            $obj = new Plate_model;

            //$document['createDate'] = $json['createDate'];
            $document['count']          = $json['count'];
            $document['plateId']        = $json['plateId'];
            $document['platePrice']     = $json['platePrice'];
            $document['plateCity']      = $json['plateCity'];
            $document['plateCategory']  = $json['plateCategory'];
            $document['plateNumber']    = $json['plateNumber'];
            $document['name']           = $json['name'];
            $document['priceFrom']      = $json['priceFrom'];
            $document['priceTo']        = $json['priceTo'];
            $document['sortType']       = $json['sortType'];
            $document['sortOrder']      = $json['sortOrder'];
            $document['userId']         = $json['userId'];
            $document['keyword']        = $json['keyword'];
            $locale                     = $json['locale'];
            //pre($json['plateCity']); die();
            if(isset($json['plateCity']) && $json['plateCity'] != "")
            { 
                $table                      = "plateCity";
                $name                       = $json['plateCity'];
                $id                         = $obj->findName2Id($table,$name);
                if(!empty($id))
                {
                    $document['plateCity']      = $id['id'];
                }
                else
                {
                    $document['plateCity']      = '0';
                    $document['plateCityOther'] = $json['plateCity']; 
                }
            }
            //pre($document); die();
            $get_Plates1 = $obj->get_Plates($document);
            //pre();
            if($get_Plates1)
            {
                $get_Plates = array();
                foreach ($get_Plates1 as $key=>$value)
                {
                    if($value['plateCity'] != "")
                    { 
                        if($value['plateCity']==0)
                        {
                            $value['plateCity']      = $value['plateCityOther'];
                        }
                        else
                        {
                            $table                      = "plateCity";
                            $id                         = $value['plateCity'];
                            $name                       = $obj->findId2Name($table,$id,$locale);
                            if(!empty($name))
                            {
                                $value['plateCity']      = $name['name'];
                            }
                            else
                            {
                                $document['plateCity']      = '0';
                                $document['plateCityOther'] = $json['plateCity']; 
                            }
                        }
                    }

                    array_push($get_Plates, $value);
                }
            }
            if ($get_Plates1) {
                $isSuccess = true;
                $message = "Plate list displayed successfully ";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم عرض اللوحات بنجاح";
                }
                $data = $get_Plates;
                $TotalCount = $get_Plates[0]['totalCount'];
            } else {
                $isSuccess = false;
                $message = "No plate found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد لوحات";
                }
                $data = array();
                $TotalCount = 0;
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function plateLike() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['plateId'] = $json['plateId'];
            $document['isLike'] = $json['isLike'];
            $islike = $json['isLike'];

            $isSuccess = false;
            $obj = new Plate_model;

            $searchIsLike = $obj->searchIsLike($document); //from platelike
//print_r($searchIsLike); die;


            if (!empty($searchIsLike)) {
                $searchIsLike = $searchIsLike['isLike'];
                $updateIsLike = $obj->updateIsLike($document);
                $countTotalLikes = $obj->countTotalLikes($document);
                $document['totalLikes'] = $countTotalLikes;

                $countTotalDislikes = $obj->countTotalDislikes($document);
                $document['totalDislikes'] = $countTotalDislikes;
//$updateTotalDislikes            = $obj->updateTotalDislikes($document);

                if ($searchIsLike == 0 and $islike == 0) {
                    $isSuccess = false;
                    $message = "Already Disliked";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الغاء الإعجاب";
                    }
                } else if ($searchIsLike == 1 and $islike == 1) {
                    $isSuccess = false;
                    $message = "Already Liked";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الاعجاب";
                    }
                } else if ($searchIsLike == 0 and $islike == 1) {
//$updateTotalDislike                   = $obj->updateTotalDislike($document);
//$countTotalLikes                        = $obj->countTotalLikes($document);
                    $document['totalLikes'] = $countTotalLikes + 1;
                    $updateTotalLikes = $obj->updateTotalLikes($document);

//$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes'] = $countTotalDislikes - 1;
                    $updateTotalDislikes = $obj->updateTotalDislikes($document);

                    $isSuccess = true;
                    $message = "Liked successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الاعجاب";
                    }
                } else { //if($searchIsLike==1 and $islike==0)
//$countTotalLikes                        = $obj->countTotalLikes($document);
                    $document['totalLikes'] = $countTotalLikes - 1;
                    $updateTotalLikes = $obj->updateTotalLikes($document);

//$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes'] = $countTotalDislikes + 1;
                    $updateTotalDislikes = $obj->updateTotalDislikes($document);

                    $isSuccess = true;
                    $message = "Dislike successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الغاء الاعجاب";
                    }
                }

//$getplateLikes                                = $obj->getplateLikes($document['plateId']); //from platelike
//$data                                       = $getplateLikes;
            } else {
                $insertIsLike = $obj->insertIsLike($document);
//$getplateLikes                              = $obj->getplateLikes($document['plateId']); //from platelike
                $countTotalLikes = $obj->countTotalLikes($document);
                $countTotalDislikes = $obj->countTotalDislikes($document);
                if ($islike == 1) {
                    $document['totalLikes'] = $countTotalLikes + 1;
                    $updateTotalLikes = $obj->updateTotalLikes($document);

                    $isSuccess = true;
                    $message = "Liked successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الاعجاب";
                    }
                } else {
//$countTotalDislikes                     = $obj->countTotalDislikes($document);
                    $document['totalDislikes'] = $countTotalDislikes + 1;
                    $updateTotalDislikes = $obj->updateTotalDislikes($document);

                    $isSuccess = true;
                    $message = "Disliked successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الغاء الاعجاب";
                    }
                }
            }

            $getplateLikes = $obj->getplateLikes($document['plateId']); //from platelike
            $data = $getplateLikes;

// $data = $document['totalLikes'];
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }

        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function addFav() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['plateId'] = $json['plateId'];
            $document['userId'] = $json['userId'];
            $document['isFav'] = $json['isFav'];

            $isSuccess = false;
            $obj = new Plate_model;

            $isFavExist = $obj->isFavExist($document);
//            print_r($isFavExist);
//            exit();
            if (!empty($isFavExist)) {
                $updateFav = $obj->updateFav($document);
                if ($updateFav) {
                    $isSuccess = true;
                    if ($document['isFav'] == 0) {
                        $message = "Fav updated successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم تحديث المفضلة";
                        }
                    } else {
                        $message = "Fav added successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم الإضافة إلى المفضلة";
                        }
                    }
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Fav couldn't update";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لم يتم تحديث المفضلة";
                    }
                    $data = array();
                }
            } else {
//                $getPlate = $obj->getPlate($document);
//                if (!empty($getPlate)) {
//                    $plateId = $getPlate['plateId'];
//                    $document['plateId'] = $plateId;
//                    $isFavPlate = $obj->isFavModels($document);
//                    if ($isFavPlate) {
//                        $isSuccess = false;
//                        $message = "Same model could not fav again";
//                        $data = array();
//                    } else {
//                        unset($document['plateId']);
                $addFav = $obj->addFav($document);

                if ($addFav) {
                    $isSuccess = true;
                    $message = "Fav added successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم الإضافة إلى المفضلة";
                    }
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Fav couldn't add";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لم يتم تحديث المفضلة";
                    }
                    $data = array();
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function favPlates() 
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
        {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];

            $isSuccess = false;
            $obj = new Plate_model;

            $favPlates1 = $obj->favPlates($document);
            
            $favPlates = array();
            foreach ($favPlates1 as $key=>$value)
            {
                if($value['plateCity'] != "")
                { 
                    if($value['plateCity']==0)
                    {
                        $value['plateCity']      = $value['plateCityOther'];
                    }
                    else
                    {
                        $table                      = "plateCity";
                        $id                         = $value['plateCity'];
                        $name                       = $obj->findId2Name($table,$id,$locale);
                        if(!empty($name))
                        {
                            $value['plateCity']      = $name['name'];
                        }
                        else
                        {
                            $document['plateCity']      = '0';
                            $document['plateCityOther'] = $json['plateCity']; 
                        }
                    }
                }
                array_push($favPlates, $value);
            }
            
            $total = count($favPlates);
            if ($favPlates) {
                $isSuccess = true;
                $message = "Plate list displayed successfully ";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم عرض اللوحات بنجاح";
                }
                $TotalCount = $total;
                $data = $favPlates;
            } else {
                $isSuccess = false;
                $message = "no any plate found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد لوحات";
                }
                $TotalCount = 0;
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function likePlates() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {

            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];


            $isSuccess = false;
            $obj = new Plate_model;

            $likePlates1 = $obj->likePlates($document);
            
            $likePlates = array();
            foreach ($likePlates1 as $key=>$value)
            {
                if($value['plateCity'] != "")
                { 
                    if($value['plateCity']==0)
                    {
                        $value['plateCity']      = $value['plateCityOther'];
                    }
                    else
                    {
                        $table                      = "plateCity";
                        $id                         = $value['plateCity'];
                        $name                       = $obj->findId2Name($table,$id,$locale);
                        if(!empty($name))
                        {
                            $value['plateCity']      = $name['name'];
                        }
                        else
                        {
                            $document['plateCity']      = '0';
                            $document['plateCityOther'] = $json['plateCity']; 
                        }
                    }
                }
                array_push($likePlates, $value);
            }
            $total = count($likePlates);

            if ($likePlates) {
                $isSuccess = true;
                $message = "Plate list displayed successfully ";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم عرض اللوحات بنجاح";
                }
                $TotalCount = $total;
                $data = $likePlates;
            } else {
                $isSuccess = false;
                $message = "no any plate found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد لوحات";
                }
                $TotalCount = 0;
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function editPlatenew() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $date = date('Y-m-d H:i:s');

            $document = array();

            $document['plateId'] = $json['plateId'];
            $document['userId'] = $json['userId'];
            $document['plateNumber'] = $json['plateNumber'];
            $document['platePrice'] = $json['platePrice'];
            //$document['plateCondition'] = $json['plateCondition'];
            $document['plateCity'] = $json['plateCity'];
            $document['modifyDate'] = $date;
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['address'] = $json['address'];
            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['phone'] = $json['phone'];


            $isSuccess = false;
            $obj = new Plate_model;
            
            if(isset($document['plateCity']) && $document['plateCity'] != "")
            { 
                $table                      = "plateCity";
                $name                       = $document['plateCity'];
                $id                         = $obj->findName2Id($table,$name);
                if(!empty($id))
                {
                    $document['plateCity']      = $id['id'];
                }
                else
                {
                    $document['plateCity']      = '0';
                    $document['plateCityOther'] = $json['plateCity']; 
                }
            }
            $editPlate = $obj->editPlate($document);

            if ($editPlate) {
                $isSuccess = true;
                $message = "Plate edited successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم التعديل بنجاح";
                }
                $data = $editPlate;
            } else {
                $isSuccess = false;
                $message = "Plate could not update";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا يمكن التعديل حالياً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function reportplate() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['plateId'] = $json['plateId'];
            $document['userId'] = $json['userId'];

            $isSuccess = false;
            $obj = new plate_model;

            $isReported = $obj->isReported($document);

            if (empty($isReported)) {
                $reportplate = $obj->reportplate($document);
                if (!empty($reportplate)) {
                    $isSuccess = true;
                    $message = "Plate reported successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم التبليغ بنجاح";
                    }
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Plate could not report";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يمكن التبليغ حالياً";
                    }
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "Already reported";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم التبليغ مسبقا";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function updateTotalLikes() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
// $document['isSold']             = $json['isSold']; 
// $document['userId']             = $json['userId'];
            $document['createDate'] = $json['createDate'];
            $isSuccess = false;
            $obj = new Plate_model;

            $getSoldPlates = $obj->getSoldPlates($document);
            if (!empty($getSoldPlates)) {
                $isSuccess = true;
                $message = "Result display";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "إظهار النتائج";
                }
                $data = $getSoldPlates;
                $TotalCount = count($data);
            } else {
                $isSuccess = true;
                $message = "No plate found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا توجد لوحات";
                }
                $data = array();
                $TotalCount = 0;
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
            $TotalCount = 0;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "TotalCount" => $TotalCount, "Result" => $data));
    }

    public function getPlateCategory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['city'] = $json['city'];

            $isSuccess = false;
            $obj = new Plate_model;

            $image = $obj->getPlateCategory($document);

            if (!empty($image)) {
                $isSuccess = true;
                $message = "Plate  Category";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "فئة اللوحة";
                }
                $data = $image;
            } else {
                $isSuccess = false;
                $message = "wrong details";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "معلومات غير صحيحة";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function getPlateAllCategory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $isSuccess = false;
            $obj = new Plate_model;

            $allCategory = $obj->getPlateAllCategory($document);

            if (!empty($allCategory)) {
                $isSuccess = true;
                $message = "Plate All  Category";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "كل الفئات";
                }
                $data = $allCategory;
            } else {
                $isSuccess = false;
                $message = "wrong details";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "معلومات غير صحيحة";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

//============= https://github.com/tazotodua/useful-php-scripts/ ===========

    public function blockPlate() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['plateId'] = $json['plateId'];
            $document['userId'] = $json['userId'];
            $document['isSold'] = 1;

            $isSuccess = false;
            $obj = new Plate_model;

            $blockPlate = $obj->blockPlate($document);

            if (!empty($blockPlate)) {
                $isSuccess = true;
                $message = "Plate Blocked successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم ايقاف طلبات الشراء لهذه اللوحة";
                }
                $data = $document['plateId'];
            } else {
                $isSuccess = false;
                $message = "wrong details";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "معلومات غير صحيحة";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function GetHistory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['count'] = $json['count'];
            $document['userId'] = $json['userId'];

            $isSuccess = false;
            $obj = new Plate_model;

            $HistoryPlate1 = $obj->GetHistory($document);
            
            
            if (!empty($HistoryPlate1)) 
                {
                
                $HistoryPlate = array();
                foreach ($HistoryPlate1 as $key=>$value)
                {
                    if($value['plateCity'] != "")
                    { 
                        if($value['plateCity']==0)
                        {
                            $value['plateCity']      = $value['plateCityOther'];
                        }
                        else
                        {
                            $table                      = "plateCity";
                            $id                         = $value['plateCity'];
                            $name                       = $obj->findId2Name($table,$id,$locale);
                            if(!empty($name))
                            {
                                $value['plateCity']      = $name['name'];
                            }
                            else
                            {
                                $document['plateCity']      = '0';
                                $document['plateCityOther'] = $json['plateCity']; 
                            }
                        }
                    }
                    array_push($HistoryPlate, $value);
                }
                
                $isSuccess = true;
                $message = "Get Plate History successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تفاصيل اللوحات السابقة";
                }
                $data = $HistoryPlate;
            } else {
                $isSuccess = false;
                $message = "wrong details";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "معلومات غير صحيحة";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function GetCity() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['locale'] = $json['locale'];

            $isSuccess = false;
            $obj = new Plate_model;

            $cityName = $obj->GetCityName($document);
            if (!empty($cityName)) {
                $isSuccess = true;
                $message = "Get City Name successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم إظهار المدن بنجاح";
                }
                $data = $cityName;
            } else {
                $isSuccess = false;
                $message = "wrong details";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "معلومات غير صحيحة";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function getAdminSetting() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $isSuccess = false;
            $obj = new Plate_model;

            $getAdminSetting = $obj->getAdminSetting($document);

            if ($getAdminSetting) {
                $isSuccess = true;
                $message = "Admin payment option";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "خيار الدفع";
                }
                $data = $getAdminSetting;
            } else {
                $isSuccess = false;
                $message = "no payment option found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم يتم العثور على خيار للدفع";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

////////////////////---------------Start--------Motary M4--------Start---------------///////////////////////
//////////////////--------End--------Motary M4--------End--------///////////////////////
}
