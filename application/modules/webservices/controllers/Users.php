<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('email');
        //$this->load->helper('push'); 
    }

    /*
     * Modified By      :Amit Kumar
     * Modified Date    :11Jan,16
     * Description      : Function used for user registration 
     */

    public function userSignUp() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];
            $document['loginType'] = $json['loginType'];
            $loginType = $json['loginType'];
            $document['socialId'] = $json['socialId'];
            $isSuccess = false;
            $obj = new user_model;
            if ($loginType == '1' or $loginType == '2' or $loginType == '3') {

                $isUserExistS = $obj->isUserExistS($document);
                //echo $isUserExistS; die;
                if (!$isUserExistS) {
                    $isUserExist = $obj->isUserExist($document);
                    if (!$isUserExist) {
                        $isSuccess = true;
                        $message = "User registered successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم تسجيل الاشتراك بنجاح";
                        }
                        $data = array();
                    } else {
                        $isSuccess = false;
                        $message = "already registered";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "اسم المستخدم مشترك مسبقاً";
                        }
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "already registered";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "اسم المستخدم مشترك مسبقاً";
                    }
                    $data = array();
                }
            } else {
                $isUserExist = $obj->isUserExist($document);
                //echo $isUserExist; die;
                if (!$isUserExist) {
                    $isSuccess = true;
                    $message = "User registered successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم تسجيل الاشتراك بنجاح";
                    }
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "already registered";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "اسم المستخدم مشترك مسبقاً";
                    }
                    $data = array();
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    /*
     * Modified By      :Amit Kumar
     * Modified Date    :11Jan,16
     * Description      : Function used to Create Profile
     */

    public function createProfile() {

        $json = file_get_contents('php://input');
        // $myfile = fopen("logs.txt", "a") or die("Unable to open file!");
        //$txt = $json;
        //fwrite($myfile, "\n". $txt);
        //fclose($myfile);		
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $document['email'] = $json['email'];
            $document['name'] = $json['name'];
            $document['mobile'] = $json['mobile'];
            $document['gender'] = $json['gender'];
            $document['country'] = $json['country'];
            $document['socialId'] = $json['socialId'];
            $document['loginType'] = $json['loginType'];
            $loginType = $document['loginType'];
            $document['password'] = md5($json['password']);
            $document['deviceToken'] = $json['deviceToken'];
            $document['deviceType'] = $json['deviceType'];

            $imageType = $json['imageType'];
            $document['profileImage'] = $json['profileImage'];

            $isSuccess = false;
            $obj = new user_model;

            /*
              if($json['profileImage']!="")
              {
              $profileImage                   = $json['profileImage'];
              }
              else
              {
              $document['profileImage']       = "";
              }

             */
            if ($loginType == 0) {
                $user = $obj->isUserExist($document);

                if (!$user) {

                    if ($document['profileImage'] != NULL or $document['profileImage'] != "") {
                        /*        $document['profileImage'] = "";  
                          }
                          else{ */
                        $document['profileImage'] = $obj->makeBase64Img($document['profileImage']);
                        $document['profileImage'] = base_url() . "profileImages/" . $document['profileImage'];
                    }
                    // print_r($document);die();
                    $results = $obj->createProfile($document);
                    if ($results) {
                        $user = $obj->getProfile($results);
                        //print_r($user);die();
                        // $user['profileImage'] =  "http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/profileImages".$user['profileImage'];

                        $isSuccess = true;
                        $message = "Profile created successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم انشاء الملف الجانبي بنجاح";
                        }
                        $data = $user;
                    } else {
                        $isSuccess = false;
                        $message = "Profile could not create";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "لم نتمكن من انشاء الملف الجانبي";
                        }
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "Profile already exist";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "موجود مسبقاً";
                    }
                    $data = array();
                }
            } else {

                if ($imageType == "url") {
                    $document['profileImage'] = $json['profileImage'];
                } else if ($imageType == "image") {
                    $document['profileImage'] = $obj->makeBase64Img($document['profileImage']);
                    $document['profileImage'] = base_url() . "profileImages/" . $document['profileImage'];
                } else {
                    $document['profileImage'] = "";
                }



                $user = $obj->isUserExistS($document);

                if (!$user) {
                    $results = $obj->createProfile($document);
                    if ($results) {
                        $user = $obj->getProfile($results);
                        $isSuccess = true;
                        $message = "Profile created successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم انشاء الملف الجانبي بنجاح";
                        }
                        $data = $user;
                    } else {
                        $isSuccess = false;
                        $message = "Profile could not create";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "لم نتمكن من انشاء الملف الجانبي";
                        }
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "Profile already exist";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "موجود مسبقاً";
                    }
                    $data = array();
                }
            }
            /*   else
              {
              $user                        = $obj->isUserExistS($document);

              if(!$user)
              {
              if($document['profileImage']==NULL or $document['profileImage']=="")
              {
              $document['profileImage'] = "";
              }
              else{
              $document['profileImage']       = $obj->makeBase64Img($profileImage);
              $document['profileImage']        = base_url()."profileImages/".$document['profileImage'];
              }
              $createProfile                        = $obj->createProfile($document);
              if($createProfile)
              {
              $user           =   $obj->getProfile($createProfile);

              $isSuccess      = true;
              $message        = "Profile created successfully";
              $data           = $user;
              }
              else
              {
              $isSuccess      = false;
              $message        = "Profile could not create";
              $data           = array();
              }
              }
              else
              {
              $isSuccess      = false;
              $message        = "Profile already exist";
              $data           = array();
              }
              } */
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }

        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function UserVerifyCode() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];
            $document['verificationCode'] = $json['otp'];
            //$document['deviceToken']        = $json['deviceToken'];
            $isSuccess = false;
            $message = "";
            $obj = new user_model;
            $results = $obj->UserVerifyCode($document);
            if ($results == 1) {
                $isSuccess = true;
                $message = "OTP verified successfully";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تم التأكد من رمز التحقق";
                }
                $data = array();
            } elseif ($results == 2) {
                $isSuccess = false;
                $message = "OTP mismatched";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "رمز التحقق غير متوافق";
                }
                $data = array();
            } elseif ($results == 3) {
                $isSuccess = false;
                $message = "user does not exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم تقم بالتسجيل مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Modified By      :Raman Chauhan
     * Modified Date    :11Jan,16
     * Description      : Function used for Valet login
     */

    public function userLogin() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['loginType'] = $json['loginType'];
            $loginType = $json['loginType'];
            $document['socialId'] = $json['socialId'];
            $document['email'] = $json['email'];
            $document['password'] = md5($json['password']); //md5($json['password']); 

            $document['deviceToken'] = $json['deviceToken'];
            $document['deviceType'] = $json['deviceType'];

            $isSuccess = false;
            $message = "";
            $obj = new user_model;


            if ($loginType == '1' or $loginType == '2' or $loginType == '3') {
                $isUserExistS = $obj->isUserExistS($document);

                if ($isUserExistS) {
                    $user = $obj->userLoginS($document);
                    if ($user) {
                        $updateProfile = $obj->updateProfile($document);
                        $isSuccess = true;
                        $message = "Login successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم تسجيل الدخول بنجاح";
                        }
                        $messageCode = "1";
                        $data = $user;
                    }
                    /* else
                      {
                      $isSuccess      = false;
                      $message        = "Wrong password!";
                      $messageCode    =  "2";
                      $data           = array();
                      }
                     */
                } else {
                    $isSuccess = false;
                    $message = "User does not exist";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لم تقم بالتسجيل مسبقاً";
                    }
                    $messageCode = "3";
                    $data = array();
                }
            } else {

                $isUserExist = $obj->isUserExist($document);

                if ($isUserExist) {
                    $user = $obj->userLogin($document);
                    //print_r($user);
                    //echo die();
                    if ($user) {
                        $updateProfile = $obj->updateProfile($document);

                        $isSuccess = true;
                        $message = "Login successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم تسجيل الدخول بنجاح";
                        }
                        $messageCode = "1";
                        $user['profileImage'] = $user['profileImage'];
                        $data = $user;
                    } else {
                        $isSuccess = false;
                        $message = "Wrong password!";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "كلمة المرور غير صحيحة";
                        }
                        $messageCode = "2";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "User does not exist";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لم تقم بالتسجيل مسبقاً";
                    }
                    $messageCode = "3";
                    $data = array();
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $messageCode = "";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "messageCode" => $messageCode, "Result" => $data));
    }

    /*
     * Modified By      :Amit kUmar
     * Modified Date    :11Jan,16
     * Description      :Function used to Forget Password 
     */
    /*
      public function forgetPassword()
      {
      $json                               = file_get_contents('php://input');
      if(is_json($json))
      {
      $json                           = json_decode($json,true);
      $document                       = array();
      $document['email']              = $json['email'];

      $isSuccess                      = false;
      $obj                            = new user_model;
      $isUserExist                    = $obj->isUserExist($document);
      if($isUserExist)
      {
      // echo $isUserExist; die;
      $user                       = $obj->getProfile($isUserExist);
      //echo($user['password']);
      //print_r($user); die;
      $password=$document['password'] = $user['password'];
      $email  = $document['email'];

      $config  = array();
      $config['protocol'] = 'mail';
      $this->load->library('email');
      $config = array();
      $config['useragent']           = "CodeIgniter";
      $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
      $config['protocol']            = "smtp";
      $config['smtp_host']           = "localhost";
      $config['smtp_port']           = "25";
      $config['mailtype'] = 'html';
      $config['charset']  = 'utf-8';
      $config['newline']  = "\r\n";
      $config['wordwrap'] = TRUE;
      $this->email->initialize($config);
      $this->email->from($email, 'Motary App');
      $this->email->to('rudra11.raman@gmail.com');
      $msg = "Message from Motary Your password is $password";
      $this->email->subject('Motary App:Your Password');
      $this->email->message($msg);
      if($this->email->send())
      {
      $to = $email;
      $msg = "Your password is ".$password;
      $sub = "Your Password from motary";
      $headers = 'From: Motary <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

      mail($to,$sub,$msg,$headers);

      $isSuccess      = true;
      $message        = "Password sent to mail successfully";
      $data           = array();
      }
      else
      {
      $isSuccess      = false;
      $message        = "sorry, password could not sent";
      $data           = array();
      //echo $this->email->print_debugger();
      }

      }
      else
      {
      $isSuccess      = false;
      $message        = "User not exist";
      $data           = array();
      }
      }
      else
      {
      $isSuccess      = false;
      $message        = "Invaid Json Input";
      $data           = array();
      }
      echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
      }

     */

    public function forgetPasswordByOTP() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];
            $document['verificationCode'] = rand(1000, 9999);

            $isSuccess = false;
            $obj = new user_model;
            $isUserExist = $obj->isUserExist($document);

            if ($isUserExist) {
                if ($isUserExist['loginType'] == 1 and $isUserExist['password'] == "") {
                    $isSuccess = false;
                    $message = "you can't retrieve password of your social account";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يمكن استرجاع كلمة المرور لهذا الحساب";
                    }
                    $data = array();
                } else if ($isUserExist['loginType'] == 2 and $isUserExist['password'] == "") {
                    $isSuccess = false;
                    $message = "you can't retrieve password of your social account";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يمكن استرجاع كلمة المرور لهذا الحساب";
                    }
                    $data = array();
                } else if ($isUserExist['loginType'] == 3 and $isUserExist['password'] == "") {
                    $isSuccess = false;
                    $message = "you can't retrieve password of your social account";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لا يمكن استرجاع كلمة المرور لهذا الحساب";
                    }
                    $data = array();
                } else {
                    $updateProfile = $obj->updateProfile($document);
                    $to = $document['email'];
                    $msg = "Your OTP is " . $document['verificationCode'];
                    $sub = "Your OTP from motary";
                    $headers = 'From: Motary <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

                    $sentmail = mail($to, $sub, $msg, $headers);
                    if (!empty($sentmail)) {
                        $isSuccess = true;
                        $message = "OTP sent to your mail successfully";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "تم إرسال رمز التحقق الى البريد الإلكتروني المسجل";
                        }
                        $data = array();
                    } else {
                        $isSuccess = false;
                        $message = "sorry, OTP could not sent";
                        if (isset($json['locale']) and $json['locale'] == "ar") {
                            $message = "لم نتمكن من إرسال رمز التحقق";
                        }
                        $data = array();
                    }
                }
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم تقم بالتسجيل مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    /*
     * Modified By      :Amit kUmar
     * Modified Date    :11Jan,16
     * Description      :Function used to change Password 
     */

    public function changePassword() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];
            $document['password'] = md5($json['newPassword']);

            $isSuccess = false;
            $obj = new user_model;


            $isUserExist = $obj->isUserExist($document);

            if ($isUserExist) {
                $results = $obj->changePassword($document);
                if (!empty($results)) {
                    $isSuccess = true;
                    $message = "Password changed successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم تحديث كلمة المرور";
                    }
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "password could not change";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لم نتمكن من تغيير كلمة المرور";
                    }
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم تقم بالتسجيل مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

//////////////////---------------Start--------Motary M4--------Start-----------------///////////////////////


    public function userProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $isSuccess = false;
            $obj = new User_model;

            $userProfile = $obj->userProfile($document);
            if (!empty($userProfile)) {
                $isSuccess = true;
                $message = "User details display";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "تفاصيل المستخدم";
                }
                $data = $userProfile;
            } else {
                $isSuccess = false;
                $message = "User could not found";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لا يوجد مستخدم بهذا الاسم";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function editProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];

            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['mobile'] = $json['mobile'];
            $document['gender'] = $json['gender'];
            $document['country'] = $json['country'];
            $document['socialId'] = $json['socialId'];
            //$document['loginType']          = $json['loginType'];
            //$document['password']           = $json['password'];
            $document['deviceToken'] = $json['deviceToken'];
            $document['deviceType'] = $json['deviceType'];

            if ($json['profileImage'] != "") {
                $document['profileImage'] = $json['profileImage'];
                $imageType = $json['imageType'];
            }


            $isSuccess = false;
            $obj = new user_model;

            $isUserIdExist = $obj->isUserIdExist($document);
            if ($isUserIdExist) {
                if ($json['profileImage'] != "" and $imageType == "image") {
                    $document['profileImage'] = $obj->makeBase64Img($document['profileImage']);
                    $document['profileImage'] = base_url() . "profileImages/" . $document['profileImage'];
                }

                $editProfile = $obj->editProfile($document);

                if ($editProfile) {
                    $userProfile = $obj->userProfile($document);

                    $isSuccess = true;
                    $message = "Profile has been updated successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم تحديث الملف الجانبي بنجاح";
                    }
                    $data = $userProfile;
                } else {
                    $isSuccess = false;
                    $message = "Profile could not update";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "لم نتمكن من تحديث الملف الجانبي";
                    }
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم تقم بالتسجيل مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }

        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

//////////////////--------------End--------Motary M4--------End-------------///////////////////////



    public function chatPush() {
        $hello = array();
        $msg = array();
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['message'] = $json['message'];
            $document['time'] = $json['time'];
            $document['senderId'] = $json['senderId'];
            $document['senderImage'] = $json['senderImage'];
            $isSuccess = false;
            $obj = new User_model;

            $isUserIdExist = $obj->isUserIdExist($document);

            if ($isUserIdExist) {
                $userProfile = $obj->userProfile($document);
                $message = $document['message'];
                // $msg                      = $json;
                $msg = $document;
                //$msg                        = json_encode($json,true);

                $hello['userId'] = $document['userId'];
                $hello['message'] = $document['message'];
                $hello['time'] = $document['time'];
                $hello['senderId'] = $document['senderId'];
                $hello['senderImage'] = $document['senderImage'];
                $hello['pushType'] = 1; // chat push
                $msg = $hello;

                $msg = json_encode($msg, true);






                $sendPush = generatePush($userProfile['deviceType'], $userProfile['deviceToken'], $msg);
                //$sendPush                   = AmazonSNS($userProfile['deviceToken'],$msg);


                if ($sendPush) {
                    $message = "notification sent successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم إرسال التنبيه بنجاح";
                    }
                } else {
                    //$message                    = "notification could not send";
                    $message = "notification sent successfully";
                    if (isset($json['locale']) and $json['locale'] == "ar") {
                        $message = "تم إرسال التنبيه بنجاح";
                    }
                }

                $isSuccess = true;
                $data = $hello;
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم تقم بالتسجيل مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function updateLocale() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['userId'] = $json['userId'];
            $document['locale'] = $json['locale'];

            $isSuccess = false;
            $obj = new User_model;

            $isUserIdExist = $obj->isUserIdExist($document);
            if ($isUserIdExist) {
                $userProfile = $obj->UpdateLocale($document);

                $message = "locale updated successfully";
                $isSuccess = true;
                $data = $userProfile;
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                if (isset($json['locale']) and $json['locale'] == "ar") {
                    $message = "لم تقم بالتسجيل مسبقاً";
                }
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            if (isset($json['locale']) and $json['locale'] == "ar") {
                $message = "خطأ في النظام";
            }
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

}
