<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('html');
        $this->load->model('Admin_model');
        $this->load->helper('cookie');

        if ($this->router->fetch_method() != "index") {
            if (empty($this->session->userdata('username'))) {
                redirect('admin');
            }
        }
    }

    public function index() {

        $obj = new admin_model;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userName', 'userName', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if (isset($_REQUEST['login'])) {

            $document = array();
            $document['userName'] = $_REQUEST['userName'];
            $document['password'] = $_REQUEST['password'];

            $user = $obj->isUserExist($document);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('login');
            } else {
                if ($user == false) {
                    $this->load->view('login');
                } else {
                    $this->session->set_userdata('username', $document['userName']);
                    if (isset($_REQUEST['remember']) && $_REQUEST['remember'] == "Remember Me") {
                        $this->input->set_cookie('user_name', $document['userName'], time() + 86500, '', '/');
                        $this->input->set_cookie('user_password', $document['password'], time() + 86500, '', '/');
                    }
                    redirect('admin/allUsers');
                }
            }
            //$this->form_validation->set_rules('email', 'Email', 'required');
        } else {
            $this->load->view('login');
        }
    }

    public function dashboard() {
        $this->load->view('dashboard_view');
    }

    public function logout() {
        $this->session->unset_userdata('some_name');
        $this->session->sess_destroy();
        redirect('admin', 'refresh');
    }

    public function allUsers() {
        $this->data['query'] = $this->Admin_model->getAllUsers();
        $this->load->view('manage_user', $this->data);
    }

    function viewuser() {
        $id = $_GET['id'];
        $this->data['res'] = $this->Admin_model->get_user($id);
        $this->load->view('view_user', $this->data);
    }

    public function allCars() {
        $this->data['query'] = $this->Admin_model->getallCars();
        $this->load->view('all_cars', $this->data);
        //$this->load->view('allCars');
    }

    public function activeCars() {

        $this->data['query'] = $this->Admin_model->getactiveCars();
        $this->load->view('active_cars', $this->data);
        //  $this->load->view('inactiveCars');
    }

    public function inactiveCars() {
        $this->data['query'] = $this->Admin_model->getinactiveCars();
        $this->load->view('inactive_cars', $this->data);
        //  $this->load->view('inactiveCars');
    }

    function viewcar() {
        $cid = $_GET['id'];
        $oldCarId = $this->Admin_model->getOldCarId($cid);
        $this->data['oldres'] = $this->Admin_model->get_car($oldCarId);
        $this->data['res'] = $this->Admin_model->get_car($cid);
        $this->load->view('view_car', $this->data);
    }

    function editcar() {
        //$id = $this->uri->segment(5); //echo $id;
        $cid = $_GET['id'];
        $this->data['res'] = $this->Admin_model->get_car($cid);
        //$this->load->view('edit_car',$this->data);

        if (!$_POST) {

            $this->load->view('edit_car', $this->data);
        } else {

            //$this->form_validation->set_rules('status', '<strong>Status</strong>', 'required');
            //if ($this->form_validation->run() == FALSE)
            //{
            //	$this->data['error']=validation_errors();
            //	$this->load->view('edit_car',$this->data);
            //}
            //else
            // {  
            $this->Admin_model->update_car($_POST, $cid);
            $this->session->set_flashdata('success', 'Record updated Successfully');
            redirect("admin/editcar?id=$cid");
            //}			
        }
    }

    function editMake() {
        $mid = $_GET['id'];
        $this->data['res'] = $this->Admin_model->getMakeWithLogo($mid);

        if (!$_POST) {
            $this->load->view('editMake', $this->data);
        } else {
            //$this->form_validation->set_rules('status', '<strong>Status</strong>', 'required');
            //if ($this->form_validation->run() == FALSE)
            //{
            //	$this->data['error']=validation_errors();
            //	$this->load->view('edit_car',$this->data);
            //}
            //else
            // {  
            //$this->Admin_model->updateMake($_POST,$mid);
            //redirect("admin/editMake?id=$mid");
            //}	

            $make = array();
            $make['id'] = $_POST['id'];
            $make['name'] = $_POST['name'];
            $make['arabic_name'] = $_POST['arabic_name'];
            $logo = $_FILES['logo']['name'];

            //$alredyExist = $this->Admin_model->isMakeNameExist($_POST);
            //if(empty($alredyExist))
            //{
            if ($logo != "") {
                $logo = 'logo';
                $make['logo'] = $this->Admin_model->uploadCarLogo($logo, $make);
            }

            $updateMake = $this->Admin_model->updateMake($make);

            $this->session->set_flashdata('success', 'Record updated Successfully');
            redirect("admin/editMake?id=" . $make['id']);
            //}
            //else
            //{
            //$this->data['res']=$this->Admin_model->getMakeWithLogo($mid);
            //	$this->session->set_flashdata('success','Sorry! This make alredy exist');
            //	redirect("admin/editMake?id=".$make['id']);						
            //}
        }
    }

    function deleteCar() {
        $cid = $_GET['id'];
        $this->Admin_model->deleteCar($cid);
        $this->session->set_flashdata('success', 'Record deleted Successfully');
        redirect('admin/inactiveCars');
    }

    function deleteMake($id) {
        //die('raman');
        $mid = $id;
        //echo $mid; die();
        $this->Admin_model->deleteMake($mid);
        //echo $this->db->last_query();
        $this->session->set_flashdata('success', 'Record Deleted Successfully');
        redirect('admin/allMake');
    }

    function addMake() {
        if (!$_POST) {
            $this->load->view('addMake');
        } else {
            $make = $_POST['name'];
            $logo = $_FILES['logo']['name'];
            $logo = explode('.', $logo);
            $logo = '.' . $logo['1'];
            $logo = $make . $logo;
            $arabicName = $_POST['arabic_name'];

            $addedmake = $this->Admin_model->addMake($make, $logo, $arabicName);
            if ($addedmake) {
                $logo = 'logo';
                $this->Admin_model->uploadCarLogo($logo, $make);
            }
            $this->session->set_flashdata('success', 'Record added Successfully');
            redirect("admin/addMake");
        }
    }

    function addYear() {
        $this->data['makes'] = $this->Admin_model->getallmake();

        if (!$_POST) {
            $this->load->view('addYear', $this->data);
        } else {
            $alredyExist = $this->Admin_model->isYearExistOnMakeId($_POST);
            //pre($alredyExist); die();
            if (!empty($alredyExist)) {
                $this->session->set_flashdata('success', 'This year already exist');
                redirect("admin/addYear");
            } else {
                $this->Admin_model->addYear($_POST);
                $this->session->set_flashdata('success', 'Record added Successfully');
                redirect("admin/addYear");
            }
        }
    }

    function addModel() {
        $this->data['makes'] = $this->Admin_model->getallmake();
        if (!$_POST) {
            $this->load->view('addModel', $this->data);
        } else {
            $this->Admin_model->addModel($_POST);
            $this->session->set_flashdata('success', 'Record added Successfully');
            redirect("admin/addModel");
        }
    }

    public function allMake() {
        //$this->db->order_by('carmake.id','desc');
        $this->data['make'] = $this->Admin_model->getallmakeWithLogo();
        $this->load->view('allMake', $this->data);
    }

    public function allYear() {
        $this->data['year'] = $this->Admin_model->getallyear();
        $this->load->view('allYear', $this->data);
    }

    public function allModel() {
        $this->data['model'] = $this->Admin_model->getallmodel();
        $this->load->view('allModel', $this->data);
    }

    function allMakeYearModel() {
        $this->data['makes'] = $this->Admin_model->getallmake();
        if (!$_POST) {
            $this->load->view('allMakeYearModel', $this->data);
        } else {
            //$this->Admin_model->addModel($_POST);
            $this->session->set_flashdata('success', 'Record added Successfully');
            redirect("admin/allMakeYearModel");
        }
    }

    public function getmodelbyyear() {
        $makeyear_ids = $_REQUEST['makeyear_ids'];
        $result = $this->Admin_model->getmodelbyyear($makeyear_ids);
        ?>
        <select id="model_id" class="ui-corner-all" name="model_id" required>
            <option value="">--Select--</option>
        <?php foreach ($result as $model) { ?>
                <option value="<?php echo $model['id']; ?>"><?php echo $model['name']; ?></option>
        <?php } ?>
        </select>
        <?php
    }

    public function getyearbymake() {
        $make_id = $_REQUEST['make_id'];
        $result = $this->Admin_model->getyearbymake($make_id);
        ?>
        <select id="makeyear_ids" class="ui-corner-all" name="makeyear_id" required>
            <option value="">--Select--</option>
        <?php foreach ($result as $year) { ?>
                <option value="<?php echo $year['id']; ?>"><?php echo $year['year']; ?></option>
        <?php } ?>
        </select>
        <?php
    }

    function paymentSetting() {
        $this->data['paymentSetting'] = $this->Admin_model->getpaymentSetting();

        if (!$_POST) {
            $this->load->view('paymentSetting', $this->data);
        } else {

            unset($_POST['submit']);
            $this->Admin_model->update_paymentSetting($_POST);
            //$this->load->view('paymentSetting',$this->data);
            $this->session->set_flashdata('success', 'Record Updated Successfully');
            redirect("admin/paymentSetting");
        }
    }

    function paymentModeSetting() {
        $this->data['paymentModeSetting'] = $this->Admin_model->getpaymentSetting();

        if (!$_POST) {
            $this->load->view('paymentModeSetting', $this->data);
        } else {
            unset($_POST['submit']);
            $res = $this->Admin_model->update_paymentSetting($_POST);
            if($res==1){
                $this->session->set_flashdata('success', 'Record Updated Successfully');
            }
            else{
                $this->session->set_flashdata('success', 'Record could not be Updated');
            }
            redirect("admin/paymentModeSetting");
        }
    }

    function changePassword() {
        if (!$_POST) {
            $this->load->view('changePassword');
        } else {
            unset($_POST['submit']);
            $res = $this->Admin_model->changePassword($_POST);
            if ($res == 1) {
                $this->session->set_flashdata('success', 'Password has been changed successfully');
            } else if ($res == 2) {
                $this->session->set_flashdata('failure', 'Password could not confirm');
            } else if ($res == 3) {
                $this->session->set_flashdata('failure', 'Old password is wrong');
            }
            redirect("admin/changePassword");
        }
    }

    public function allPlates() {
        $this->data['query'] = $this->Admin_model->getallPlates();
        $this->load->view('all_plates', $this->data);
    }

    public function activePlates() {
        $this->data['query'] = $this->Admin_model->getactivePlates();
        $this->load->view('active_plates', $this->data);
    }

    public function inactivePlates() {
        $this->data['query'] = $this->Admin_model->getinactivePlates();
        $this->load->view('inactive_plates', $this->data);
    }

    function viewplate() {
        $cid = $_GET['id'];
        $oldPlateId = $this->Admin_model->getOldPlateId($cid);
        $this->data['oldres'] = $this->Admin_model->get_plate($oldPlateId);
        $this->data['res'] = $this->Admin_model->get_plate($cid);
        $this->load->view('view_plate', $this->data);
    }

    function editplate() {
        //$id = $this->uri->segment(5); //echo $id;
        $cid = $_GET['id'];
        $this->data['res'] = $this->Admin_model->get_plate($cid);
        //$this->load->view('edit_car',$this->data);

        if (!$_POST) {
            $this->load->view('edit_plate', $this->data);
        } else {
            $this->Admin_model->update_plate($_POST, $cid);
            $this->session->set_flashdata('success', 'Record updated Successfully');
            redirect("admin/editplate?id=$cid");
        }
    }

    function deleteplate() {
        $cid = $_GET['id'];
        $this->Admin_model->deletePlate($cid);
        $this->session->set_flashdata('success', 'Record deleted Successfully');
        redirect('admin/inactivePlates');
    }

    function plateStatus() {
        $cid = $_GET['id'];
        $pid = $_GET['plateId'];

        $this->Admin_model->updatePlatestatus($cid, $pid);

        //$this->db->query('update mt_plate set plateStatus = "'.$cid.'" where plateId="'.$pid.'"')or die(mysql_error());
        $this->session->set_flashdata('success', 'Record updated Successfully');
        redirect('admin/allPlates');
    }

    function carStatus() {
        $cid = $_GET['id'];
        $pid = $_GET['carId'];

        $this->Admin_model->updatecarstatus($cid, $pid);

        //$this->db->query('update mt_plate set plateStatus = "'.$cid.'" where plateId="'.$pid.'"')or die(mysql_error());
        $this->session->set_flashdata('success', 'Record updated Successfully');
        redirect('admin/allCars');
    }

    function reportCar() {
        $result = $this->Admin_model->GetReportedCars();
//         echo "<pre>";
//         print_r($result);
//         die();
        $data['reportcar'] = $result;
        $this->load->view('reportedCars', $data);
    }

    function reportPlate() {
        $result = $this->Admin_model->GetReportedPlate();
//         echo "<pre>";
//         print_r($result);
//         die();
        $data['reportplate'] = $result;
        $this->load->view('reportedPlate', $data);
    }

    function gen_notification() {

        if (!$_POST) {
            $this->load->view('genPush');
        } else {
            //pre($_POST);die;        
            unset($_POST['submit']);
            $count = $this->Admin_model->genPush($_POST);

            $this->session->set_flashdata('success', "Notification Has Been Sent Successfully To $count Users");
            redirect("admin/gen_notification");
        }
    }

}
