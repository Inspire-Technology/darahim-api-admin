<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Test extends CI_Controller 
{
    function __construct()
    {
            parent::__construct();
           
            $this->load->library('session');
			$this->load->helper(array('form','url'));
			$this->load->helper('html');
            $this->load->model('Admin_model');
            $this->load->helper('cookie');
    }

    public function sendPush()
    {    
   
        $deviceType              = $this->input->get('deviceType');
        $deviceToken             = $this->input->get('deviceToken');
        $message['pushType']     = $this->input->get('pushType');
	$message['message']      = $this->input->get('message');

		generatePush($deviceType, $deviceToken, $message);
    }
}