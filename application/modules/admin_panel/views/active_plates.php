<?php $this->load->view('header'); ?>

<?php $this->load->view('headertop'); ?>

<?php $this->load->view('headernav'); ?>

<?php $sn = 1;
$perpage = 15;
$start = 0; ?>





<div id="main" class="container_16">

    <div class="grid_16">

        <div id="content">

            <div class="nodes index">

                <h2>Active Plates  <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
                <h3 style="color:green; ">
                    <?php
                    if ($this->session->flashdata('success')) {
                        echo $this->session->flashdata('success');
                    }
                    ?>
                </h3> 
                <table cellpadding="10" cellspacing="0" width="100%" border="0" bgcolor="#FFF" class="display" id="example" >

                    <thead>

                        <tr class="hello">
                            <th width="5%">S.N.</th>
                            <th width="13%">Plate Id</th>
                            <th width="11%">User Id</th>
                            <th width="11%">User Name</th>
                            <th width="11%">Plate Number</th>
                            <th width="11%">Plate Price</th>
                            <th width="11%">Plate City</th>
                            <th width="11%">Plate Category</th>
                            <th width="11%">Paid</th>
                            <th width="11%">Create Date</th>

                            <th width="5%">Action</th>

                        </tr>

                    </thead>



                    <tbody>

<?php foreach ($query as $w) { ?>

                            <tr class="record even gradeA">

                                <td><?php echo $sn + $start; ?>.</td>
                                <td><?php echo ucfirst($w['plateId']); ?>&nbsp;</td>
                                <td><?php echo ucfirst($w['userId']); ?>&nbsp;</td>
                                <td><?php echo ucfirst($w['name']); ?>&nbsp;</td>
                                <td><?php echo ucfirst($w['plateNumber']); ?>&nbsp;</td>
                                <td><?php echo ucfirst($w['platePrice']); ?>&nbsp;</td>
                                <td><?php echo ucfirst($w['plateCity']); ?>&nbsp;</td>
                                <td><?php echo ucfirst($w['plateCategory']); ?>&nbsp;</td>
                                <td><?php
                                    if(!empty($w['paymentStatus'])){
                                    echo ucwords($w['paymentStatus']);
                                }
                                else{
                                    if ($w['isPaid'] == '0') {
                                        echo "No";
                                    } else {
                                        echo "Yes";
                                    }
                                }
                                ?>
                                </td>
                                <td><?php echo $w['createDate']; ?>&nbsp;</td>
                                <td nowrap="nowrap" align="center">
                                    <a href="<?php echo base_url("index.php/admin/viewplate?id=$w[plateId]"); ?>"><img src="<?php echo base_url('assets/images/view.jpg'); ?>" alt="View" title="View plate"></a>
                                    <a href="<?php echo base_url("index.php/admin/editplate?id=$w[plateId]"); ?>"><img src="<?php echo base_url('assets/images/edit.jpg') ?>" alt="Edit" title="Edit plate"/></a>
                                    <a href="<?php echo base_url('index.php/admin/deleteplate?id=' . $w['plateId']); ?>"  title="Delete" onclick="return confirm('Are you sure to delete this?')">
                                        <img  src="<?php echo base_url('assets/images/delete.jpg'); ?>" alt="delete" title="Delete" /></a>
                                </td>

                            </tr>

    <?php $sn++;
} ?>
                    </tbody>

                </table>
                <br>
                <br />



            </div>

        </div>
    
    </div>

    <div class="clear">&nbsp;</div>

</div>

<div class="push"></div>



<?php $this->load->view('footer'); ?>