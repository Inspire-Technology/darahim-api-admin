<?php $this->load->view('header'); ?>
<?php $this->load->view('headertop'); ?>
<?php $this->load->view('headernav'); ?>


 <div id="main" class="container_16">
   <div class="grid_16"> 
    <div id="content"> 
     <div class="settings form">
      <h2>Change Password  <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
      <div class="setting">
<div class="input1 text" align="right"></div>
</div>
  <h3 style="color:green; ">
      <?php
      if($this->session->flashdata('success'))
      {
          echo $this->session->flashdata('success');
      }
      ?>
  </h3>
  <h3 style="color:red; ">
      <?php
      if($this->session->flashdata('failure'))
      {
          echo $this->session->flashdata('failure');
      }
      ?>
  </h3>

 <?php if(isset($error)){?><div class="error"><?php echo $error; ?></div><?php }?>
 <?php if(isset($insert_id)){?><div class="error"><?php echo $insert_id; ?></div><?php }?>
<form id="form1" name="form1" method="post" action=""  enctype="multipart/form-data">
       <fieldset> 
        
<!--<div class="select" style="padding-left:200px;"> <a href="javascript:history.go(-1)"></a></div>-->

   
    <div class="setting">
        <div class="input text">
            <label for="Setting0Value">Old Password</label>
            <input type="text" name="oldpass" placeholder="Enter your old password" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" required /><br />
            <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
        </div>
    </div>

    <div class="setting">
        <div class="input text">
            <label for="Setting0Value">New Password</label>
            <input type="text" name="newpass" placeholder="Enter your new password" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" required /><br />
            <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
        </div>
    </div>

    <div class="setting">
        <div class="input text">
            <label for="Setting0Value">Confirm New Password</label>
            <input type="text" name="conpass" placeholder="Confirm your new password" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" required /><br />
            <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
        </div>
    </div>
				   </fieldset> 
       
       <div class="submit"><input type="submit" name="submit" value="Change" onclick="return checkForm();" /></div>
      </form>
      </div>                
      </div> 
      </div> 
   <div class="clear">&nbsp;</div> 
      </div>
	  </div>
<div class="push"></div>

      
<?php $this->load->view('footer'); ?> 


