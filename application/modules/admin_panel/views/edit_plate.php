<?php $this->load->view('header'); ?>
<?php $this->load->view('headertop'); ?>
<?php $this->load->view('headernav'); ?>

<!--http://fordsaratov.ru/manager/fckeditor/integration.htm-->

<div id="main" class="container_16">
   <div class="grid_16"> 
    <div id="content"> 
     <div class="settings form">
      <h2>Edit Plate <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
       <div class="setting">
         <div class="input1 text" align="right"></div>
        </div>
             <?php if(isset($error)){?><div class="error"><?php echo $error; ?></div><?php }?>

          <form id="form1" name="form1" method="post" action="">
           <fieldset> 
        
                 <div class="setting">
                    <div class="input text">
                        <label for="Setting0Value">Plate Id</label>
                        <input type="text" name="plateId" value="<?php echo $res['plateId'];?>" readonly/><br />
                        <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
                    </div>
                </div>
                 
                 <div class="setting">
                    <div class="input text">
                        <label for="Setting0Value">Plate Number</label>
                        <input type="text" name="plateNumber" value="<?php echo ltrim($res['plateNumber'], '0');?>" readonly /><br />
                        <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
                    </div>
                </div>

                 <div class="setting">
                    <div class="input text">
                        <label for="Setting0Value">Plate Price</label>
                        <input type="text" name="platePrice" value="<?php echo $res['platePrice'];?>" readonly /><br />
                        <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
                    </div>
                </div>

                
                <div class="setting">
                    <div class="input text">
                        <label for="Setting0Value">Plate City</label>
                        <input type="text" name="plateCity" value="<?php echo $res['plateCity'];?>" readonly /><br />
                        <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
                    </div>
                </div>

                <div class="setting">
                    <div class="input text">
                        <label for="Setting0Value">Plate Category</label>
                        <input type="text" name="plateCategory" value="<?php echo $res['plateCategory'];?>" readonly /><br />
                        <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
                    </div>
                </div>

                <div class="setting">
                    <div class="input text">
                        <label for="Setting0Value">Create Date</label>
                        <input type="text" name="createDate" value="<?php echo $res['createDate'];?>" readonly /><br />
                        <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
                    </div>
                </div>



                <div class="setting">
                    
                    <div class="input text">
                	<label for="Setting2Value">Paid</label>
                      <?php
                                if(!empty($res['paymentStatus'])){
                                    echo ucwords($res['paymentStatus']);
                                }
                                else{
                                    if ($res['isPaid'] == '0') {
                                        echo "No";
                                    } else {
                                        echo "Yes";
                                    }
                                }
                                    ?>
                </div>
                    
                <div class="input text">
                	<label for="Setting2Value">Status</label>
                      <select name="plateStatus" id="status" style="width:100px;" >
                          <option value="">---Select Status---</option>
                          <option value="1" <?php if($res['plateStatus']=="1"){ echo "selected"; }?>>Active</option>
                          <option value="0" <?php if($res['plateStatus']=="0"){ echo "selected"; }?>>Inactive</option>
                      </select>   
                </div>
                </div>

             

     </fieldset> 

   <div class="submit"><input type="submit" name="submit" value="Update" style="width:100px;" /></div>

   </form>


      </div>                	

      </div> 

      </div> 

 <div class="clear">&nbsp;</div> 

       </div>

<div class="push"></div>


<?php $this->load->view('footer'); ?>



	<script>
		$(document).ready(function()
		{
			 var  u_type_id=<?php //echo $res['u_type'];?>;
			 alert(u_type_id); 
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url();?>index.php/admin/form_ajax/load",
				data: { u_type_id:u_type_id},
				success: function(msg){
					//alert(msg);
					$("#response_field").html(msg);
				}
			});
		});

</script>