<?php $this->load->view('header'); ?>
<?php $this->load->view('headertop'); ?>
<?php $this->load->view('headernav'); ?>


 <div id="main" class="container_16">
   <div class="grid_16"> 
    <div id="content"> 
     <div class="settings form">
      <h2>Add Model  <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
      <div class="setting">
<div class="input1 text" align="right"></div>
</div>
<h3 style="color:green; ">
      <?php
      if($this->session->flashdata('success'))
      {
          echo $this->session->flashdata('success');
      }
      ?>
    </h3>

 <?php if(isset($error)){?><div class="error"><?php echo $error; ?></div><?php }?>
 <?php if(isset($insert_id)){?><div class="error"><?php echo $insert_id; ?></div><?php }?>
<form id="form1" name="form1" method="post" action=""  enctype="multipart/form-data">
       <fieldset> 
        
<!--<div class="select" style="padding-left:200px;"> <a href="javascript:history.go(-1)"></a></div>-->

                 <div class="setting" id="coun">
                     <div class="input text">
                         <label for="Setting0Value">Make</label> 
                         <select name="make_id" id="make_id" required>
                         <option value="">--Select Make--</option>
          <?php	   foreach($makes as $mk)	   { ?>
		  <option value="<?php echo $mk['id']; ?>"> <?php echo $mk['name']; ?></option>
		  <?php } ?>
         </select>
                     </div>
                </div>

                 <div class="setting" id="coun">
                     <div class="input text">
                         <label for="Setting0Value">Year</label> 
                         <div id="makeyear_ids">
                         <select name="makeyear_id" id="makeyear_id" required>
                           <option value="">--Select Year--</option>
                         </select>
                       </div>
                     </div>
                </div>
   
    <div class="setting">
        <div class="input text">
            <label for="Setting0Value">Model</label>
            <input type="text" name="name" placeholder="Enter Your Model" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" required /><br />
            <span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
        </div>
    </div>
				   </fieldset> 
       
       <div class="submit"><input type="submit" name="submit" value="Add" onclick="return checkForm();" /></div>
      </form>
      </div>                
      </div> 
      </div> 
   <div class="clear">&nbsp;</div> 
      </div>
	  </div>
<div class="push"></div>
                   <script>
                   $("#make_id").live('change',function(){  
                                      var make_id=$(this).val();//alert(make_id);
                                      var data = 'make_id='+ make_id;
                                      $.ajax({
                                          url : "<?php echo base_url(); ?>/index.php/admin/getyearbymake",
                                          data : data,
                                          success:function(res) { 
                                            $('#makeyear_ids').html(res);
                                              //document.getElementById('makeyear_id').HTML = res;
                                          }           
                                      });

                          });
                   </script>   

      
<?php $this->load->view('footer'); ?> 


