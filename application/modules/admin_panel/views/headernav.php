<div id="nav-container">
  <div class="container_16">
    <div id="nav">
        <ul class="sf-menu sf-js-enabled">
          <!--  <li><a href="<?php //echo base_url('index.php/admin/dashboard');?>">&nbsp;Home&nbsp;</a></li>
            <li><a>|</a></li> -->

          <li><a href="javascript:;">Manage Users</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/allUsers');?>">All&nbsp;Users</a></li>	
            </ul>
          </li>	
          <li><a>|</a></li>
          
          <li><a href="javascript:;">Manage Car Posts</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/allCars');?>">All&nbsp;Posts</a></li>	
              <li><a href="<?php echo base_url('index.php/admin/activeCars');?>">Active&nbsp;Posts</a></li> 
              <li><a href="<?php echo base_url('index.php/admin/inactiveCars');?>">Inactive&nbsp;Posts</a></li> 
            </ul>
          </li>	
          <li><a>|</a></li>

          <li><a href="javascript:;">Manage Plates</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/allPlates');?>">All&nbsp;Plates</a></li> 
              <li><a href="<?php echo base_url('index.php/admin/activePlates');?>">Active&nbsp;Plates</a></li> 
              <li><a href="<?php echo base_url('index.php/admin/inactivePlates');?>">Inactive&nbsp;Plates</a></li> 
            </ul>
          </li> 

          <li><a>|</a></li>
          
          <li><a href="javascript:;">Manage Make</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/addMake');?>">Add&nbsp;Make</a></li>	
              <li><a href="<?php echo base_url('index.php/admin/allMake');?>">All&nbsp;Make</a></li>	
            </ul>
          </li>	
          <li><a>|</a></li>
          
          <li><a href="javascript:;">Manage Year</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/addYear');?>">Add&nbsp;Year</a></li>	
              <li><a href="<?php echo base_url('index.php/admin/allMYM');?>">All&nbsp;Year</a></li>  
              <!--<li><a href="<?php //echo base_url('index.php/admin/allYear');?>">All&nbsp;Year</a></li> -->
            </ul>
          </li>	
          <li><a>|</a></li>
          
          <li><a href="javascript:;">Manage Model</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/addModel');?>">Add&nbsp;Model</a></li>	
              <li><a href="<?php echo base_url('index.php/admin/allMYM');?>">All&nbsp;Model</a></li>  
          <!--    <li><a href="<?php //echo base_url('index.php/admin/allModel');?>">All&nbsp;Model</a></li> --> 
            </ul>
          </li>	
          <li><a>|</a></li>

          <li><a href="javascript:;">Manage Report</a>
            <ul class="has-ul">
              <li><a href="<?php echo base_url('index.php/admin/reportCar');?>">Reported&nbsp;Car</a></li>	
              <li><a href="<?php echo base_url('index.php/admin/reportPlate');?>">Reported&nbsp;Plate</a></li>  
          <!--    <li><a href="<?php //echo base_url('index.php/admin/allModel');?>">All&nbsp;Model</a></li> --> 
            </ul>
          </li>	
          <li><a>|</a></li>
           <li><a href="<?php echo base_url('index.php/admin/gen_notification');?>">General&nbsp;Notification</a></li>
		  <li><a>|</a></li>
          <li><a href="javascript:;">Admin Settings</a>
            <ul class="has-ul">
            <!--  <li><a href="<?php echo base_url('admin/settings/account');?>">Account&nbsp;Setting</a></li>	
              <li><a href="<?php echo base_url('admin/settings/password');?>">Password&nbsp;Setting</a></li>
              <li><a href="<?php echo base_url('admin/settings/contactinfo');?>">Contact&nbsp;Information</a></li> -->
                
                  <li><a href="<?php echo base_url('index.php/admin/paymentSetting');?>">Payment&nbsp;Setting</a></li>
                  <li><a href="<?php echo base_url('index.php/admin/paymentModeSetting');?>">Payment&nbsp;Mode</a></li>
                  <li><a href="<?php echo base_url('index.php/admin/changePassword');?>">Change&nbsp;Password</a></li>
                  <li><a href="<?php echo base_url('index.php/admin/logout');?>">Logout</a></li>
            </ul>
          </li>		
       </ul>
    </div>  
  </div>
</div>