<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Beans.tw - Admin Panel</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/reset.css');?>">

	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/960.css');?>">-->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/jquery-ui.css');?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/admin.css');?>">

  <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/extra.css');?>">-->

	

  <script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery.min.js');?>"></script>

  

  <script>

	$(function(){ 

	$('.ui-state-default').hover(function(){

		$(this).addClass('ui-state-hover');

	});

	

	$('.ui-state-default').mouseleave(function(){

		$(this).removeClass('ui-state-hover');

	});

	});

	</script>

  

  <style>

	

	</style>

  

</head>



<body>

	<!--<div id="wrapper" class="login">-->

		<div id="header">

			<p id="backtosite">Admin Login - Beans.tw</p> 

	</div>	



  <div id="main" style="min-height:540px;">

  	<div id="login">

    <?php if(isset($error) && $error!=''){?> <div class="error"><?=$error;?></div><?php } ?> 

   	    

    <?php echo form_open(base_url('index.php/admin')); ?>

        

		<div class="users form">

    <form id="UserAdminLoginForm" name="login_form" method="post" action="" accept-charset="utf-8">

			<fieldset style="margin-top:15px;">

      

      <div class="input text required">

      	<label for="UserUsername">Username:</label>

				<input name="username" maxlength="60" id="username" type="text" value="">

      </div>

      

      <div class="input password required">

      	<label for="UserPassword">Password:</label>

				<input name="password" id="password" type="password" value="">

      </div>        

			

    	<div class="submit">

      	<input name="submit" value="Log In" type="submit" class="ui-state-default ui-corner-all">

      </div>

      </fieldset>

		</form>

	</div>            

</div>

</div>

<!--</div>-->

<?php $this->load->view('admin/segments/footer');?>



