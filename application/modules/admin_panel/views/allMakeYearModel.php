<?php $this->load->view('header'); ?>
<?php $this->load->view('headertop'); ?>
<?php $this->load->view('headernav'); ?>


 <div id="main" class="container_16">
   <div class="grid_16"> 
    <div id="content"> 
     <div class="settings form">
      <h2>All Makes/Years/Models <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
      <div class="setting">
<div class="input1 text" align="right"></div>
</div>
<form id="form1" name="form1" method="post" action=""  enctype="multipart/form-data">
       <fieldset> 
        
<!--<div class="select" style="padding-left:200px;"> <a href="javascript:history.go(-1)"></a></div>-->

                 <div class="setting" id="coun">
                     <div class="input text">
                         <label for="Setting0Value">Make</label> 
                         <select name="make_id" id="make_id">
                         <option value="">--Make--</option>
          <?php	   foreach($makes as $mk)	   { ?>
		  <option value="<?php echo $mk['id']; ?>"> <?php echo $mk['name']; ?></option>
		  <?php } ?>
         </select>
                     </div>
                </div>

                 <div class="setting" id="coun">
                     <div class="input text">
                         <label for="Setting0Value">Year</label> 
                         <div id="makeyear_ids">
                         <select name="makeyear_id" id="makeyear_id">
                           <option value="">--Years--</option>
                         </select>
                       </div>
                     </div>
                </div>

                 <div class="setting" id="coun">
                     <div class="input text">
                         <label for="Setting0Value">Model</label> 
                         <div id="models">
                         <select name="model" id="model">
                           <option value="">--Models--</option>
                         </select>
                       </div>
                     </div>
                </div>
   
				   </fieldset> 
       
      </form>
      </div>                
      </div> 
      </div> 
   <div class="clear">&nbsp;</div> 
      </div>
	  </div>
<div class="push"></div>
                   <script>
                   $("#make_id").live('change',function(){  
                                      var make_id=$(this).val();//alert(make_id);
                                      var data = 'make_id='+ make_id;
                                      $.ajax({
                                          url : "<?php echo base_url(); ?>index.php/admin/getyearbymake",
                                          data : data,
                                          success:function(res) { 
                                            $('#makeyear_ids').html(res);
                                          }           
                                      });

                          });
                   </script> 

                   <script>
                   $("#makeyear_ids").live('change',function(){  
                                      var makeyear_ids=$(this).val();//alert(make_id);
                                      var data = 'makeyear_ids='+ makeyear_ids;
                                      $.ajax({
                                          url : "<?php echo base_url(); ?>index.php/admin/getmodelbyyear",
                                          data : data,
                                          success:function(res) { 
                                            $('#models').html(res);
                                          }           
                                      });

                          });
                   </script>   

      
<?php $this->load->view('footer'); ?> 


