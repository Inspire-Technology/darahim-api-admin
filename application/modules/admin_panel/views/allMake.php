<?php $this->load->view('header'); ?>
<?php $this->load->view('headertop'); ?>
<?php $this->load->view('headernav'); ?>
<?php $sn = 1; $perpage = 15; $start=0;	 ?>

  <div id="main" class="container_16">
   <div class="grid_16">
    <div id="content">
     <div class="nodes index">
      <!-- <h2>Manage User <a href="<?php //echo base_url("admin/add");?>" class="add-sec"><strong>Add User</strong></a></h2> -->
      <h3 style="color:green; ">
      <?php
      if($this->session->flashdata('success'))
      {
          echo $this->session->flashdata('success');
      }
      ?>
    </h3>
      <h2>All Makes <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
      <table cellpadding="10" cellspacing="0" width="100%" border="0" bgcolor="#FFF" class="display" id="example" >
      <thead>
      <tr class="hello">
        <th width="5%">S.N.</th>
        <th width="10%">Make id</th>
        <th width="30%">Make</th>
        <th width="30%">Arabic Make</th>
        <th width="35%">Logo</th>
        <th width="20%">Action</th> 
      </tr>
     	</thead>
			<tbody>
			<?php foreach($make as $w){	?>
			<tr class="record even gradeA">
        <td><?php echo $sn+$start; ?>.</td>
        <td><?php echo $w['id'];?>&nbsp;</td>
        <td><?php echo $w['name'];?>&nbsp;</td>
        <td><?php echo $w['arabic_name'];?>&nbsp;</td>
        <td><img src="http://motary.ae/motaryApp/carLogo/<?php echo $w['carLogo'];?>" alt="<?php echo strtolower($w['carLogo']); ?>" width="50px" hieght="50px">
        </td>



        <td nowrap="nowrap" align="center">
<!--<a href="<?php //echo base_url("index.php/admin/viewMake?id=$w[id]");?>"><img src="<?php //echo base_url('icons/view.png');?>" alt="View" title="View user"></a> -->
<a href="<?php echo base_url("index.php/admin/editMake?id=$w[id]");?>"><img src="<?php echo base_url('icons/edit.png')?>" alt="Edit" title="Edit user"/></a>
<a href="<?php echo base_url('index.php/admin/deleteMake/'.$w['id']);?>"  title="Delete" onclick="return confirm('Are you sure to delete this?')">
        <img  src="<?php echo base_url('icons/delete.png');?>" alt="delete" title="Delete" /></a>
        </td>


			</tr>

			<?php $sn++; } ?>
			</tbody>

	</table>
<br>
	<br/>
  </div>
  </div>
  </div>
  <div class="clear">&nbsp;</div>
  </div>
  <div class="push"></div>

<?php $this->load->view('segments/footer'); ?>