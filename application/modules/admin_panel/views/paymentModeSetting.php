<?php $this->load->view('header'); ?>
<?php $this->load->view('headertop'); ?>
<?php $this->load->view('headernav'); ?>


<div id="main" class="container_16">
    <div class="grid_16"> 
        <div id="content"> 
            <div class="settings form">
                <h2>Payment Mode Setting  <a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h2> 
                <div class="setting">
                    <div class="input1 text" align="right"></div>
                </div>

                <h3 style="color:green; ">
                    <?php
                    if ($this->session->flashdata('success')) {
                        echo $this->session->flashdata('success');
                    }
                    ?>
                </h3>
                <?php if (isset($paymentModeSetting['submit'])) { ?><div class="error"> success</div><?php } ?>
                <input type="radio" id="chkMode" name="chkMode" <?php if($paymentModeSetting['dev_isActive']==1) echo 'checked="checked"' ?> value="1" /> Development Mode Active
                <input type="radio" id="chkMode" name="chkMode" <?php if($paymentModeSetting['dev_isActive']==0) echo 'checked="checked"' ?> value="0" style="margin-left: 30px;" /> Production Mode Active
                <br/><br/>
                <form id="devForm" name="devForm" method="post" action="<?php echo base_url('index.php/admin/paymentModeSetting'); ?>"<?php if($paymentModeSetting['dev_isActive']==0) echo 'style="display:none"' ?>>
                    
                    <fieldset> 
                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Store Key</label> 
                                <input type="text" name="dev_key" placeholder="App Key" value="<?php echo $paymentModeSetting['dev_key']; ?>">
                                <input type="hidden" name="dev_isActive" value="1">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Store Id</label> 
                                <input type="text" name="dev_storeId" placeholder="Store Id" value="<?php echo $paymentModeSetting['dev_storeId']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">App Id</label> 
                                <input type="text" name="dev_appId" placeholder="App Id" value="<?php echo $paymentModeSetting['dev_appId']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">App Name</label> 
                                <input type="text" name="dev_appName" placeholder="App Name" value="<?php echo $paymentModeSetting['dev_appName']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">App Version</label> 
                                <input type="text" name="dev_appVersion" placeholder="App Version" value="<?php echo $paymentModeSetting['dev_appVersion']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Type</label> 
                                <input type="text" name="dev_transType" placeholder="Transfer Type" value="<?php echo $paymentModeSetting['dev_transType']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Class</label> 
                                <input type="text" name="dev_transClass" placeholder="Transfer Class" value="<?php echo $paymentModeSetting['dev_transClass']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Description</label> 
                                <input type="text" name="dev_transDesc" placeholder="Transfer Description" value="<?php echo $paymentModeSetting['dev_transDesc']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Currency</label> 
                                <input type="text" name="dev_transCurrency" placeholder="Transfer Currency" value="<?php echo $paymentModeSetting['dev_transCurrency']; ?>">
                            </div>
                        </div>

                    </fieldset> 
                    
                    <div class="submit"><input type="submit" name="submit" value="Update" /></div>
                </form>
                    
                    <form id="proForm" name="proForm" method="post" action="<?php echo base_url('index.php/admin/paymentModeSetting'); ?>" <?php if($paymentModeSetting['dev_isActive']==1) echo 'style="display:none"' ?>>
                    
                    <fieldset> 
                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Store Key</label> 
                                <input type="text" name="pro_key" placeholder="App Key" value="<?php echo $paymentModeSetting['pro_key']; ?>">
                                <input type="hidden" name="dev_isActive" value="0">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Store Id</label> 
                                <input type="text" name="pro_storeId" placeholder="Store Id" value="<?php echo $paymentModeSetting['pro_storeId']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">App Id</label> 
                                <input type="text" name="pro_appId" placeholder="App Id" value="<?php echo $paymentModeSetting['pro_appId']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">App Name</label> 
                                <input type="text" name="pro_appName" placeholder="App Name" value="<?php echo $paymentModeSetting['pro_appName']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">App Version</label> 
                                <input type="text" name="pro_appVersion" placeholder="App Version" value="<?php echo $paymentModeSetting['pro_appVersion']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Type</label> 
                                <input type="text" name="pro_transType" placeholder="Transfer Type" value="<?php echo $paymentModeSetting['pro_transType']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Class</label> 
                                <input type="text" name="pro_transClass" placeholder="Transfer Class" value="<?php echo $paymentModeSetting['pro_transClass']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Description</label> 
                                <input type="text" name="pro_transDesc" placeholder="Transfer Description" value="<?php echo $paymentModeSetting['pro_transDesc']; ?>">
                            </div>
                        </div>

                        <div class="setting" id="coun">
                            <div class="input text">
                                <label for="Setting0Value">Transfer Currency</label> 
                                <input type="text" name="pro_transCurrency" placeholder="Transfer Currency" value="<?php echo $paymentModeSetting['pro_transCurrency']; ?>">
                            </div>
                        </div>

                    </fieldset> 

                    <div class="submit"><input type="submit" name="submit" value="Update" /></div>
                </form>
            </div>                
        </div> 
    </div> 
    <div class="clear">&nbsp;</div> 
</div>
</div>
<div class="push"></div>

<script type="text/javascript">

$(document).ready(function(){

    $('input[type=radio]').click(function(){
        var v = $(this).val();
        if(v == 0){
            $('#devForm').hide();
            $('#proForm').show();
        }
        else{
            $('#devForm').show();
            $('#proForm').hide();
        }
    });    
});
</script>
<?php $this->load->view('footer'); ?> 


