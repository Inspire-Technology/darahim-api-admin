<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getAllUsers() {
        $result = $this->db->get('users')->result_array();
        return $result;
    }

    function get_user($userId) {
        $data = $this->db->get_where('users', array('userId' => $userId));
        return $data->row_array();
    }

    function getallCars() {
        $d = array(0, 1);
        $this->db->order_by('carId', 'desc');
        $this->db->where_in('carStatus', $d);
        $result = $this->db->get('car')->result_array();
        return $result;
    }

    function getinactiveCars() {
        //$status = 0;
        $this->db->where('carStatus', 0);
        $this->db->order_by('carId', 'desc');
        $result = $this->db->get('car')->result_array();
        return $result;
        //$query = last_query();
        // echo $query;
        // die();
    }

    function getactiveCars() {
        //$status = 0;
        $this->db->where('carStatus', 1);
        $this->db->order_by('carId', 'desc');
        $result = $this->db->get('car')->result_array();
        return $result;
        //$query = last_query();
        // echo $query;
        // die();
    }

    function get_car($carId) {
        $data = $this->db->get_where('car', array('carId' => $carId));
        return $data->row_array();
    }

    function update_car($car, $carId) { //print_r($car); die();
        //echo $carId; die();
        unset($car['submit']);
        $this->db->where('carId', $carId);
        $this->db->Update('car', $car);
        if ($car['carStatus'] != 0) {
            $this->pushNotificationAddCar($car['carModel'], $carId);
        }
    }

    function pushNotificationAddCar($document, $carId) {

        $this->db->select('users.deviceType,car.carId,users.deviceToken,users.isPushNotification,users.locale,car.carModel,car.carYear');
        $this->db->where('car.carModel', $document);
        //$this->db->where('car.isSold', 1); 
        $this->db->where('users.isPushNotification', 1);
        $this->db->join('carfavourite', 'car.carId = carfavourite.carId');
        $this->db->join('users', 'users.userId = carfavourite.userId');
        $result = $this->db->get('car')->result_array();

        if (!empty($result)) {
            $i = 0;
            foreach ($result as $key => $value) {
                $message = array();
                $message['message'] = "Check out " . $result[$i]['carModel'] . ", " . $result[$i]['carYear'] . " for sale in Darahim app";
                if ($result[$i]['locale'] == "ar") {
                    $message['message'] = "قم بالاطلاع على إعلان السيارة المفضلة عبر تطبيق دراهم" . $result[$i]['carModel'] . "," . $result[$i]['carYear'];
                }
                $message['pushType'] = 2;
                $message['carId'] = $carId; //$result[$i]['carId'];//added by nitish chauhan for getting carid of the favourite car
                $deviceType = $result[$i]['deviceType'];
                $deviceToken = $result[$i]['deviceToken'];
                $generatePush = generatePush($deviceType, $deviceToken, $message);
                // $generatePush       = AmazonSNS($deviceToken,$message);
                $i++;
            }
        } else {
            return false;
        }
    }

    function genPush($msg) {

        $this->db->select('users.deviceType,users.deviceToken,users.isPushNotification,users.locale');
        $this->db->where('users.isPushNotification', 1);
        //$this->db->select('users.deviceType,users.deviceToken,users.isPushNotification,users.locale');
        //$this->db->where('users.isPushNotification', 1);
        //$this->db->where('users.email', 'try@motary.ae');
        $result = $this->db->get('users')->result_array();
        //pre($result);die;
        if (!empty($result)) {

            //$message = array();
            $count = 0;
            foreach ($result as $key => $value) {
            //for($i=0;$i<100;$i++)   

                $message = urlencode($msg['message']);

                //$message['pushType'] = 2;

                $deviceType = $value['deviceType'];
                // $generatePush       = AmazonSNS($deviceToken,$message);
                $deviceToken = $value['deviceToken'];
                //$url = base_url().'index.php/test/sendPush';
                //$param =  array('deviceType' => $deviceType, 'deviceToken'=> $deviceToken, //'pushType'=>$message['pushType'], 'message'=> $message['message']);
                //$this->mylibrary->do_in_background($url, $param);
                //-----------------------shelll script----
//2>/dev/null &'."'";
                // $cmd = "'".$deviceToken.' '.$message.' AIzaSyCCYlFbWVEls29mnm6oaFG0in0pQVZZGOo '.$deviceType."'";
                //$out = shell_exec('./test.sh ' .$deviceToken.' '.$message. ' AIzaSyCCYlFbWVEls29mnm6oaFG0in0pQVZZGOo ' .$deviceType. ' > /dev/null 2>/dev/null &');
                $out = shell_exec('./test.sh ' . $deviceToken . ' ' . $message . ' AIzaSyCCYlFbWVEls29mnm6oaFG0in0pQVZZGOo ' . $deviceType . ' > /dev/null 2>/dev/null &');

                //$out = shell_exec('./test.sh APA91bENrQsovT8TxhZjLamkLwJUrjyplMxxI6hCgML5w-Q2R1f-px8LQmSiVu_Y_hWLGhWZTsbW_l1wBBYQoi0zvR3t__KSr3UuMNQMKoTSXWLu2BMYp2k tr AIzaSyCCYlFbWVEls29mnm6oaFG0in0pQVZZGOo 0');
//$out = shell_exec('./test.sh APA91bENrQsovT8TxhZjLamkLwJUrjyplMxxI6hCgML5w-Q2R1f-px8LQmSiVu_Y_hWLGhWZTsbW_l1wBBYQoi0zvR3t__KSr3UuMNQMKoTSXWLu2BMYp2k test_shell AIzaSyCCYlFbWVEls29mnm6oaFG0in0pQVZZGOo');
                //-----------------------------------------





                $count ++;
            }
            return $count;
        } else {
            return false;
        }
    }

    function deleteCar($cid) {
        $this->db->where('carId', $cid);
        $this->db->delete('car');
        //$query=$this->db->last_query();
        //echo $query; die();
    }

    function deleteMake($mid) {
        $this->db->where('id', $mid);
        $result = $this->db->get('carmake')->row_array();
        $mname = $result['name'];

        $this->db->where('id', $mid);
        $this->db->delete('carmake');

        $this->db->where('carMake', $mname);
        $this->db->delete('carlogo');

        //$query=$this->db->last_query();
        //echo $query; die();
    }

    function addMake($make, $logo, $arabicName) {
        //print_r($data);die;
        //unset($data['submit']);
        $data1 = array('name' => $make, 'arabic_name' => $arabicName);
        $this->db->insert('carmake', $data1);
        $make_id = $this->db->insert_id();

        $data2 = array('makeId' => $make_id, 'carMake' => $make, 'carLogo' => $logo);
        $this->db->insert('carlogo', $data2);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function uploadCarLogo($key, $make) { //echo $key.' '.$make; die;
        $files = $_FILES[$key];
        $cnt = count($files['name']);
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {
            $ext = explode('.', $_FILES[$key]['name']);
            $ext = $ext['1'];
            $name = "carLogo/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = $make . '.' . $ext;
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    function addYear($data) {
        unset($data['submit']);
        $this->db->insert('caryear', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function isYearExistOnMakeId($data) {
        $make_id = $data['make_id'];
        $year = $data['year'];
        $this->db->where('make_id', $make_id);
        $this->db->where('year', $year);
        $result = $this->db->get('caryear')->result_array();
        //echo $this->db->last_query();
        return $result;
    }

    function isMakeNameExist($data) {
        $this->db->where('name', $data['name']);
        $result = $this->db->get('carmake')->row_array();
        //echo $this->db->last_query();
        return $result;
    }

    function addModel($data) {
        unset($data['submit']);
        unset($data['make_id']);
        $this->db->insert('carmodel', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function getallmake() {
        $result = $this->db->get('carmake')->result_array();
        return $result;
    }

    function getallmakeWithLogo() {

        $this->db->SELECT('carmake.id as id,carmake.name as name,carlogo.carLogo as carLogo,carmake.arabic_name as arabic_name');
        $this->db->order_by('id', 'desc');
        $this->db->join('carlogo', 'carlogo.makeId=carmake.id');
        $result = $this->db->get('carmake')->result_array();
        return $result;
    }

    function getMakeWithLogo($mid) {
        $this->db->where('carmake.id', $mid);
        $this->db->SELECT('carmake.id as id,carmake.name as name,carlogo.carLogo as carLogo,carmake.arabic_name as arabic_name');
        $this->db->join('carlogo', 'carlogo.carMake=carmake.name');
        $result = $this->db->get('carmake')->row_array();
        return $result;
    }

    function getallyear() {
        $this->db->order_by('year', 'desc');
        $result = $this->db->get('caryear')->result_array();
        return $result;
    }

    function getmakeby_mid($make_id) {
        $this->db->where('id', '$make_id');
        $result = $this->db->get('carmake')->row_array();
        //print_r($result); die();
        return $result;
    }

    function getyearby_yid($year_id) {
        $this->db->where('id', '$year_id');
        $result = $this->db->get('caryear')->result_array();
        return $result;
    }

    function getyearby_mid($make_id) {
        $this->db->order_by('year', 'desc');
        $this->db->where('make_id', '$make_id');
        $result = $this->db->get('caryear')->result_array();
        return $result;
    }

    function getmodelby_yid($year_id) {
        $this->db->where('makeyear_id', '$year_id');
        $result = $this->db->get('carmodel')->result_array();
        return $result;
    }

    function getallmodel() {
        $result = $this->db->get('carmodel')->result_array();
        return $result;
        //$query = last_query();
        // echo $query;
        // die();
    }

    function getpaymentoption() {

        $result = $this->db->select('paymentOption');
        $result = $this->db->get('adminSettings')->row_array();
        return $result;
        //$query = last_query();
        // echo $query;
        // die();
    }

    function getyearbymake($make_id) {
        //echo $make_id; die();
        $this->db->order_by('year', 'desc');
        $this->db->where('make_id', $make_id);
        $result = $this->db->get('caryear')->result_array();
        return $result;
    }

    function getmodelbyyear($makeyear_id) {
        //echo $make_id; die();
        $this->db->where('makeyear_id', $makeyear_id);
        $result = $this->db->get('carmodel')->result_array();
        return $result;
    }

    function update_paymentSetting($paymentOption) {
        
        $this->db->Update('adminSettings', $paymentOption);
        return $this->db->affected_rows();
    }

    function getpaymentSetting() {
        $result = $this->db->get('adminSettings')->row_array();
        return $result;
    }

    // function addMake($document)
    // {
    //echo "hello"; die();
    //   $this->db->insert('carmake', $document);
    //  $id = $this->db->insert_id();
    //  if($id)
    //  {
    //     return $id;
    //  }
    //  else
    //  {
    //      return false;
    // }
    //}

    function updateMake($make) { //print_r($car); die();
        //echo $carId; die();
        //print_r($make);
        $carmakeData['name'] = $make['name'];
        $carmakeDataAR['arabic_name'] = $make['arabic_name'];

        $carlogoData['makeId'] = $make['id'];
        $carlogoData['carMake'] = $make['name'];
        if (isset($make['logo'])) {
            $carlogoData['carLogo'] = $make['logo'];
        }



        $this->db->where('id', $make['id']);
        $this->db->Update('carmake', $carmakeData);

        $this->db->where('id', $make['id']);
        $this->db->Update('carmake', $carmakeDataAR);

        $this->db->where('makeId', $make['id']);
        $this->db->Update('carlogo', $carlogoData);
    }

    function changePassword($data) {
        $res = $this->db->get('admin')->row_array();
        if ($data['oldpass'] == $res['password']) {
            if ($data['newpass'] == $data['conpass']) {
                $doc['password'] = $data['newpass'];
                $this->db->Update('admin', $doc);
                return 1;
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    function getUsersById($userId) {
        $collection = $this->mongo_db->db->selectCollection('tbl_users');
        $userId = array("_id" => new MongoId($userId));
        $result = $collection->findOne($userId);
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function getAreaByAreaId($areaId) {
        $collection = $this->mongo_db->db->selectCollection('tbl_parking_area');
        $areaId = array("_id" => new MongoId($areaId));
        $result = $collection->findOne($areaId);
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function getAllArea() {
        $collection = $this->mongo_db->db->selectCollection('tbl_parking_area');
        $result = $collection->find();
        $count = $collection->count();

        if (!empty($result)) {
            return array('result' => $result, 'count' => $count);
        } else {
            return false;
        }
    }

    function getAllZone($area_id) {
        $collection = $this->mongo_db->db->selectCollection('tbl_parking_zone');
        $areaId = array("area_id" => $area_id);
        $result = $collection->find($areaId);
        $count = 0;
        foreach ($result as $key) {
            $count = $count + 1;
        }


        if (!empty($result)) {
            return array('result' => $result, 'count' => $count);
        } else {
            return false;
        }
    }

    function getValetByValetId($valetId) {
        $collection = $this->mongo_db->db->selectCollection('tbl_valet');
        $result = $collection->findOne(array("valetId" => $valetId));
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function isUserExist($document) {

        //connect to mongodb collection (i.e., table) named as ‘tbl_users’
        $this->db->where('userName', $document['userName']);
        $this->db->where('password', $document['password']);
        $result = $this->db->get('admin')->row_array();

        //$collection = $this->mongo_db->db->selectCollection('tbl_admin');
        //selecting records from the collection - tbl_users 
        //$result     = $collection->findOne(array("username"=>$document['username'],"password"=>$document['password'])); 

        if (!empty($result)) {

            return true;
        } else {

            return false;
        }
    }

    function insertArea($document) {
        //connect to mongodb collection (i.e., table) named as ‘tbl_users’

        $collection = $this->mongo_db->db->selectCollection('tbl_parking_area');
        //selecting records from the collection - tbl_users 

        $result = $collection->insert($document);

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function insertLocality($document) {
        //connect to mongodb collection (i.e., table) named as ‘tbl_users’

        $collection = $this->mongo_db->db->selectCollection('tbl_parking_zone');
        //selecting records from the collection - tbl_users 

        $result = $collection->insert($document);

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function getallPlates() {
        $d = array(0, 1);
        $this->db->where('plateNumber<>', '');
        $this->db->where_in('plateStatus', $d);
        $this->db->order_by('plateId', 'desc');
        $result = $this->db->get('plate')->result_array();

        return $result;
    }

    function getinactivePlates() {
        //$status = 0;
        $this->db->where('plateStatus', 0);
        $this->db->where('plateNumber<>', '');
        $this->db->order_by('plateId', 'desc');
        $result = $this->db->get('plate')->result_array();
        return $result;
        //$query = last_query();
        // echo $query;
        // die();
    }

    function getactivePlates() {
        //$status = 0;
        $this->db->where('plateStatus', 1);
        $this->db->where('plateNumber<>', '');
        $this->db->order_by('plateId', 'desc');
        $result = $this->db->get('plate')->result_array();
        return $result;
        //$query = last_query();
        // echo $query;
        // die();
    }

    function get_plate($carId) {
        $data = $this->db->get_where('plate', array('plateId' => $carId));
        return $data->row_array();
    }

    function update_plate($car, $carId) {
        unset($car['submit']);
        $this->db->where('plateId', $carId);
        $this->db->Update('plate', $car);
    }

    function deletePlate($cid) {
        $this->db->where('plateId', $cid);
        $this->db->delete('plate');
        //$query=$this->db->last_query();
        //echo $query; die();
    }

    function getOldCarId($cid) {
        $this->db->where('newCarId', $cid);
        $result = $this->db->get('caredit')->row_array();
        if (!empty($result)) {
            return $result['oldCarId'];
        } else {
            return false;
        }
    }

    function getOldPlateId($cid) {
        $this->db->where('newPlateId', $cid);
        $result = $this->db->get('platedit')->row_array();
        if (!empty($result)) {
            return $result['oldPlateId'];
        } else {
            return false;
        }
    }

    function updatePlatestatus($cid, $pid) {

        if ($cid == 0) {
            $status = 1;
        } else {

            $status = 0;
        }
        $this->db->query('update mt_plate set plateStatus = "' . $status . '" where plateId="' . $pid . '"')or die(mysql_error());
    }

    function updatecarstatus($cid, $pid) {
        if ($cid == 0) {
            $status = 1;
        } else {

            $status = 0;
        }
        $this->db->query('update mt_car set carStatus = "' . $status . '" where carId="' . $pid . '"')or die(mysql_error());
    }

    function GetReportedCars() {
        return $this->db->get('reportedCars')->result_array();
    }

    function GetReportedPlate() {
        return $this->db->get('reportedPlate')->result_array();
    }

}
