<?php

function sendAndroidPush($deviceToken, $msg, $badge = 0, $check = 0, $type = "") {

    // echo $type;die;
    $registrationIDs = array($deviceToken);

    if (is_array($deviceToken)) {

        $registrationIDs = $deviceToken;
    } else {
        $registrationIDs = array($deviceToken);
    }
    //print_r($registrationIDs);
    // die;
    // Message to be sent
    $message = $msg;


    //echo $message;die;
    //Set POST variables
    $url = 'https://android.googleapis.com/gcm/send';

    //APA91bHVGsZNduWkCCPuEwNKTiQjmbjIXsr6CiqgMHvoHa2CPJU3ogr8ZEAyPQqbFjVgyg1TjuhcwMNFgWZxh6o9bKsPOC2PFsaojFL2b8BMunPChO3jyy-EYwW_oD9aapSLn4FVpS41Zy7zrzpUudI6XiorYfRYAA
    //$registrationIDs = 'APA91bHVGsZNduWkCCPuEwNKTiQjmbjIXsr6CiqgMHvoHa2CPJU3ogr8ZEAyPQqbFjVgyg1TjuhcwMNFgWZxh6o9bKsPOC2PFsaojFL2b8BMunPChO3jyy-EYwW_oD9aapSLn4FVpS41Zy7zrzpUudI6XiorYfRYAA';
//	$fields = array(
//		'registration_ids' => $registrationIDs,
//		'data' => array("message" => $message, "type" => "1",'challengeId'=>$badge,'pushType'=>$check),
//	);

    $fields = array(
        'registration_ids' => $registrationIDs,
        'data' => array("msg" => $message)
    );

    // echo '<pre>';
    // print_r($fields);
    //   $fields = array(
//		'registration_ids' => 'APA91bFrqEVyEOkrbsuMnHYM9D5rOhIm6utpVXHKwzys3d6Gni1EPE-pWiqQdqs9BawjaSpnF246LdV_P7mktyvt9ITEIqw5mxbJ3BaPFHU0KENwLrMKqODNyqm1oB09WQAPzr14_uMsYLKT59KSZAWnSLzqiJF6NQ',
//		'data' => array("message" => $message,'challengeId'=>$badge),
//	);
    //key=AIzaSyClTotl7ROnm10cneMfPJuNPfV80VUZS-o
    $headers = array(
        'Authorization: key=AIzaSyCCYlFbWVEls29mnm6oaFG0in0pQVZZGOo',
        'Content-Type: application/json'
    );
    //AIzaSyACvXL6SzRvoRjk_qsOD_aLhuXatBvnlYQ
    //Open connection
    $ch = curl_init();

    //Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    //Execute post
    $result = curl_exec($ch);


    $myfile = file_put_contents('logs.txt', $result, FILE_APPEND);
    $myfile = file_put_contents('logs.txt', "=====================================", FILE_APPEND);

    return $result;
    //Close connection
    curl_close($ch);
}

function sendIphonePush($deviceToken, $msg, $badge = 0, $check = 0, $version = 1) {

    //$apnsHost = 'gateway.sandbox.push.apple.com';            //development phase
    $apnsHost = 'gateway.push.apple.com';            //distribution phase
    $apnsPort = '2195';                                //.pem file ko project root per paste karna hai
    $apnsCert = 'ck.pem';                            //certificate pem file
    $passPhrase = '123456789';                            //cetificate password
    $streamContext = stream_context_create();
    stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
    $apnsConnection = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
    if ($apnsConnection == false) {
        return;
    } else {
        
    }

    if (is_array($msg)) {
        $msgbody = $msg["message"];
    } else {

        $msg = json_decode($msg, true);
//pre($msg);die;
        $msgbody = $msg["message"];
    }
    $message = json_encode($msg);
//pre($message);die;
//pre($msg);die;
    $payload['aps'] = array(
        'alert' => array(
            'body' => $msgbody,
            'action-loc-key' => 'Motary',
        ),
        'json' => $msg,
        'badge' => 1,
        'sound' => 'oven.caf',
    );
    $payload = json_encode($payload);

    try {

        if ($message != "") {
            $apnsMessage = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
            $fwrite = fwrite($apnsConnection, $apnsMessage);
            if ($fwrite) {
                
            } else {
                
            }
        }
    } catch (Exception $e) {
        echo 'Caught exception: ' . $e->getMessage() . "\n";
        error_log($e->getMessage() . chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
    }
}

/* function sendIphonePush($deviceToken,$msg,$badge=0,$check=0,$version="") {


  //echo $deviceToken;  die;                              //Noted by vivek
  $apnsHost = 'gateway.sandbox.push.apple.com';	   //development phase
  //$apnsHost = 'gateway.push.apple.com';            //distribution phase
  $apnsPort = '2195';                                //.pem file ko project root per paste karna hai
  $apnsCert = 'ck.pem';                            //certificate pem file
  //$apnsCert = 'ck1.pem';
  $passPhrase = '123456489';                            //cetificate password
  $streamContext = stream_context_create();
  stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
  $apnsConnection = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
  if ($apnsConnection == false) {
  //echo "Failed to connect {$error} {$errorString}\n";
  //print "Failed to connect {$error} {$errorString}\n";
  //error_log($error.chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
  return;
  } else {
  ///echo "Connection successful";
  //error_log('Connection successful', 3, "/mnt/srv/MOOVWORKER/push-errors.log");
  }
  $message = $msg;

  //$payload['aps'] = array('alert' => $message, 'sound' => 'default','challengeId'=>$badge,'pushType'=>$check);
  $payload['aps'] = array('alert' => $message,
  'sound' => 'default',
  'badge' => 1,
  'bundleVersion' => $version
  );


  $payload['aps'] = array(
  'alert' => array(
  'body' => 'Notification from Motary',
  'action-loc-key' => 'Motary',
  ),
  'json' => $message,
  'badge' => 1,
  'sound' => 'oven.caf',
  );

  $payload = json_encode($payload);
  //print_r($payload);
  //$deviceToken = "dfe587d02a99d57fa7d785c1901409d408dfa920fa90890fbe3fed1fc090c7ee";
  //$deviceToken = $deviceToken;//"dfe587d02a99d57fa7d785c1901409d408dfa920fa90890fbe3fed1fc090c7ee";

  try {

  if ($message != "") {
  //echo $deviceToken;
  //echo $message;
  $apnsMessage = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
  $fwrite = fwrite($apnsConnection, $apnsMessage);
  if ($fwrite) {
  //echo "true";
  //error_log($fwrite.chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
  } else {
  //echo "false";
  }
  }
  } catch (Exception $e) {
  //echo 'Caught exception: '.  $e->getMessage(). "\n";
  //error_log($e->getMessage().chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
  }
  } */

function generatePush($deviceType, $deviceToken, $message) {


    if ($deviceType == '0') {

        sendAndroidPush($deviceToken, $message);
    } else if ($deviceType == '1') {
        //$message = implode($message);
        //$message = is_array($message)?implode($message):$message;
        /*  //$val = $deviceToken.'|'.$message;
          //AmazonSNS($deviceToken,$message);

          //$ch = curl_init('http://ec2-52-205-20-98.compute-1.amazonaws.com/motaryAdmin/thirdpushapi.php');
          //curl_setopt($ch, CURLOPT_POST, 1);
          //curl_setopt($ch, CURLOPT_POSTFIELDS, array('dt'=>$deviceToken,'message'=>'123' ));
          //curl_setopt($ch, CURLOPT_POSTFIELDS, array('dt'=>$val )); //
          //curl_setopt($ch, CURLOPT_POSTFIELDS, array('message'=>$message )); //
          //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          //$result = curl_exec($ch);

          //curl_close($ch);
          // pre($result);die('gxhdfh');
          //$result = json_decode($result, True);
          //print_r($result) ;die;
          //echo 'Curl: ', function_exists('curl_version') ? 'Enabled' : 'Disabled';
          //echo "current";	die;
          // $geo            = file_get_contents('http://ec2-52-91-61-98.compute-1.amazonaws.com/motaryAdmin/thirdpushapi.php',true);
         */
        sendIphonePush($deviceToken, $message);
    } else {

        /*
         * do nothing
         */
    }
}

function AmazonSNS($token, $msg) {
## first get arn using token
    $host = "localhost";
    $username = "mootary";
    $password = "UAE!@#00";
    $db_name = "mootary_app";
    mysql_connect("$host", "$username", "$password")or die("cannot connect");
    mysql_select_db("$db_name")or die("cannot select DB");
    mysql_query("SET NAMES 'UTF8'");

    $select = "SELECT EndpointArn FROM `mt_users` WHERE deviceToken='$token'";
    $result = mysql_query($select) or die(mysql_error());
    $row = mysql_fetch_row($result);
    $arn = $row[0];

    $url_n = 'http://www.motary.ae/sns/classSNS.php';


    $fields_n = array(
        'message' => $msg,
        'url' => '',
        'arn' => $arn,
        'method' => 'pushNoti'
    );


    $postvars_n = http_build_query($fields_n);
    $ch_n = curl_init();
    curl_setopt($ch_n, CURLOPT_URL, $url_n);
    curl_setopt($ch_n, CURLOPT_POST, count($fields_n));
    curl_setopt($ch_n, CURLOPT_POSTFIELDS, $postvars_n);

    $result_n = curl_exec($ch_n);
    curl_close($ch_n);
}

function sendDriverPush($deviceToken, $msg) {

    //echo $deviceToken;
    //$apnsHost = 'gateway.sandbox.push.apple.com';	
    $apnsHost = 'gateway.push.apple.com';
    $apnsPort = '2195';
    $apnsCert = 'driver_dis.pem';
    $passPhrase = '';
    $streamContext = stream_context_create();
    stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
    $apnsConnection = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
    if ($apnsConnection == false) {
        //echo "Failed to connect {$error} {$errorString}\n";
        //print "Failed to connect {$error} {$errorString}\n";
        error_log($error . chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
        return;
    } else {
        //	echo "Connection successful";	
        error_log('Connection successful', 3, "/mnt/srv/MOOVWORKER/push-errors.log");
    }
    $message = $msg;
    $payload['aps'] = array('alert' => $message, 'sound' => 'default');
    $payload = json_encode($payload);
    //$deviceToken = "dfe587d02a99d57fa7d785c1901409d408dfa920fa90890fbe3fed1fc090c7ee";
    //$deviceToken = $deviceToken;//"dfe587d02a99d57fa7d785c1901409d408dfa920fa90890fbe3fed1fc090c7ee";

    try {

        if ($message != "") {
            //echo $deviceToken;
            //echo $message;
            $apnsMessage = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
            $fwrite = fwrite($apnsConnection, $apnsMessage);
            if ($fwrite) {
                //echo "true";
                error_log($fwrite . chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
            } else {
                //echo "false";
            }
        }
    } catch (Exception $e) {
        //echo 'Caught exception: '.  $e->getMessage(). "\n";
        error_log($e->getMessage() . chr(13), 3, "/mnt/srv/MOOVWORKER/push-errors.log");
    }
}

?>
