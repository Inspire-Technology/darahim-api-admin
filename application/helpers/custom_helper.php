<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pre'))
{
    function pre($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }   
}
if ( ! function_exists('is_json'))
{
    function is_json($string,$return_data = false) 
    {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    } 
}

