<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Beans.tw - Admin Panel</title>
   
  <link rel="icon" href="<?php echo base_url('public/images/fav.png');?>" type="image/png">  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/reset.css');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/960.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/jquery-ui.css');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/thickbox.css');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/extra.css');?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/css/admin.css');?>" />
    
  
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery_003.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('public/admin/js/croogo.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery-ui.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery_002.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery_007.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery_004.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery_005.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/superfish.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/supersubs.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery_006.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/thickbox-compressed.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/admin.js');?>"></script>
  
	<style type="text/css" title="currentStyle">
		@import "<?php echo base_url('public/admin/css/demo_page.css');?>";
		@import "<?php echo base_url('public/admin/css/demo_table.css');?>";
	</style>
	<script type="text/javascript" language="javascript" src="<?php echo base_url('public/admin/js/jquery.js');?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url('public/admin/js/jquery.dataTables.js');?>"></script>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#example').dataTable( {
			"sPaginationType": "full_numbers"
		});
	});
	</script>
	<script type="text/javascript">
	String.prototype.parseURL = function() {
		return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+/, function(url) {
		return url.link(url);
	});
	};
	</script>
	
 <!--<style> .ui-corner-all{ border-radius:5px;	} </style>-->   
     
</head>

<body>
	<div id="wrapper">
 